"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngFileUpload',
    'backofficeApp'
]);

// app.constant('APPCONFIG', {
//     'APIURL': 'http://heaven.dev/api/'
// });

app.constant('APPCONFIG', {
    // 'APIURL': 'http://localhost:3000/'
    'APIURL': 'http://'+window.location.hostname+':3000/'
    // 'APIURL': 'http://192.168.0.122:3000/'
});

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');

});