(function () {
    "use strict";
    angular.module('faqApp')
        .controller('FaqController', FaqController);

    FaqController.$inject = ['$scope', '$rootScope', '$state', '$location', 'FaqService', 'toastr', 'SweetAlert', '$uibModal'];

    function FaqController($scope, $rootScope, $state, $location, FaqService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getFaqsList = getFaqsList;
        vm.getSingleFaq = getSingleFaq;
        vm.saveFaq = saveFaq;
        vm.deleteFaq = deleteFaq;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'FAQs';
        vm.totalFaqs = 0;
        vm.faqsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.faqForm = { faq_id: '' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.faq');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleFaq(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getFaqsList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getFaqsList(newPage, searchInfo);
        }//END changePage();

        /* to save faq after add and edit  */
        function saveFaq() {
            FaqService.saveFaq(vm.faqForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'FAQ');
                        $state.go('backoffice.faq');
                    } else {
                        toastr.error(response.data.message, 'FAQ');
                    }
                } else {
                    toastr.error(response.data.message, 'FAQ');
                }
            }, function (error) {
                toastr.error('Internal server error', 'FAQ');
            });
        }//END saveFaq();

        /* to get faq list */
        function getFaqsList(newPage, obj) {
            vm.totalFaqs = 0;
            vm.faqList = [];
            FaqService.getFaqs(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.faqs && response.data.faqs.length > 0) {
                        vm.totalFaqs = response.data.total;
                        vm.faqList = response.data.faqs;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getFaqsList();

        /* to get single faq */
        function getSingleFaq(faq_id) {
            FaqService.getSingleFaq(faq_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleFaq = response.data.faq;
                        vm.faqForm = response.data.faq;
                    } else {
                        toastr.error(response.data.message, 'FAQ');
                        $state.go('backoffice.faq');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'FAQ');
            });
        }//END getSingleFaq();

        /** to delete a faq **/
        function deleteFaq(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this FAQ?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    FaqService.deleteFaq(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.faqList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'FAQ');
                    });
                }
            });
        }//END deleteFaq();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getFaqsList(1, '');
        }//END reset();
    }

}());
