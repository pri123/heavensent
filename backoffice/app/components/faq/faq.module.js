(function () {

    angular.module('faqApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var faqPath = 'app/components/faq/';
        $stateProvider
            .state('backoffice.faq', {
                url: 'faq',
                views: {
                    'content@backoffice': {
                        templateUrl: faqPath + 'views/index.html',
                        controller: 'FaqController',
                        controllerAs: 'faq'
                    }
                }
            })
            .state('backoffice.faq.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: faqPath + 'views/form.html',
                        controller: 'FaqController',
                        controllerAs: 'faq'
                    }
                }
            })
            .state('backoffice.faq.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: faqPath + 'views/form.html',
                        controller: 'FaqController',
                        controllerAs: 'faq'
                    }
                }
            })
            .state('backoffice.faq.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: faqPath + 'views/view.html',
                        controller: 'FaqController',
                        controllerAs: 'faq'
                    }
                }
            })
    }

}());