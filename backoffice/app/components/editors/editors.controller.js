(function() {
    'use strict';
    angular.module('editorApp').controller('EditorController', EditorController);

    EditorController.$inject = ['$scope', 'editorServices', 'sharedService', 'toastr', '$location', 'SweetAlert', '$state', '$rootScope'];

    function EditorController($scope, editorServices, sharedService, toastr, $location, SweetAlert, $state, $rootScope) {
        var vm = this;
        $rootScope.headerTitle = 'Editors';
        vm.addForm = {};
        vm.validatePhone = validatePhone;
        vm.uploadFiles = uploadFiles;
        vm.saveEditor = saveEditor;
        vm.getEditorList = getEditorList;
        vm.changePage = changePage;
        vm.getIndex = getIndex;
        
        vm.sort = sort;
        vm.totalEditors = 0;
        vm.editorsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = {
            current: 1
        };
        vm.editFlag = false;
        vm.deleteEditor = deleteEditor;
        vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        vm.setAsDefaultEditor = setAsDefaultEditor;
        vm.title = 'Add';
        vm.uploadDocs = uploadDocs;
        if(!vm.editFlag) {
            vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        }
        vm.deleteUserFile = deleteUserFile;
        
        vm.changeStatus = changeStatus;

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if(path[3] == ""){
                $state.go('backoffice.editors');
                return false;
            } else {
            	editEditor(path[3]);
            }
        }

        /* to accept only number */
        function validatePhone(number) {
        	return sharedService.validatePhoneNumber(number);
        }//END validatePhone()

        function uploadFiles(files, errFiles) {
	    	vm.addForm.profile_image = files[0];
	    	var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            reader.onloadend = function (event) {
                $scope.$apply(function ($scope) {
                	vm.profile_image_path = event.target.result;
                });
            }
	    }

	    $scope.uploadDocument = function(files, errFiles) {
	    	vm.addForm.document_file = files[0];
	    }

        /* to save editor details */
        function saveEditor() {
            $rootScope.$emit("StartLoader", {});
            vm.addForm.email_from = $rootScope.user.email;
        	editorServices.saveEditor(vm.addForm).then(function(response) {
                if(response.data.status == 1) {
                    toastr.success(response.data.message, 'Success');
                    $state.go('backoffice.editors');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            },function (error) {
                toastr.error('Something went wrong!', 'Error');
            }).finally(function () {
                $rootScope.$emit("CloseLoader", {});
            }); 
        }//END saveEditor()

        /* for sorting editor list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getEditorList(newPage, searchInfo);
        }//END changePage()

        //retrive indexing
        function getIndex(newPage, length){
            var index = sharedService.indexPerPage(newPage, length, vm.editorsPerPage);
                        vm.fromIndex = index.fromIndex;
                        vm.toIndex = index.toIndex;
        }//END getIndex()

        /* to get editor list */
        function getEditorList(newPage, obj) {
        	editorServices.getEditors(newPage, obj).then(function(response) {
            	if (response.status == 200) {
                	if(response.data.editors && response.data.editors.length > 0) {
                        vm.totalEditors = response.headers('total_editors');
                        vm.editorList = response.data.editors;
                        getIndex(newPage, vm.editorList.length);
                    } else {
                        vm.totalEditors = 0;
                        vm.editorList = "";
                        getIndex(newPage, vm.editorList.length);
                    }
                } 
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getEditorList()

        /* to show editor details in form */
        function editEditor(userId) {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.user_id = userId;
                getEditorById(userId);
        }//END editEditor()

        /* to get editor information */
        function getEditorById(userId) {
        	editorServices.getEditorById(userId).then(function(response) {
            	if (response.status == 200) {
                    if (response.data.status == 1) {
                        console.log(response.data);
                        vm.editFlag = true;
                        vm.addForm.name = response.data.user_details.firstname;
                        vm.addForm.company_name = response.data.user_details.company_name;
                        vm.addForm.address = (response.data.user_details.address == 'undefined') ? '' : response.data.user_details.address;
                        vm.addForm.phone = (response.data.user_details.phone == 'undefined') ? '' : response.data.user_details.phone;
                        vm.addForm.email = response.data.user_details.email;
                        vm.addForm.social_security = (response.data.user_details.social_security == 'undefined') ? '' : response.data.user_details.social_security;
                        vm.addForm.ein = (response.data.user_details.ein == 'undefined') ? '' : response.data.user_details.ein;
                        vm.addForm.profile_image = (response.data.user_details.profile_image == 'undefined') ? '' : response.data.user_details.profile_image;
                        vm.addForm.createdAt = moment(response.data.user_details.created_at).format('MMMM Do YYYY, h:mm:ss a');
                        vm.addForm.userId = response.data.user_details.user_id;
                        vm.addForm.roleId = response.data.user_details.role_id;
                        vm.addForm.isDefaultEditor = response.data.user_details.is_default_editor;
                        vm.addForm.password = response.data.user_details.password;
                        vm.profile_image = response.data.user_details.profile_image;
                        // if(vm.addForm.profile_image != '' && vm.addForm.profile_image != undefined) {
                        // 	vm.profile_image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/avatar/thumbs/' + vm.addForm.profile_image;
                    	// } else {
                    	// 	vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
                    	// }
                        if(response.data.user_files.length > 0) {
                            vm.userFiles = [];
                            angular.forEach(response.data.user_files, function(value, key) {
                                var image_path = '';
                                if(value.type == 2) {
                                    image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/files/' + value.file_name;
                                }
                                vm.userFiles.push({
                                    'id':value.id,
                                    'original_name':value.original_name+'.'+value.extension,
                                    'size':formatBytes(value.size),
                                    'type':value.type,
                                    'image_path':image_path,
                                    'extension':value.extension
                                });

                            });
                        }

                        if(response.data.user_details.status == 0) {
                            vm.addForm.status = '2';
                        } else {
                            vm.addForm.status = '1';
                        }
                    }
                }
            },function (error) {
                $state.go('backoffice.editors');
                toastr.error(error.data.message, 'Error');
            });
        }//END getEditorById();

        /* to convert file size to KB,MB,GB */
        function formatBytes(bytes) {
            if(bytes < 1024) return bytes + " Bytes";
            else if(bytes < 1048576) return(bytes / 1024).toFixed(2) + " KB";
            else if(bytes < 1073741824) return(bytes / 1048576).toFixed(2) + " MB";
            else return(bytes / 1073741824).toFixed(2) + " GB";
        }//END formatBytes()

        /** to delete a editor **/
        function deleteEditor() {   
        	var id = vm.addForm.userId;
        	SweetAlert.swal({
               title: "Are you sure you want to delete this editor?",
               text: "",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
               cancelButtonText: "No",
               closeOnConfirm: false,
               closeOnCancel: true,
               html: true
            }, 
            function(isConfirm){ 
                if (isConfirm) {
                    editorServices.deleteEditor(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            $state.go('backoffice.editors');
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END deleteEditor()

        /* to set a editor as default */
        function setAsDefaultEditor(event) {
            var id = vm.addForm.userId;
            SweetAlert.swal({
               title: "Are you sure you want to set this editor as default?",
               text: "",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",
               cancelButtonText: "No",
               closeOnConfirm: false,
               closeOnCancel: true,
               html: true
            }, 
            function(isConfirm){ 
                if (isConfirm) {
                    editorServices.setAsDefaultEditor(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Success!", response.data.message, "success");
                            vm.addForm.isDefaultEditor = 1;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END setAsDefaultEditor()

        $scope.uploadMultipleDocument = function(files, errFiles) {
            vm.documents = files;
            vm.addForm.documents = files;
        }

        function uploadDocs() {
            $rootScope.$emit("StartLoader", {});
            var data = {documents:vm.documents,user_id:vm.user_id}
            editorServices.uploadDocs(data).then(function(response) {
                if(response.data.status == 1) {
                    vm.userFiles = [];
                    angular.forEach(response.data.files, function(value, key) {
                        var image_path = '';
                        if(value.type == 2) {
                            image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/files/' + value.file_name;
                        }
                        vm.userFiles.push({
                            'id':value.id,
                            'original_name':value.original_name+'.'+value.extension,
                            'size':formatBytes(value.size),
                            'type':value.type,
                            'image_path':image_path,
                            'extension':value.extension
                        });

                    });
                    toastr.success(response.data.message, 'Success');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            },function (error) {
                toastr.error(error.data.message, 'Error');
            }).finally(function () {
                $rootScope.$emit("CloseLoader", {});
            }); 
        }

        /** to delete a user file **/
        function deleteUserFile(obj,index) {
            var id = obj.id;
            SweetAlert.swal({
               title: "Are you sure you want to delete this file?",
               text: "",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
               cancelButtonText: "No",
               closeOnConfirm: false,
               closeOnCancel: true,
               html: true
            }, 
            function(isConfirm){ 
                if (isConfirm) {
                    editorServices.deleteUserFile(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.userFiles.splice(index, 1);
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END deleteUserFile()

        /* to change active/inactive status of editor */ 
        function changeStatus(status) {
            var data = { userId: vm.user_id, status: status };
            editorServices.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

    }

}());