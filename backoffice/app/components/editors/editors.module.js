(function () {
    'use strict';
    angular.module('editorApp', []).config(config);

    function config($stateProvider, $urlRouterProvider) {
        var projectPath = 'app/components/editors/';
        $stateProvider
            .state('backoffice.editors', {
                url: 'editors',
                views: {
                    'content@backoffice': {
                        templateUrl: projectPath + 'views/index.html',
                        controller: 'EditorController',
                        controllerAs: 'editor'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'backoffice.dashboard',
                    label: 'All Editors'
                },
                data: {
                    pageTitle: 'Samples Testing',
                    grantAccessTo: 'authenticated'
                }
            })
            .state('backoffice.editors.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: projectPath + 'views/create.html',
                        controller: 'EditorController',
                        controllerAs: 'editor'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'backoffice.editors',
                    label: 'Add Editor'
                }
            })
            .state('backoffice.editors.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: projectPath + 'views/create.html',
                        controller: 'EditorController',
                        controllerAs: 'editor'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'backoffice.editors',
                    label: 'Edit Editor'
                }
            })
            .state('backoffice.editors.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: projectPath + 'views/view.html',
                        controller: 'EditorController',
                        controllerAs: 'editor'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'backoffice.editors',
                    label: 'Editor Details'
                }
            })
        //$locationProvider.html5Mode(true);
    }

}());