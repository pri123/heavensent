(function() {
    "use strict";

    angular
        .module('editorApp')
        .service('editorServices', editorServices);

    editorServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    function editorServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

    
        self.saveEditor = saveEditor;
        self.getEditors = getEditors;
        self.getEditorById = getEditorById;
        self.deleteEditor = deleteEditor;
        self.setAsDefaultEditor = setAsDefaultEditor;
        self.uploadDocs = uploadDocs;
        self.deleteUserFile = deleteUserFile;
        self.changeStatus = changeStatus;
        
        /* to create new editor */
        function saveEditor(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'save-editor';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    obj.name = (obj.name === undefined) ? '' : obj.name;
                    obj.company_name = (obj.company_name === undefined) ? '' : obj.company_name;
                    obj.email = (obj.email === undefined) ? '' : obj.email;
                    obj.password = (obj.password === undefined) ? '' : obj.password;
                    formData.append("name", obj.name);
                    formData.append("company_name", obj.company_name);
                    formData.append("address", obj.address);
                    formData.append("phone", obj.phone);
                    formData.append("email", obj.email);
                    formData.append("password", obj.password);
                    formData.append("social_security", obj.social_security);
                    formData.append("ein", obj.ein);
                    formData.append("profile_image", obj.profile_image);
                    angular.forEach(obj.documents, function(value, key) {
                        formData.append("documents[]", obj.documents[key]);    
                    });
                    formData.append("email_from", obj.email_from);
                    if(obj.userId != undefined && obj.userId != '') {
                        formData.append("user_id", obj.userId);
                    }    
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END saveEditor();

        /* to get all editors */
        function getEditors(pageNum, obj) {
            var deferred = $q.defer();
            var where = 'pageNum=' + pageNum;
            if (obj != undefined) {
                if (obj.name != undefined && obj.name != "")
                    where += '&name=' + obj.name;
                
                if (obj.status != undefined && obj.status != "")
                    where += '&status=' + obj.status;
            }
            
            $http({
                url: APPCONFIG.APIURL + 'get-editor-list?' + where,
                method: "GET",
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            })
            .then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;

        }//END getEditors();

        /* to get editor */
        function getEditorById(id) {
            var deferred = $q.defer();
            $http({
                url: APPCONFIG.APIURL + 'get-editor',
                method: "POST",
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    id = (id === undefined) ? '' : id;
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getEditorById();

        /* to delete a editor from database */
        function deleteEditor(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'delete-editor';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    id = (id === undefined) ? '' : id;
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END deleteEditor();

        /* to set a editor as default */
        function setAsDefaultEditor(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'set-default-editor';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    id = (id === undefined) ? '' : id;
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END setAsDefaultEditor()

        /* to upload multiple docs */
        function uploadDocs(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'upload-documents';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    angular.forEach(obj.documents, function(value, key) {
                        formData.append("documents[]", obj.documents[key]);    
                    });
                    
                    if(obj.user_id != undefined && obj.user_id != '') {
                        formData.append("user_id", obj.user_id);
                    }    
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END uploadDocs();

        /* to delete a file from database */
        function deleteUserFile(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'delete-file';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    id = (id === undefined) ? '' : id;
                    formData.append("id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END deleteUserFile();
        
        /* to change active/inactive status of user */
        function changeStatus(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'change-user-status';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    formData.append("user_id", obj.userId);
                    formData.append("status", obj.status);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END changeStatus()

        return self;
    };
})();