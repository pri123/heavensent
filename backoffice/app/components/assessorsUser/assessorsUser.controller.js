(function () {
    "use strict";
    angular.module('assessorsUserApp')
        .controller('AssessorsUserController', AssessorsUserController);

    AssessorsUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AssessorsUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function AssessorsUserController($scope, $rootScope, $state, $location, AssessorsUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getAssessorsUserList = getAssessorsUserList;
        vm.getSingleAssessorsUser = getSingleAssessorsUser;
        vm.saveAssessorsUser = saveAssessorsUser;
        vm.deleteAssessorsUser = deleteAssessorsUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Assessors User';
        vm.totalAssessorsUser = 0;
        vm.assessorsUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.assessorsUserForm = { user_id: '', usertypeFlag: '4', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.assessorsUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleAssessorsUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting assessors user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAssessorsUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAssessorsUserList(newPage, searchInfo);
        }//END changePage();

        /* to save assessors user after add and edit  */
        function saveAssessorsUser() {
            AssessorsUserService.saveAssessorsUser(vm.assessorsUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Assessors User');
                        $state.go('backoffice.assessorsUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Assessors User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Assessors User');
                            }else{
                                toastr.error(response.data.message, 'Assessors User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Assessors User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessors User');
            });
        }//END saveAssessorsUser();

        /* to get assessors user list */
        function getAssessorsUserList(newPage, obj) {
            vm.totalAssessorsUser = 0;
            vm.assessorsUserList = [];
            AssessorsUserService.getAssessorsUser(newPage, obj, vm.orderInfo, '4').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalAssessorsUser = response.data.total;
                        vm.assessorsUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAssessorsUserList();

        /* to get single assessors user */
        function getSingleAssessorsUser(user_id) { 
            var UserType = 4;           
            AssessorsUserService.getSingleAssessorsUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleAssessorsUser = response.data.data[0];
                        // vm.cafeUserForm = response.data.data[0];
                        vm.assessorsUserForm.user_id = response.data.data[0].user_id;
                        vm.assessorsUserForm.usertype = response.data.data[0].user_role_id;
                        vm.assessorsUserForm.registertype = response.data.data[0].user_registertype;
                        vm.assessorsUserForm.assessors_name = response.data.data[0].assessors_name;
                        vm.assessorsUserForm.assessors_email = response.data.data[0].assessors_email;
                        vm.assessorsUserForm.assessors_phone = response.data.data[0].assessors_phone;
                        vm.assessorsUserForm.assessors_address = response.data.data[0].assessors_address;
                        vm.assessorsUserForm.user_username = response.data.data[0].user_username;
                        vm.assessorsUserForm.status = response.data.data[0].status; 
                        vm.assessorsUserForm.userusername = true;
                    } else {
                        toastr.error(response.data.message, 'Assessors User');
                        $state.go('backoffice.assessorsUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessors User');
            });
        }//END getSingleAssessorsUser();

        /** to delete a assessors user **/
        function deleteAssessorsUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Assessors User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AssessorsUserService.deleteAssessorsUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.assessorsUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Assessors User');
                    });
                }
            });
        }//END deleteAssessorsUser();

        /* to change active/inactive status of assessors user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            AssessorsUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this Assessors User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    AssessorsUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAssessorsUserList(1, '');
        }//END reset();
    }

}());
