(function () {
    "use strict";
    angular
        .module('demoApp')
        .service('DemoService', DemoService);

    DemoService.$inject = ['$http', 'APPCONFIG', '$q'];

    function DemoService($http, APPCONFIG, $q) {

        /* save demo */
        function saveDemo(obj) {
            // var URL = APPCONFIG.APIURL + 'demo/add';

            // var requestData = {};
            // if (typeof obj !== 'undefined' && obj !== '') {
            //     if (typeof obj.demo_id !== 'undefined' && obj.demo_id !== '') {
            //         requestData['demo_id'] = obj.demo_id;
            //         URL = APPCONFIG.APIURL + 'demo/edit';
            //     }

            //     if (typeof obj.demo_data !== 'undefined' && obj.demo_data !== '') {
            //         requestData['demo_data'] = obj.demo_data;
            //     }
            // }

            // return this.runHttp(URL, requestData);
        } //END saveDemo();

        /* to get all demo */
        function getDemos(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'demo';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.demo_id !== 'undefined' && obj.demo_id !== '') {
                    requestData['demo_id'] = parseInt(obj.demo_id);
                }

                if (typeof obj.demo_data !== 'undefined' && obj.demo_data !== '') {
                    requestData['demo_data'] = obj.demo_data;
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getDemos();

        /* to get single demo */
        function getSingleDemo(demo_id) {
            var URL = APPCONFIG.APIURL + 'demo/view';
            var requestData = {};

            if (typeof demo_id !== undefined && demo_id !== '') {
                requestData['demo_id'] = demo_id;
            }

            return this.runHttp(URL, requestData);
        } //END getdemoById();

        /* to delete a demo from database */
        function deleteDemo(demo_id) {
            var URL = APPCONFIG.APIURL + 'demo/delete';
            var requestData = {};

            if (typeof demo_id !== undefined && demo_id !== '') {
                requestData['demo_id'] = demo_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteDemo();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getDemos: getDemos,
            getSingleDemo: getSingleDemo,
            saveDemo: saveDemo,
            deleteDemo: deleteDemo
        }

    };//END FaqService()
}());
