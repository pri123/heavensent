(function () {
    "use strict";
    angular.module('demoApp')
        .controller('DemoController', DemoController);

    DemoController.$inject = ['$scope', '$rootScope', '$state', '$location', 'DemoService', 'toastr', 'SweetAlert', '$uibModal','Upload'];

    function DemoController($scope, $rootScope, $state, $location, DemoService, toastr, SweetAlert, $uibModal,Upload) {
        var vm = this;

        vm.getDemosList = getDemosList;
        vm.getSingleDemo = getSingleDemo;
        vm.saveDemo = saveDemo;
        vm.deleteDemo = deleteDemo;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Demos';
        vm.totalDemos = 0;
        vm.demosPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.demoForm = { demo_id: '' };
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.demo');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleDemo(path[3]);
            }
        }

      
        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getDemosList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getDemosList(newPage, searchInfo);
        }//END changePage();

        /* to save demo after add and edit  */
        function saveDemo() {
            console.log(vm.demoForm.file.$ngfName);
            var tempPath = vm.demoForm.file.$ngfName,
             targetPath = path.resolve('./assets/uploads/worker/tempPath');
                    if (path.extname(vm.demoForm.file.$ngfName).toLowerCase() === '.jpg') {
                        fs.rename(tempPath, targetPath, function(err) {
                            if (err) throw err;
                            console.log("Upload completed!");
                        });
                    } else {
                        fs.unlink(tempPath, function () {
                            if (err) throw err;
                            console.error("Only .png files are allowed!");
                        });
                    }
            
            // DemoService.saveDemo(vm.demoForm).then(function (response) {
            //     if (response.status == 200) {
            //         if (response.data.status == 1) {
            //             toastr.success(response.data.message, 'DEMO');
            //             $state.go('backoffice.demo');
            //         } else {
            //             toastr.error(response.data.message, 'DEMO');
            //         }
            //     } else {
            //         toastr.error(response.data.message, 'DEMO');
            //     }
            // }, function (error) {
            //     toastr.error('Internal server error', 'DEMO');
            // });
        }//END saveDemo();

        /* to get demo list */
        function getDemosList(newPage, obj) {
            vm.totalDemos = 0;
            vm.demoList = [];
            DemoService.getDemos(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.demos && response.data.demos.length > 0) {
                        vm.totalDemos = response.data.total;
                        vm.demoList = response.data.demos;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getDemosList();

        /* to get single demo */
        function getSingleDemo(demo_id) {
            DemoService.getSingleDemo(demo_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleDemo = response.data.demo;
                        vm.demoForm = response.data.demo;
                    } else {
                        toastr.error(response.data.message, 'FAQ');
                        $state.go('backoffice.demo');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'DEMO');
            });
        }//END getSingleDemo();

        /** to delete a demo **/
        function deleteDemo(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Demo Data?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    DemoService.deleteDemo(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.demoList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'FAQ');
                    });
                }
            });
        }//END deleteDemo();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getDemosList(1, '');
        }//END reset();
    }

}());
