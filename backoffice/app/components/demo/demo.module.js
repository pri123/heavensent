(function () {

    angular.module('demoApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var demoPath = 'app/components/demo/';
        $stateProvider
            .state('backoffice.demo', {
                url: 'demo',
                views: {
                    'content@backoffice': {
                        templateUrl: demoPath + 'views/index.html',
                        controller: 'DemoController',
                        controllerAs: 'demo'
                    }
                }
            })
            .state('backoffice.demo.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: demoPath + 'views/form.html',
                        controller: 'DemoController',
                        controllerAs: 'demo'
                    }
                }
            })
            .state('backoffice.demo.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: demoPath + 'views/form.html',
                        controller: 'DemoController',
                        controllerAs: 'demo'
                    }
                }
            })
            .state('backoffice.demo.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: demoPath + 'views/view.html',
                        controller: 'DemoController',
                        controllerAs: 'demo'
                    }
                }
            })
    }

}());