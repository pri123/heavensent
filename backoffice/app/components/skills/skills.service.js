(function () {
    "use strict";
    angular
        .module('skillsApp')
        .service('SkillsService', SkillsService);

        SkillsService.$inject = ['$http', 'APPCONFIG', '$q'];

    function SkillsService($http, APPCONFIG, $q) {

        /* save skills */
        function saveSkills(obj) {
            var URL = APPCONFIG.APIURL + 'skills/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.skills_id !== 'undefined' && obj.skills_id !== '') {
                    requestData['skills_id'] = obj.skills_id;
                    URL = APPCONFIG.APIURL + 'skills/edit';
                }

                if (typeof obj.skills_name !== 'undefined' && obj.skills_name !== '') {
                    requestData['skills_name'] = obj.skills_name;
                }
                
                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                } 
                
                if (typeof obj.cat_id !== 'undefined' && obj.cat_id !== '') {
                    requestData['parent'] = obj.cat_id;
                } 
            }

            return this.runHttp(URL, requestData);
        } //END saveSkills();
       

        /* to get all skills */
        function getSkills(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'skills';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.skills_id !== 'undefined' && obj.skills_id !== '') {
                    requestData['skills_id'] = parseInt(obj.skills_id);
                }

                if (typeof obj.skills_name !== 'undefined' && obj.skills_name !== '') {
                    requestData['skills_name'] = obj.skills_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }  
                
                if (typeof obj.parent !== 'undefined' && obj.parent !== '') {
                    requestData['parent'] = parseInt(obj.parent);
                } 
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSkills();

        /* to get single skill */
        function getSingleSkill(skills_id) {
            var URL = APPCONFIG.APIURL + 'skills/view';
            var requestData = {};

            if (typeof skills_id !== undefined && skills_id !== '') {
                requestData['skills_id'] = skills_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleSkill();

        /* to get all parent skills  */
        function getAllMainCatSkills(parent) {
            var URL = APPCONFIG.APIURL + 'skills/AllList';

            var requestData = { parent };             
                   
            return this.runHttp(URL, requestData);
        }//END getAllMainCatSkills();        

        /* to delete a skills from database */
        function deleteSkills(skills_id) {
            var URL = APPCONFIG.APIURL + 'skills/delete';
            var requestData = {};

            if (typeof skills_id !== undefined && skills_id !== '') {
                requestData['skills_id'] = skills_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteSkills();

        /* to change active/inactive status of skills */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'skills/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.skills_id != undefined && obj.skills_id != "") {
                    requestData["skills_id"] = obj.skills_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()  

        function getCategoryParent(obj){
            var URL = APPCONFIG.APIURL + 'skills/parent';
            
            var requestData = {obj};
            
            return this.runHttp(URL, requestData);
        }
        
        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getSkills: getSkills,
            getSingleSkill: getSingleSkill,
            saveSkills: saveSkills,
            deleteSkills: deleteSkills,
            changeStatus: changeStatus,
            getAllMainCatSkills: getAllMainCatSkills,
            getCategoryParent: getCategoryParent
        }

    };//END SkillsService()
}());
