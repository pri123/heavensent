(function () {
    "use strict";

    angular
        .module('authApp')
        .service('authServices', authServices);

    authServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    var someValue = '';

    function authServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

        self.checkLogin = checkLogin;
        self.checkForgotPassword = checkForgotPassword;
        self.checkValidUser = checkValidUser;
        self.setAuthToken = setAuthToken;
        self.getAuthToken = getAuthToken;
        self.saveUserInfo = saveUserInfo;
        self.checkValidUrl = checkValidUrl;
        self.getUserProfile = getUserProfile;
        self.updateProfile = updateProfile;
        self.changePassword = changePassword;
        self.logout = logout;

        //to check if user is login and set user details in rootscope
        function checkLogin(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login';
            var requestData = {};

            if (typeof obj.email !== undefined && obj.email !== '') {
                requestData['username'] = obj.email;
            }
            if (typeof obj.password !== undefined && obj.password !== '') {
                requestData['password'] = obj.password;
            }
            if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                requestData['usertype'] = obj.usertypeFlag;
            }
            if (typeof obj.device_token !== undefined && obj.device_token !== '') {
                requestData['device_token'] = obj.device_token;
            }
            if (typeof obj.device_type !== undefined && obj.device_type !== '') {
                requestData['device_type'] = obj.device_type;
            }
            
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                $rootScope.isLogin = true;
                deferred.resolve(response);
            }, function (response) {
                $rootScope.isLogin = false;
                $rootScope.$broadcast('auth:login:required');
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkLogin();


        function checkValidUser(isAuth) {
            var URL = APPCONFIG.APIURL + 'validate-user';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("type", 'admin');
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined,
                    'Access-Token': getAuthToken()
                }
            }).then(function (response) {
                $rootScope.isLogin = true;
                saveUserInfo(response.data);
                if (isAuth === false) {
                    $rootScope.$broadcast('auth:login:success');
                } else {
                    checkValidUrl(response.data.user_detail.role_id, $location.$$path);
                }
                deferred.resolve();
            }).catch(function (response) {
                $rootScope.isLogin = false;
                if (isAuth === false) {
                    deferred.resolve();
                } else {
                    $rootScope.$broadcast('auth:login:required');
                    deferred.resolve();
                }
            });
            return deferred.promise;
        } //END checkValidUser();

        function checkForgotPassword(email) {
            var URL = APPCONFIG.APIURL + 'forgot-password';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("email", email);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkForgotPassword();

        function setAuthToken(userInfo) {
            // localStorage.setItem('token', userInfo.headers('Access-token'));
            localStorage.setItem('token', userInfo);
        } //END setAuthToken();

        function getAuthToken() {
            if (localStorage.getItem('token') != undefined && localStorage.getItem('token') != null)
                return localStorage.getItem('token');
            else
                return null;
        } //END getAuthToken();

        function saveUserInfo(data) {
            var user = {};
            if (data.status == 1) {
                user = data.user_detail;
                $rootScope.user = user;
                $rootScope.baseUrl = APPCONFIG.APIURL;
                $rootScope.bucketName = APPCONFIG.BUCKETNAME;
                //$cookies.putObject("token_key", localStorage.getItem('token'));
            }
            return user;
        } //END saveUserInfo();

        function checkValidUrl(role, location) {
            var urlData = location.split('/');
            var actualUrl = urlData[1];
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'valid-url';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("role_id", role);
                    formData.append("url", actualUrl);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        deferred.resolve(response);
                    } else {
                        $state.go('backoffice.dashboard');
                    }
                }
            });
            return deferred.promise;
        }

        /* to get user profile data */
        function getUserProfile(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'view-profile';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END getUserProfile()

        /* to update profile data */
        function updateProfile(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'update-profile';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    var name = obj.name.split(" ");
                    obj.lastname = (name[1] === undefined) ? '' : name[1];
                    formData.append("firstname", name[0]);
                    formData.append("lastname", obj.lastname);
                    formData.append("company_name", obj.company_name);
                    formData.append("address", obj.address);
                    formData.append("phone", obj.phone);
                    formData.append("email", obj.email);
                    formData.append("user_id", obj.user_id);
                    formData.append("profile_image", obj.profile_image);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END updateProfile();

        /* to update profile data */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'change-password';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("old_password", obj.password);
                    formData.append("new_password", obj.new_password);
                    formData.append("c_password", obj.confirm_passowrd);
                    formData.append("user_id", obj.user_id);
                    formData.append("token", getAuthToken());
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END changePassword();

        function logout() {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'logout';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) { },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                localStorage.removeItem('token');
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END logout()  

        return self;
        
    };
})();