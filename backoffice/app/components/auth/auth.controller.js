(function () {
    'use strict';
    angular.module('authApp').controller('AuthController', AuthController);

    AuthController.$inject = ['$rootScope', '$location', 'authServices', '$state', 'toastr'];

    function AuthController($rootScope, $location, authServices, $state, toastr) {
        var vm = this;
        vm.login = login;
        // vm.forgotPassword = forgotPassword;

        vm.user = { usertypeFlag: '1', device_token: ' ', device_type: '1' };
        // vm.user = { email: 'admin@mailinator.com', password: '123456' };
        $rootScope.bodyClass = '';

        function login(loginInfo) {
            // $state.go('backoffice.dashboard');
            authServices.checkLogin(loginInfo).then(function (response) {
                var token = response.data.data[0].device_token;
                if (response.status == 200) {
                    // authServices.setAuthToken(token); //Set auth token
                    $state.go('backoffice.dashboard');
                } else {
                    toastr.error(response.data.message, "Error");
                }
            }).catch(function (response) {
                toastr.error(response.data.error, "Error");
            });
        }; //END login()   

        // function forgotPassword(userInfo) {
        //     authServices.checkForgotPassword(userInfo).then(function (response) {
        //         if (response.status == 200) {
        //             toastr.success(response.data.message, "Forgot Password");
        //         } else {
        //             toastr.error(response.data.message, "Forgot Password");
        //         }
        //     }).catch(function (response) {
        //         toastr.error(response.data.message, "Forgot Password");
        //     });
        // }; //END forgotPassword() 
    };
}());