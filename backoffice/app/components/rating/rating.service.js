(function () {
    "use strict";

    angular
        .module('ratingApp')
        .service('RatingService', RatingService);

    RatingService.$inject = ['$http', 'APPCONFIG', '$q'];

    function RatingService($http, APPCONFIG, $q) {
        var self = this;
        self.getRatings = getRatings,
        self.deleteRating = deleteRating,
        self.saveRating = saveRating,
        self.getRatingById = getRatingById,
        self.changeStatus = changeStatus;
                
        // self.saveRoleOrder = saveRoleOrder;
        

        /* to get all Rating */
        function getRatings(pageNum, obj) {
            var deferred = $q.defer();

            var requestData = { pageNum };
            if (obj != undefined) {
                if (obj.id != undefined && obj.id != "") {
                    requestData["id"] = obj.id;
                }

                if (obj.name != undefined && obj.name != "") {
                    requestData["name"] = obj.name;
                }

                if (obj.created_at != undefined && obj.created_at != "") {
                    requestData["created_at"] = moment(obj.created_at).format("YYYY-MM-DD");
                }

                if (obj.status != undefined && obj.status != "") {
                    requestData["status"] = obj.status;
                }
            }

            $http({
                url: APPCONFIG.APIURL + 'rating',
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;

        }//END getRatings();
        
        /* to change active/inactive status of Rating */
        function changeStatus(obj) {
            var deferred = $q.defer();
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.rating_id != undefined && obj.rating_id != "") {
                    requestData["rating_id"] = obj.rating_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status_id"] = obj.status;
                }                
            }            
            $http({
                url: APPCONFIG.APIURL + 'rating/status',
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            
            return deferred.promise;
        }//END changeStatus()

        /* to delete a Rating from database */
        function deleteRating(id) {
            var deferred = $q.defer();
            
            var requestData = {};
            if (id != undefined) {
                requestData["rating_id"] = id;
            }   
            $http({
                url: APPCONFIG.APIURL + 'rating/delete',
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END deleteRating();

        /* to create new Rating */
        function saveRating(obj) {
            var deferred = $q.defer();
            var requestData = {};            
            if(obj.ratingId == 0){
                if (typeof obj.name != undefined && obj.name !== '') {
                    requestData["name"] = obj.name;
                }
                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }   
                $http({
                    url: APPCONFIG.APIURL + 'rating/add',
                    method: "POST",
                    data: requestData,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(error);
                });
            }else{
                if (obj.name != undefined) {
                    requestData["name"] = obj.name;
                }   
                if (obj.ratingId != undefined) {
                    requestData["rating_id"] = obj.ratingId;
                }
                $http({
                    url: APPCONFIG.APIURL + 'rating/edit',
                    method: "POST",
                    data: requestData,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(error);
                });
            }
            return deferred.promise;
        } //END saveRating();

        /* to get Rating by ID */
        function getRatingById(id) {
            var deferred = $q.defer();
            var requestData = {};
            if (id != undefined) {
                requestData["rating_id"] = id;
            }   
            $http({
                url: APPCONFIG.APIURL + 'rating/getRatingById',
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getRatingById();

       
       
       
        // return {
        //     saveEditor: saveEditor,
        //     getEditors: getEditors,
        //     getEditorById: getEditorById,
        //     deleteEditor: deleteEditor,
        //     setAsDefaultEditor: setAsDefaultEditor,
        //     uploadDocs: uploadDocs,
        //     deleteUserFile: deleteUserFile,
        //     changeStatus: changeStatus
        // };
      
        /* to set a editor as default */
        // function setAsDefaultEditor(id) {
        //     var deferred = $q.defer();
        //     var URL = APPCONFIG.APIURL + 'set-default-editor';
        //     $http({
        //         method: 'POST',
        //         url: URL,
        //         processData: false,
        //         transformRequest: function (data) {
        //             var formData = new FormData();
        //             id = (id === undefined) ? '' : id;
        //             formData.append("user_id", id);
        //             return formData;
        //         },
        //         headers: {
        //             'X-Requested-With': 'XMLHttpRequest',
        //             'Content-Type': undefined
        //         }
        //     }).then(function (response) {
        //         deferred.resolve(response);
        //     }, function (error) {
        //         deferred.reject(error);
        //     });
        //     return deferred.promise;
        // }//END setAsDefaultEditor()

        /* to upload multiple docs */
        // function uploadDocs(obj) {
        //     var deferred = $q.defer();
        //     var URL = APPCONFIG.APIURL + 'upload-documents';
        //     $http({
        //         method: 'POST',
        //         url: URL,
        //         processData: false,
        //         transformRequest: function (data) {
        //             var formData = new FormData();
        //             angular.forEach(obj.documents, function (value, key) {
        //                 formData.append("documents[]", obj.documents[key]);
        //             });

        //             if (obj.user_id != undefined && obj.user_id != '') {
        //                 formData.append("user_id", obj.user_id);
        //             }
        //             return formData;
        //         },
        //         headers: {
        //             'X-Requested-With': 'XMLHttpRequest',
        //             'Content-Type': undefined
        //         }
        //     }).then(function (response) {
        //         deferred.resolve(response);
        //     }, function (error) {
        //         deferred.reject(error);
        //     });
        //     return deferred.promise;
        // } //END uploadDocs();

        /* to delete a file from database */
        // function deleteUserFile(id) {
        //     var deferred = $q.defer();
        //     var URL = APPCONFIG.APIURL + 'delete-file';
        //     $http({
        //         method: 'POST',
        //         url: URL,
        //         processData: false,
        //         transformRequest: function (data) {
        //             var formData = new FormData();
        //             id = (id === undefined) ? '' : id;
        //             formData.append("id", id);
        //             return formData;
        //         },
        //         headers: {
        //             'X-Requested-With': 'XMLHttpRequest',
        //             'Content-Type': undefined
        //         }
        //     }).then(function (response) {
        //         deferred.resolve(response);
        //     }, function (error) {
        //         deferred.reject(error);
        //     });
        //     return deferred.promise;
        // } //END deleteUserFile();

                
    };
})();