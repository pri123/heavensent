(function () {
    'use strict';
    angular.module('ratingApp', []).config(config);

    function config($stateProvider, $urlRouterProvider) {
        var ratingPath = 'app/components/rating/';
        $stateProvider
            .state('backoffice.rating', {
                url: 'rating',
                views: {
                    'content@backoffice': {
                        templateUrl: ratingPath + 'views/index.html',
                        controller: 'RatingController',
                        controllerAs: 'rating'
                    }
                }                
            })
            .state('backoffice.rating.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: ratingPath + 'views/create.html',
                        controller: 'RatingController',
                        controllerAs: 'rating'
                    }
                }
            })
            .state('backoffice.rating.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: ratingPath + 'views/create.html',
                        controller: 'RatingController',
                        controllerAs: 'rating'
                    }
                }
            })
            .state('backoffice.rating.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: ratingPath + 'views/view.html',
                        controller: 'RatingController',
                        controllerAs: 'rating'
                    }
                }
            });
    }

}());