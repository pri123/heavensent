(function() {
    'use strict';
    angular.module('ratingApp').controller('RatingController', RatingController);

    RatingController.$inject = ['$scope', '$rootScope', '$state', '$location', 'RatingService', 'toastr', 'SweetAlert', '$uibModal'];

    function RatingController($scope, $rootScope, $state, $location, RatingService, toastr, SweetAlert, $uibModal) {
        
        console.log($state.current.name);

        var vm = this;
        $rootScope.headerTitle = 'Ratings'; 
               
        vm.getRatingList = getRatingList;
        vm.getSingleRating = getSingleRating;
        vm.saveRating = saveRating;
        vm.deleteRating = deleteRating; 
        vm.formatDate = formatDate;
        vm.changePage = changePage;       
        vm.changeStatus = changeStatus;
        vm.editRating = editRating;
         
        vm.sort = sort;
        vm.reset = reset;
                
        vm.title = 'Add New';
        vm.addForm = { rating_id: '' };        
        vm.totalRatings = 0;
        vm.ratingsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if(path[3] == ""){
                $state.go('backoffice.rating');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                editRating(path[3]);
                getSingleRating(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting editor list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getRatingList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getRatingList(newPage, searchInfo);
        }//END changePage()

        /* to save editor details */
        function saveRating() {
            vm.addForm.ratingId = (vm.addForm.ratingId == undefined || vm.addForm.ratingId == '') ? 0 : vm.addForm.ratingId;    
            RatingService.saveRating(vm.addForm).then(function(response) {
                if(response.data.status == 1) {
                    toastr.success(response.data.message, 'Rating');
                    $state.go('backoffice.rating');
                } else {
                    toastr.error(response.data.error, 'Rating');
                }
            },function (error) {
                toastr.error('Internal server error', 'Rating');
            }).finally(function () {
                $rootScope.$emit("CloseLoader", {});
            }); 
        }//END saveEditor()

        /* to get Rating list */
        function getRatingList(newPage, obj) {
            vm.totalRatings = 0;
            vm.ratingList = [];
        	RatingService.getRatings(newPage, obj).then(function(response) {
                if (response.status == 200) {
                	if(response.data.data && response.data.data.length > 0) {
                        vm.totalRatings = response.data.data.length;
                        vm.ratingList = response.data.data;                       
                    }                     
                } 
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getRatingList()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getRatingList(1,'');
        }//END reset()

        /* to change active/inactive status of Rating */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            var data = { rating_id: status.rating_id, status: statusId };
            RatingService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /** to delete a Rating **/
        function deleteRating(obj) {   
            var id = obj;
            SweetAlert.swal({
               title: "Are you sure you want to delete this editor?",
               text: "",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
               cancelButtonText: "No",
               closeOnConfirm: false,
               closeOnCancel: true,
               html: true
            }, 
            function(isConfirm){ 
                if (isConfirm) {
                    RatingService.deleteRating(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            $state.reload();
                            return true;  
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END deleteRating()

        /* to show editor details in form */
        function editRating(userId) {
            vm.editFlag = true;
            vm.title = 'Edit';
            vm.user_id = userId;
            getRatingById(userId);
        }//END editRating()
        
        /* to get Rating information */
        function getRatingById(userId) {
            
        	RatingService.getRatingById(userId).then(function(response) {                
            	if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.editFlag = true;
                        vm.addForm.ratingId = response.data.data[0].rating_id;
                        vm.addForm.name = response.data.data[0].rating_name;
                        vm.addForm.status = response.data.data[0].status;                        
                    }
                }
            },function (error) {
                $state.go('backoffice.editors');
                toastr.error(error.data.message, 'Error');
            });
        }//END getRatingById();

        /* to get single role */
        function getSingleRating(rating_id) {
            RatingService.getRatingById(rating_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleRating = response.data.data[0];
                        vm.ratingForm = response.data.data[0];
                    } else {
                        toastr.error(response.data.message, 'Rating');
                        $state.go('backoffice.rating');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Role');
            });
        }//END getSingleRole();




        // vm.validatePhone = validatePhone;
        // vm.uploadFiles = uploadFiles;
        // vm.getIndex = getIndex;

        // vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        // vm.setAsDefaultEditor = setAsDefaultEditor;
        // vm.uploadDocs = uploadDocs;
        // if(!vm.editFlag) {
        //     vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        // }
        // vm.deleteUserFile = deleteUserFile;
      

        /* to get Rating information Old */
        // function getRatingByIdOld(userId) {
            
        // 	RatingService.getRatingById(userId).then(function(response) {
                
        //     	if (response.status == 200) {
        //             if (response.data.status == 1) {
        //                 vm.editFlag = true;
        //                 vm.addForm.ratingId = response.data.data[0].rating_id;
        //                 vm.addForm.name = response.data.data[0].rating_name;
        //                 return false;
        //                 vm.editFlag = true;
        //                 vm.addForm.name = response.data.user_details.firstname;
        //                 vm.addForm.company_name = response.data.user_details.company_name;
        //                 vm.addForm.address = (response.data.user_details.address == 'undefined') ? '' : response.data.user_details.address;
        //                 vm.addForm.phone = (response.data.user_details.phone == 'undefined') ? '' : response.data.user_details.phone;
        //                 vm.addForm.email = response.data.user_details.email;
        //                 vm.addForm.social_security = (response.data.user_details.social_security == 'undefined') ? '' : response.data.user_details.social_security;
        //                 vm.addForm.ein = (response.data.user_details.ein == 'undefined') ? '' : response.data.user_details.ein;
        //                 vm.addForm.profile_image = (response.data.user_details.profile_image == 'undefined') ? '' : response.data.user_details.profile_image;
        //                 vm.addForm.createdAt = moment(response.data.user_details.created_at).format('MMMM Do YYYY, h:mm:ss a');
        //                 vm.addForm.userId = response.data.user_details.user_id;
        //                 vm.addForm.roleId = response.data.user_details.role_id;
        //                 vm.addForm.isDefaultEditor = response.data.user_details.is_default_editor;
        //                 vm.addForm.password = response.data.user_details.password;
        //                 vm.profile_image = response.data.user_details.profile_image;
        //                 // if(vm.addForm.profile_image != '' && vm.addForm.profile_image != undefined) {
        //                 // 	vm.profile_image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/avatar/thumbs/' + vm.addForm.profile_image;
        //             	// } else {
        //             	// 	vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        //             	// }
        //                 if(response.data.user_files.length > 0) {
        //                     vm.userFiles = [];
        //                     angular.forEach(response.data.user_files, function(value, key) {
        //                         var image_path = '';
        //                         if(value.type == 2) {
        //                             image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/files/' + value.file_name;
        //                         }
        //                         vm.userFiles.push({
        //                             'id':value.id,
        //                             'original_name':value.original_name+'.'+value.extension,
        //                             'size':formatBytes(value.size),
        //                             'type':value.type,
        //                             'image_path':image_path,
        //                             'extension':value.extension
        //                         });

        //                     });
        //                 }

        //                 if(response.data.user_details.status == 0) {
        //                     vm.addForm.status = '2';
        //                 } else {
        //                     vm.addForm.status = '1';
        //                 }
        //             }
        //         }
        //     },function (error) {
        //         $state.go('backoffice.editors');
        //         toastr.error(error.data.message, 'Error');
        //     });
        // }//END getRatingById();



        /* to convert file size to KB,MB,GB */
        // function formatBytes(bytes) {
        //     if(bytes < 1024) return bytes + " Bytes";
        //     else if(bytes < 1048576) return(bytes / 1024).toFixed(2) + " KB";
        //     else if(bytes < 1073741824) return(bytes / 1048576).toFixed(2) + " MB";
        //     else return(bytes / 1073741824).toFixed(2) + " GB";
        // }//END formatBytes()

        /* to accept only number */
        // function validatePhone(number) {
        // 	return sharedService.validatePhoneNumber(number);
        // }//END validatePhone()

        // function uploadFiles(files, errFiles) {
	    // 	vm.addForm.profile_image = files[0];
	    // 	var reader = new FileReader();
        //     reader.readAsDataURL(files[0]);
        //     reader.onloadend = function (event) {
        //         $scope.$apply(function ($scope) {
        //         	vm.profile_image_path = event.target.result;
        //         });
        //     }
	    // }

	    // $scope.uploadDocument = function(files, errFiles) {
	    // 	vm.addForm.document_file = files[0];
	    // }
                        
        //retrive indexing
        // function getIndex(newPage, length){
        //     var index = sharedService.indexPerPage(newPage, length, vm.editorsPerPage);
        //                 vm.fromIndex = index.fromIndex;
        //                 vm.toIndex = index.toIndex;
        // }//END getIndex()
       
        /* to set a editor as default */
        // function setAsDefaultEditor(event) {
        //     var id = vm.addForm.userId;
        //     SweetAlert.swal({
        //        title: "Are you sure you want to set this editor as default?",
        //        text: "",
        //        type: "warning",
        //        showCancelButton: true,
        //        confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",
        //        cancelButtonText: "No",
        //        closeOnConfirm: false,
        //        closeOnCancel: true,
        //        html: true
        //     }, 
        //     function(isConfirm){ 
        //         if (isConfirm) {
        //             RatingService.setAsDefaultEditor(id).then(function(response) {
        //                 if (response.data.status == 1) {
        //                     SweetAlert.swal("Success!", response.data.message, "success");
        //                     vm.addForm.isDefaultEditor = 1;
        //                 }
        //             },function (error) {
        //                 toastr.error(error.data.error, 'Error');
        //             });
        //         }
        //     });
        // }//END setAsDefaultEditor()

        // $scope.uploadMultipleDocument = function(files, errFiles) {
        //     vm.documents = files;
        //     vm.addForm.documents = files;
        // }

        // function uploadDocs() {
        //     $rootScope.$emit("StartLoader", {});
        //     var data = {documents:vm.documents,user_id:vm.user_id}
        //     RatingService.uploadDocs(data).then(function(response) {
        //         if(response.data.status == 1) {
        //             vm.userFiles = [];
        //             angular.forEach(response.data.files, function(value, key) {
        //                 var image_path = '';
        //                 if(value.type == 2) {
        //                     image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/files/' + value.file_name;
        //                 }
        //                 vm.userFiles.push({
        //                     'id':value.id,
        //                     'original_name':value.original_name+'.'+value.extension,
        //                     'size':formatBytes(value.size),
        //                     'type':value.type,
        //                     'image_path':image_path,
        //                     'extension':value.extension
        //                 });

        //             });
        //             toastr.success(response.data.message, 'Success');
        //         } else {
        //             toastr.error(response.data.error, 'Error');
        //         }
        //     },function (error) {
        //         toastr.error(error.data.message, 'Error');
        //     }).finally(function () {
        //         $rootScope.$emit("CloseLoader", {});
        //     }); 
        // }

        /** to delete a user file **/
        // function deleteUserFile(obj,index) {
        //     var id = obj.id;
        //     SweetAlert.swal({
        //        title: "Are you sure you want to delete this file?",
        //        text: "",
        //        type: "warning",
        //        showCancelButton: true,
        //        confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
        //        cancelButtonText: "No",
        //        closeOnConfirm: false,
        //        closeOnCancel: true,
        //        html: true
        //     }, 
        //     function(isConfirm){ 
        //         if (isConfirm) {
        //             RatingService.deleteUserFile(id).then(function(response) {
        //                 if (response.data.status == 1) {
        //                     SweetAlert.swal("Deleted!", response.data.message, "success");
        //                     vm.userFiles.splice(index, 1);
        //                 }
        //             },function (error) {
        //                 toastr.error(error.data.error, 'Error');
        //             });
        //         }
        //     });
        // }//END deleteUserFile()        

    }

}());