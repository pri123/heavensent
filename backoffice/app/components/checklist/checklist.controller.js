(function () {
    "use strict";
    angular.module('checklistApp')
        .controller('ChecklistController', ChecklistController);

    ChecklistController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ChecklistService', 'toastr', 'SweetAlert', '$uibModal'];

    function ChecklistController($scope, $rootScope, $state, $location, ChecklistService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getChecklistList = getChecklistList;
        vm.getSingleChecklist = getSingleChecklist;
        vm.saveChecklist = saveChecklist;
        vm.deleteChecklist = deleteChecklist;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Checklist';
        vm.totalChecklist = 0;
        vm.checklistPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.checklistForm = { checklist_id: '' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.checklist');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleChecklist(path[3]);
            }
        }

      
        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getChecklistList(1, '');
        }//END sort()

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()


        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getChecklistList(newPage, searchInfo);
        }//END changePage();

        /* to save saveChecklist after add and edit  */
        function saveChecklist() {
            ChecklistService.saveChecklist(vm.checklistForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'CHECKLIST');
                        $state.go('backoffice.checklist');
                    } else {
                        toastr.error(response.data.message, 'CHECKLIST');
                    }
                } else {
                    toastr.error(response.data.message, 'CHECKLIST');
                }
            }, function (error) {
                toastr.error('Internal server error', 'CHECKLIST');
            });
        }//END savechecklist();

        /* to get getChecklistList list */
        function getChecklistList(newPage, obj) {
            vm.totalChecklist = 0;
            vm.checklistList = [];
            ChecklistService.getChecklist(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.checklist && response.data.checklist.length > 0) {
                        vm.totalChecklist = response.data.total;
                        vm.checklistList = response.data.checklist;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getChecklistList();

        /* to get getSingleChecklist checklist */
        function getSingleChecklist(checklist_id) {
            ChecklistService.getSingleChecklist(checklist_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleChecklist = response.data.checklist;
                        vm.checklistForm = response.data.checklist;
                    } else {
                        toastr.error(response.data.message, 'CHECKLIST');
                        $state.go('backoffice.checklist');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'CHECKLIST');
            });
        }//END getSingleChecklist();

        /** to delete a checklist **/
        function deleteChecklist(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Checklist?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    ChecklistService.deleteChecklist(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.demoList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'CHECKLIST');
                    });
                }
            });
        }//END deleteChecklist();

        /* to change active/inactive status of checklist */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { checklist_id: status.checklist_id, status: statusId };
            ChecklistService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()        

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getChecklistList(1, '');
        }//END reset();
    }

}());
