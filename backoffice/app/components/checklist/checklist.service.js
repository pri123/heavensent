(function () {
    "use strict";
    angular
        .module('checklistApp')
        .service('ChecklistService', ChecklistService);

    ChecklistService.$inject = ['$http', 'APPCONFIG', '$q'];

    function ChecklistService($http, APPCONFIG, $q) {

        /* save checklist */
        function saveChecklist(obj) {
            var URL = APPCONFIG.APIURL + 'checklist/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.checklist_id !== 'undefined' && obj.checklist_id !== '') {
                    requestData['checklist_id'] = obj.checklist_id;
                    URL = APPCONFIG.APIURL + 'checklist/edit';
                }

                if (typeof obj.checklist_name !== 'undefined' && obj.checklist_name !== '') {
                    requestData['checklist_name'] = obj.checklist_name;
                }
                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }                
            }

            return this.runHttp(URL, requestData);
        } //END saveChecklist();

        /* to get all checklist */
        function getChecklist(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'checklist';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.checklist_id !== 'undefined' && obj.checklist_id !== '') {
                    requestData['checklist_id'] = parseInt(obj.checklist_id);
                }

                if (typeof obj.checklist_name !== 'undefined' && obj.checklist_name !== '') {
                    requestData['checklist_name'] = obj.checklist_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }                
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getChecklist();

        /* to get single checklist */
        function getSingleChecklist(checklist_id) {
            var URL = APPCONFIG.APIURL + 'checklist/view';
            var requestData = {};

            if (typeof checklist_id !== undefined && checklist_id !== '') {
                requestData['checklist_id'] = checklist_id;
            }

            return this.runHttp(URL, requestData);
        } //END getchecklistById();

        /* to delete a checklist from database */
        function deleteChecklist(checklist_id) {
            var URL = APPCONFIG.APIURL + 'checklist/delete';
            var requestData = {};

            if (typeof checklist_id !== undefined && checklist_id !== '') {
                requestData['checklist_id'] = checklist_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteChecklist();

        /* to change active/inactive status of worker user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'checklist/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.checklist_id != undefined && obj.checklist_id != "") {
                    requestData["checklist_id"] = obj.checklist_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()        

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getChecklist: getChecklist,
            getSingleChecklist: getSingleChecklist,
            saveChecklist: saveChecklist,
            deleteChecklist: deleteChecklist,
            changeStatus   : changeStatus
        }

    };//END ChecklistService()
}());
