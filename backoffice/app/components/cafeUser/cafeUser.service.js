(function () {
    "use strict";
    angular
        .module('cafeUserApp')
        .service('CafeUserService', CafeUserService);

    CafeUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function CafeUserService($http, APPCONFIG, $q) {

        /* save cafe user */
        function saveCafeUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.user_address !== undefined && obj.user_address !== '') {
                    requestData['address'] = obj.user_address;
                }

                if (typeof obj.user_email !== undefined && obj.user_email !== '') {
                    requestData['email'] = obj.user_email;
                }

                if (typeof obj.user_name !== undefined && obj.user_name !== '') {
                    requestData['name'] = obj.user_name;
                }

                if (typeof obj.user_phone !== undefined && obj.user_phone !== '') {
                    requestData['phone'] = obj.user_phone;
                }

                if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                    requestData['username'] = obj.user_username;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveCafeUser();

        /* to get all cafe user  */
        function getCafeUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getCafeUser();

        /* to get single role */
        function getSingleCafeUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getCafeUserById();

        /* to delete a cafe from database */
        function deleteCafeUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteCafeUser();

        /* to change active/inactive status of cafe user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'user/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change active/inactive suspend of cafe user */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.is_suspend != undefined || obj.is_suspend != "") {
                    requestData["is_suspend"] = obj.is_suspend;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getCafeUser: getCafeUser,
            getSingleCafeUser: getSingleCafeUser,
            saveCafeUser: saveCafeUser,
            deleteCafeUser: deleteCafeUser,
            changeStatus: changeStatus,
            changeSuspend: changeSuspend
        }

    };//END CafeUserService()
}());
