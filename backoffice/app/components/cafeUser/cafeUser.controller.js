(function () {
    "use strict";
    angular.module('cafeUserApp')
        .controller('CafeUserController', CafeUserController);

        CafeUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'CafeUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function CafeUserController($scope, $rootScope, $state, $location, CafeUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getCafeUserList = getCafeUserList;
        vm.getSingleCafeUser = getSingleCafeUser;
        vm.saveCafeUser = saveCafeUser;
        vm.deleteCafeUser = deleteCafeUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Cafe User';
        vm.totalCafeUser = 0;
        vm.cafeUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.cafeUserForm = { user_id: '', usertypeFlag: '2', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.cafeUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleCafeUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting cafe user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getCafeUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getCafeUserList(newPage, searchInfo);
        }//END changePage();

        /* to save cafe user after add and edit  */
        function saveCafeUser() {
            CafeUserService.saveCafeUser(vm.cafeUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Cafe User');
                        $state.go('backoffice.cafeUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Cafe User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Cafe User');
                            }else{
                                toastr.error(response.data.message, 'Cafe User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Cafe User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Cafe User');
            });
        }//END saveCafeUser();

        /* to get cafe user list */
        function getCafeUserList(newPage, obj) {
            vm.totalCafeUser = 0;
            vm.cafeUserList = [];
            CafeUserService.getCafeUser(newPage, obj, vm.orderInfo, '2').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalCafeUser = response.data.total;
                        vm.cafeUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getCafeUserList();

        /* to get single cafe user */
        function getSingleCafeUser(user_id) { 
            var UserType = 2;           
            CafeUserService.getSingleCafeUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleCafeUser = response.data.data[0];
                        // vm.cafeUserForm = response.data.data[0];
                        vm.cafeUserForm.user_id = response.data.data[0].user_id;
                        vm.cafeUserForm.usertype = response.data.data[0].user_role_id;
                        vm.cafeUserForm.registertype = response.data.data[0].user_registertype;
                        vm.cafeUserForm.user_name = response.data.data[0].cafeusers_name;
                        vm.cafeUserForm.user_email = response.data.data[0].cafeusers_email;
                        vm.cafeUserForm.user_phone = response.data.data[0].cafeusers_phone;
                        vm.cafeUserForm.user_address = response.data.data[0].cafeusers_address;
                        vm.cafeUserForm.user_username = response.data.data[0].user_username;
                        vm.cafeUserForm.status = response.data.data[0].status; 
                        vm.cafeUserForm.userusername = true;
                    } else {
                        toastr.error(response.data.message, 'Cafe User');
                        $state.go('backoffice.cafeUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Cafe User');
            });
        }//END getSingleCafeUser();

        /** to delete a cafe user **/
        function deleteCafeUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Cafe User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    CafeUserService.deleteCafeUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.cafeUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Cafe User');
                    });
                }
            });
        }//END deleteCafeUser();

        /* to change active/inactive status of cafe user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            CafeUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this Cafe User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    CafeUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getCafeUserList(1, '');
        }//END reset();
    }

}());
