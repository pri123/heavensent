(function () {

    angular.module('cafeUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var cafeUserPath = 'app/components/cafeUser/';
        $stateProvider
            .state('backoffice.cafeUser', {
                url: 'cafeUser',
                views: {
                    'content@backoffice': {
                        templateUrl: cafeUserPath + 'views/index.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafeUser'
                    }
                }
            })
            .state('backoffice.cafeUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: cafeUserPath + 'views/form.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafeUser'
                    }
                }
            })
            .state('backoffice.cafeUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: cafeUserPath + 'views/form.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafeUser'
                    }
                }
            })
            .state('backoffice.cafeUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: cafeUserPath + 'views/view.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafeUser'
                    }
                }
            })
    }

}());