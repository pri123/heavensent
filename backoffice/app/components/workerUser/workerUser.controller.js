(function () {
    "use strict";
    angular.module('workerUserApp')
        .controller('WorkerUserController', WorkerUserController);

    WorkerUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'WorkerUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function WorkerUserController($scope, $rootScope, $state, $location, WorkerUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getWorkerUserList = getWorkerUserList;
        vm.getSingleWorkerUser = getSingleWorkerUser;
        vm.saveWorkerUser = saveWorkerUser;
        vm.deleteWorkerUser = deleteWorkerUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.getAllCafeUser = getAllCafeUser;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Worker User';
        vm.totalWorkerUser = 0;
        vm.workerUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.workerUserForm = { user_id: '', usertypeFlag: '3', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.workerUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleWorkerUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting worker user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getWorkerUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getWorkerUserList(newPage, searchInfo);
        }//END changePage();

        /* to save worker user after add and edit  */
        function saveWorkerUser() {
            WorkerUserService.saveWorkerUser(vm.workerUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Worker User');
                        $state.go('backoffice.workerUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Worker User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Worker User');
                            }else{
                                toastr.error(response.data.message, 'Worker User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Worker User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker User');
            });
        }//END saveWorkerUser();

        /* to get worker user list */
        function getWorkerUserList(newPage, obj) {
            vm.totalWorkerUser = 0;
            vm.workerUserList = [];
            WorkerUserService.getWorkerUser(newPage, obj, vm.orderInfo, '3').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalWorkerUser = response.data.total;
                        vm.workerUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getWorkerUserList();

        /* to get single worker user */
        function getSingleWorkerUser(user_id) { 
            var UserType = 3;           
            WorkerUserService.getSingleWorkerUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleWorkerUser = response.data.data[0];
                        // console.log(response.data.data[0]);
                        // vm.cafeUserForm = response.data.data[0];
                        vm.workerUserForm.user_id = response.data.data[0].user_id;
                        vm.workerUserForm.usertype = response.data.data[0].user_role_id;
                        vm.workerUserForm.registertype = response.data.data[0].user_registertype;
                        vm.workerUserForm.user_name = response.data.data[0].workers_name;
                        vm.workerUserForm.user_email = response.data.data[0].workers_email;
                        vm.workerUserForm.user_phone = response.data.data[0].workers_phone;
                        vm.workerUserForm.user_address = response.data.data[0].workers_address;
                        vm.workerUserForm.user_username = response.data.data[0].user_username;
                        vm.workerUserForm.status = response.data.data[0].status;
                        // vm.workerUserForm.cafeID = response.data.data[0].cafeuser_id;
                        vm.workerUserForm.userusername = true; 
                    } else {
                        toastr.error(response.data.message, 'Worker User');
                        $state.go('backoffice.workerUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Worker User');
            });
        }//END getSingleWorkerUser();

        /** to delete a worker user **/
        function deleteWorkerUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this worker User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    WorkerUserService.deleteWorkerUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.workerUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Worker User');
                    });
                }
            });
        }//END deleteWorkerUser();

        /* to change active/inactive status of worker user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            WorkerUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this worker User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    WorkerUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getWorkerUserList(1, '');
        }//END reset();

        function getAllCafeUser(){
            vm.cafeUserList = [];
            WorkerUserService.getAllCafeUser('2').then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.cafeUserList = response.data.data;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }        
        

    }

}());
