(function () {

    angular.module('workerUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var workerUserPath = 'app/components/workerUser/';
        $stateProvider
            .state('backoffice.workerUser', {
                url: 'workerUser',
                views: {
                    'content@backoffice': {
                        templateUrl: workerUserPath + 'views/index.html',
                        controller: 'WorkerUserController',
                        controllerAs: 'workerUser'
                    }
                }
            })
            .state('backoffice.workerUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: workerUserPath + 'views/form.html',
                        controller: 'WorkerUserController',
                        controllerAs: 'workerUser'
                    }
                }
            })
            .state('backoffice.workerUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: workerUserPath + 'views/form.html',
                        controller: 'WorkerUserController',
                        controllerAs: 'workerUser'
                    }
                }
            })
            .state('backoffice.workerUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: workerUserPath + 'views/view.html',
                        controller: 'WorkerUserController',
                        controllerAs: 'workerUser'
                    }
                }
            })
    }

}());