(function () {
    "use strict";
    angular.module('backofficeApp')
        .controller('BackofficeController', BackofficeController);

    BackofficeController.$inject = ['$scope', '$location', '$state', 'authServices', '$rootScope', 'toastr', 'SweetAlert'];

    function BackofficeController($scope, $location, $state, authServices, $rootScope, toastr, SweetAlert) {
        var vm = this;

        vm.isActive = isActive;
        vm.logout = logout;
        vm.getCurrentState = getCurrentState;
        vm.getProfileInfo = getProfileInfo;
        vm.updateProfile = updateProfile;
        vm.changePassword = changePassword;
        vm.profileForm = {}; vm.passwordForm = {};

        if($state.current.name == 'backoffice') {
            $rootScope.headerTitle = 'Dashboard';
        } else if($state.current.name == 'backoffice.changePassword') {
            $rootScope.headerTitle = 'Change Password';
        }

        console.log($state.current.name, $rootScope.headerTitle);

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu

        /* to get profile details */
        function getProfileInfo() {
            console.log($rootScope.user);
            var user = $rootScope.user;
            authServices.getUserProfile(user.user_id).then(function (response) {
                if (response.data.status == 1) {
                    vm.profileForm.user_id = response.data.user_detail.user_id;
                    vm.profileForm.name = response.data.user_detail.firstname + " " + response.data.user_detail.lastname;
                    vm.profileForm.company_name = response.data.user_detail.company_name;
                    vm.profileForm.address = response.data.user_detail.address;
                    vm.profileForm.phone = response.data.user_detail.phone;
                    vm.profileForm.email = response.data.user_detail.email;
                    vm.profileForm.profile_image = response.data.user_detail.profile_image;
                    if (vm.profileForm.profile_image != '' && vm.profileForm.profile_image != undefined) {
                        vm.profile_image_path = 'https://s3.amazonaws.com/' + $rootScope.bucketName + '/uploads/users/' + vm.profileForm.user_id + '/avatar/thumbs/' + vm.profileForm.profile_image;
                    } else {
                        vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error('Something went wrong', 'Error');
            });
        }//END getProfileInfo()

        /* to upload profile image */
        $scope.uploadProfileImage = function (files, errFiles) {
            vm.profileForm.profile_image = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            reader.onloadend = function (event) {
                $scope.$apply(function ($scope) {
                    vm.profile_image_path = event.target.result;
                });
            }
        }//END uploadProfileImage()

        /* to update profile data */
        function updateProfile() {
            authServices.updateProfile(vm.profileForm).then(function (response) {
                if (response.data.status == 1) {
                    $rootScope.user.profile_image = response.data.profile.profile_image;
                    $rootScope.user.full_name = response.data.profile.full_name;
                    toastr.success(response.data.message, 'Success');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error('Something went wrong', 'Error');
            });
        }//END updateProfile()

        /* to change passowrd for admin */
        function changePassword() {
            vm.passwordForm.user_id = $rootScope.user.id;
            authServices.changePassword(vm.passwordForm).then(function (response) {
                if (response.data.status == 1) {
                    toastr.success(response.data.message, 'Success');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error(error.data.message, 'Error');
            });
        }//END changePassword()        

        function logout() {
            SweetAlert.swal({
                title: "Sure you want to Logout?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f62d51", confirmButtonText: "Yes, Logout!",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $state.go('auth.login');
                    // authServices.logout().then(function (response) {
                    //     if (response.status == 200) {
                    //         $state.go('auth.login');
                    //     }
                    // }).catch(function (response) {
                    //     toastr.error("Unable to Logout!<br/>Try again later", "Error");
                    // });
                }
            });
        }

        function getCurrentState() {
            return $state.current.name;
        }

        $rootScope.$on("StartLoader", function () {
            $rootScope.loader = true;
        });

        $rootScope.$on("CloseLoader", function () {
            $rootScope.loader = false;
        });

    }

}());