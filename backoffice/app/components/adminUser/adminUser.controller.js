(function () {
    "use strict";
    angular.module('adminUserApp')
        .controller('AdminUserController', AdminUserController);

    AdminUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AdminUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function AdminUserController($scope, $rootScope, $state, $location, AdminUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getAdminUserList = getAdminUserList;
        vm.getSingleAdminUser = getSingleAdminUser;
        vm.saveAdminUser = saveAdminUser;
        vm.deleteAdminUser = deleteAdminUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Admin User';
        vm.totalAdminUser = 0;
        vm.adminUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.adminUserForm = { user_id: '', usertypeFlag: '1', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.adminUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleAdminUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting admin user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAdminUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAdminUserList(newPage, searchInfo);
        }//END changePage();

        /* to save admin user after add and edit  */
        function saveAdminUser() {
            AdminUserService.saveAdminUser(vm.adminUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Admin User');
                        $state.go('backoffice.adminUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Admin User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Admin User');
                            }else{
                                toastr.error(response.data.message, 'Admin User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Admin User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Admin User');
            });
        }//END saveAdminUser();

        /* to get admin user list */
        function getAdminUserList(newPage, obj) {
            vm.totalAdminUser = 0;
            vm.adminUserList = [];
            AdminUserService.getAdminUser(newPage, obj, vm.orderInfo, '1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalAdminUser = response.data.total;
                        vm.adminUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAdminUserList();

        /* to get single admin user */
        function getSingleAdminUser(user_id) { 
            var UserType = 1;           
            AdminUserService.getSingleAdminUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleAdminUser = response.data.data[0];
                        // vm.cafeUserForm = response.data.data[0];
                        vm.adminUserForm.user_id = response.data.data[0].user_id;
                        vm.adminUserForm.usertype = response.data.data[0].user_role_id;
                        vm.adminUserForm.registertype = response.data.data[0].user_registertype;
                        vm.adminUserForm.user_name = response.data.data[0].admin_name;
                        vm.adminUserForm.user_email = response.data.data[0].admin_email;
                        vm.adminUserForm.user_phone = response.data.data[0].admin_phone;
                        vm.adminUserForm.user_address = response.data.data[0].admin_address;
                        vm.adminUserForm.user_username = response.data.data[0].user_username;
                        vm.adminUserForm.status = response.data.data[0].status; 
                        vm.adminUserForm.userusername = true;
                    } else {
                        toastr.error(response.data.message, 'Admin User');
                        $state.go('backoffice.adminUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Admin User');
            });
        }//END getSingleAdminUser();

        /** to delete a admin user **/
        function deleteAdminUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Admin User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AdminUserService.deleteAdminUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.adminUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Admin User');
                    });
                }
            });
        }//END deleteAdminUser();

        /* to change active/inactive status of admin user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            AdminUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this Admin User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    AdminUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAdminUserList(1, '');
        }//END reset();
    }

}());
