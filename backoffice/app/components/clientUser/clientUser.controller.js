(function () {
    "use strict";
    angular.module('clientUserApp')
        .controller('ClientUserController', ClientUserController);

    ClientUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ClientUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function ClientUserController($scope, $rootScope, $state, $location, ClientUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getClientUserList = getClientUserList;
        vm.getSingleClientUser = getSingleClientUser;
        vm.saveClientUser = saveClientUser;
        vm.deleteClientUser = deleteClientUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Client User';
        vm.totalClientUser = 0;
        vm.clientUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.clientUserForm = { user_id: '', usertypeFlag: '5', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.clientUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleClientUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting client user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getClientUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getClientUserList(newPage, searchInfo);
        }//END changePage();

        /* to save client user after add and edit  */
        function saveClientUser() {
            ClientUserService.saveClientUser(vm.clientUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Client User');
                        $state.go('backoffice.clientUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Client User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Client User');
                            }else{
                                toastr.error(response.data.message, 'Client User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Client User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Client User');
            });
        }//END saveClientUser();

        /* to get client user list */
        function getClientUserList(newPage, obj) {
            vm.totalClientUser = 0;
            vm.clientUserList = [];
            ClientUserService.getClientUser(newPage, obj, vm.orderInfo, '5').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalClientUser = response.data.total;
                        vm.clientUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getClientUserList();

        /* to get single client user */
        function getSingleClientUser(user_id) { 
            var UserType = 5;           
            ClientUserService.getSingleClientUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleClientUser = response.data.data[0];
                        // vm.cafeUserForm = response.data.data[0];
                        vm.clientUserForm.user_id = response.data.data[0].user_id;
                        vm.clientUserForm.usertype = response.data.data[0].user_role_id;
                        vm.clientUserForm.registertype = response.data.data[0].user_registertype;
                        vm.clientUserForm.user_name = response.data.data[0].clients_name;
                        vm.clientUserForm.user_email = response.data.data[0].clients_email;
                        vm.clientUserForm.user_phone = response.data.data[0].clients_phone;
                        vm.clientUserForm.user_address = response.data.data[0].clients_address;
                        vm.clientUserForm.user_username = response.data.data[0].user_username;
                        vm.clientUserForm.status = response.data.data[0].status; 
                        vm.clientUserForm.userusername = true;
                    } else {
                        toastr.error(response.data.message, 'client User');
                        $state.go('backoffice.clientUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'client User');
            });
        }//END getSingleClientUser();

        /** to delete a client user **/
        function deleteClientUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Client User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    ClientUserService.deleteClientUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.clientUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Client User');
                    });
                }
            });
        }//END deleteClientUser();

        /* to change active/inactive status of client user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            ClientUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this Client User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    ClientUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getClientUserList(1, '');
        }//END reset();
    }

}());
