(function () {

    angular.module('clientUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var clientUserPath = 'app/components/clientUser/';
        $stateProvider
            .state('backoffice.clientUser', {
                url: 'clientUser',
                views: {
                    'content@backoffice': {
                        templateUrl: clientUserPath + 'views/index.html',
                        controller: 'ClientUserController',
                        controllerAs: 'clientUser'
                    }
                }
            })
            .state('backoffice.clientUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: clientUserPath + 'views/form.html',
                        controller: 'ClientUserController',
                        controllerAs: 'clientUser'
                    }
                }
            })
            .state('backoffice.clientUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: clientUserPath + 'views/form.html',
                        controller: 'ClientUserController',
                        controllerAs: 'clientUser'
                    }
                }
            })
            .state('backoffice.clientUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: clientUserPath + 'views/view.html',
                        controller: 'ClientUserController',
                        controllerAs: 'clientUser'
                    }
                }
            })
    }

}());