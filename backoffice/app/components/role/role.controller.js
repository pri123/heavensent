(function () {
    "use strict";
    angular.module('roleApp')
        .controller('RoleController', RoleController);

    RoleController.$inject = ['$scope', '$rootScope', '$state', '$location', 'RoleService', 'toastr', 'SweetAlert', '$uibModal'];

    function RoleController($scope, $rootScope, $state, $location, RoleService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getRolesList = getRolesList;
        vm.getSingleRole = getSingleRole;
        vm.saveRole = saveRole;
        vm.deleteRole = deleteRole;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Roles';
        vm.totalRoles = 0;
        vm.rolesPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.roleForm = { role_id: '' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.role');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleRole(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getRolesList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getRolesList(newPage, searchInfo);
        }//END changePage();

        /* to save role after add and edit  */
        function saveRole() {
            RoleService.saveRole(vm.roleForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Role');
                        $state.go('backoffice.role');
                    } else {
                        toastr.error(response.data.message, 'Role');
                    }
                } else {
                    toastr.error(response.data.message, 'Role');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Role');
            });
        }//END saveRole();

        /* to get role list */
        function getRolesList(newPage, obj) {
            vm.totalRoles = 0;
            vm.roleList = [];
            RoleService.getRoles(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.roles && response.data.roles.length > 0) {
                        vm.totalRoles = response.data.total;
                        vm.roleList = response.data.roles;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getRolesList();

        /* to get single role */
        function getSingleRole(role_id) {
            RoleService.getSingleRole(role_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleRole = response.data.role;
                        vm.roleForm = response.data.role;
                    } else {
                        toastr.error(response.data.message, 'Role');
                        $state.go('backoffice.role');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Role');
            });
        }//END getSingleRole();

        /** to delete a role **/
        function deleteRole(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Role?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    RoleService.deleteRole(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.roleList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Role');
                    });
                }
            });
        }//END deleteRole();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getRolesList(1, '');
        }//END reset();
    }

}());
