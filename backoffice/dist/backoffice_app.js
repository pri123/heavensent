(function () {
    'use strict';
    var authApp = angular.module('authApp', []);

    authenticateUser1.$inject = ['authServices', '$state']

    function authenticateUser1(authServices, $state) {
        return authServices.checkValidUser(false);
    } //END authenticateUser()

    authApp.config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('auth', {
                url: '/auth',
                views: {
                    '': {
                        templateUrl: 'app/layouts/auth/layout.html'
                    },
                    'content@auth': {
                        templateUrl: 'app/components/auth/login.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                },
                resolve: {
                //    auth1: authenticateUser1
                }
            })
            .state('auth.login', {
                url: '/login',
                views: {
                    'content@auth': {
                        templateUrl: 'app/components/auth/login.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                },
                resolve: {
                    // auth1: authenticateUser1
                }
            })
            .state('auth.forgotpassword', {
                url: '/forgot-password',
                views: {
                    'content@auth': {
                        templateUrl: 'app/components/auth/forgotpassword.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                },
                resolve: {
                //    auth1: authenticateUser1
                }
            });
    });
})();
(function () {
    "use strict";

    angular
        .module('authApp')
        .service('authServices', authServices);

    authServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    var someValue = '';

    function authServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

        self.checkLogin = checkLogin;
        self.checkForgotPassword = checkForgotPassword;
        self.checkValidUser = checkValidUser;
        self.setAuthToken = setAuthToken;
        self.getAuthToken = getAuthToken;
        self.saveUserInfo = saveUserInfo;
        self.checkValidUrl = checkValidUrl;
        self.getUserProfile = getUserProfile;
        self.updateProfile = updateProfile;
        self.changePassword = changePassword;
        self.logout = logout;

        //to check if user is login and set user details in rootscope
        function checkLogin(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login';
            var requestData = {};

            if (typeof obj.email !== undefined && obj.email !== '') {
                requestData['username'] = obj.email;
            }
            if (typeof obj.password !== undefined && obj.password !== '') {
                requestData['password'] = obj.password;
            }
            if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                requestData['usertype'] = obj.usertypeFlag;
            }
            if (typeof obj.device_token !== undefined && obj.device_token !== '') {
                requestData['device_token'] = obj.device_token;
            }
            if (typeof obj.device_type !== undefined && obj.device_type !== '') {
                requestData['device_type'] = obj.device_type;
            }
            
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                $rootScope.isLogin = true;
                deferred.resolve(response);
            }, function (response) {
                $rootScope.isLogin = false;
                $rootScope.$broadcast('auth:login:required');
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkLogin();


        function checkValidUser(isAuth) {
            var URL = APPCONFIG.APIURL + 'validate-user';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("type", 'admin');
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined,
                    'Access-Token': getAuthToken()
                }
            }).then(function (response) {
                $rootScope.isLogin = true;
                saveUserInfo(response.data);
                if (isAuth === false) {
                    $rootScope.$broadcast('auth:login:success');
                } else {
                    checkValidUrl(response.data.user_detail.role_id, $location.$$path);
                }
                deferred.resolve();
            }).catch(function (response) {
                $rootScope.isLogin = false;
                if (isAuth === false) {
                    deferred.resolve();
                } else {
                    $rootScope.$broadcast('auth:login:required');
                    deferred.resolve();
                }
            });
            return deferred.promise;
        } //END checkValidUser();

        function checkForgotPassword(email) {
            var URL = APPCONFIG.APIURL + 'forgot-password';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("email", email);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkForgotPassword();

        function setAuthToken(userInfo) {
            // localStorage.setItem('token', userInfo.headers('Access-token'));
            localStorage.setItem('token', userInfo);
        } //END setAuthToken();

        function getAuthToken() {
            if (localStorage.getItem('token') != undefined && localStorage.getItem('token') != null)
                return localStorage.getItem('token');
            else
                return null;
        } //END getAuthToken();

        function saveUserInfo(data) {
            var user = {};
            if (data.status == 1) {
                user = data.user_detail;
                $rootScope.user = user;
                $rootScope.baseUrl = APPCONFIG.APIURL;
                $rootScope.bucketName = APPCONFIG.BUCKETNAME;
                //$cookies.putObject("token_key", localStorage.getItem('token'));
            }
            return user;
        } //END saveUserInfo();

        function checkValidUrl(role, location) {
            var urlData = location.split('/');
            var actualUrl = urlData[1];
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'valid-url';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("role_id", role);
                    formData.append("url", actualUrl);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        deferred.resolve(response);
                    } else {
                        $state.go('backoffice.dashboard');
                    }
                }
            });
            return deferred.promise;
        }

        /* to get user profile data */
        function getUserProfile(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'view-profile';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END getUserProfile()

        /* to update profile data */
        function updateProfile(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'update-profile';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    var name = obj.name.split(" ");
                    obj.lastname = (name[1] === undefined) ? '' : name[1];
                    formData.append("firstname", name[0]);
                    formData.append("lastname", obj.lastname);
                    formData.append("company_name", obj.company_name);
                    formData.append("address", obj.address);
                    formData.append("phone", obj.phone);
                    formData.append("email", obj.email);
                    formData.append("user_id", obj.user_id);
                    formData.append("profile_image", obj.profile_image);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END updateProfile();

        /* to update profile data */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'change-password';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("old_password", obj.password);
                    formData.append("new_password", obj.new_password);
                    formData.append("c_password", obj.confirm_passowrd);
                    formData.append("user_id", obj.user_id);
                    formData.append("token", getAuthToken());
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END changePassword();

        function logout() {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'logout';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) { },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                localStorage.removeItem('token');
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END logout()  

        return self;
        
    };
})();
(function () {
    'use strict';
    angular.module('authApp').controller('AuthController', AuthController);

    AuthController.$inject = ['$rootScope', '$location', 'authServices', '$state', 'toastr'];

    function AuthController($rootScope, $location, authServices, $state, toastr) {
        var vm = this;
        vm.login = login;
        // vm.forgotPassword = forgotPassword;

        vm.user = { usertypeFlag: '1', device_token: ' ', device_type: '1' };
        // vm.user = { email: 'admin@mailinator.com', password: '123456' };
        $rootScope.bodyClass = '';

        function login(loginInfo) {
            // $state.go('backoffice.dashboard');
            authServices.checkLogin(loginInfo).then(function (response) {
                var token = response.data.data[0].device_token;
                if (response.status == 200) {
                    // authServices.setAuthToken(token); //Set auth token
                    $state.go('backoffice.dashboard');
                } else {
                    toastr.error(response.data.message, "Error");
                }
            }).catch(function (response) {
                toastr.error(response.data.error, "Error");
            });
        }; //END login()   

        // function forgotPassword(userInfo) {
        //     authServices.checkForgotPassword(userInfo).then(function (response) {
        //         if (response.status == 200) {
        //             toastr.success(response.data.message, "Forgot Password");
        //         } else {
        //             toastr.error(response.data.message, "Forgot Password");
        //         }
        //     }).catch(function (response) {
        //         toastr.error(response.data.message, "Forgot Password");
        //     });
        // }; //END forgotPassword() 
    };
}());
(function () {
    'use strict';
    angular.module('dashboardApp', []).controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', '$rootScope', '$state', '$location', 'authServices', 'dashboardService', 'toastr'];
    function DashboardController($scope, $rootScope, $state, $location, authServices, dashboardService, toastr) {
        var vm = this;
        vm.updateProfile = updateProfile;
        vm.changePassword = changePassword;
        vm.passwordValidator = passwordValidator;

        $rootScope.bodyClass = 'fix-header fix-sidebar card-no-border';
        $scope.user = $rootScope.user;
        
        function passwordValidator(password) {
            if (!password) return;
            if (password.length < 6) return "Password must be at least " + 6 + " characters long";
            if (!password.match(/[A-Z]/)) return "Password must have at least one capital letter";
            if (!password.match(/[0-9]/)) return "Password must have at least one number";

            return true;
        };

        function updateProfile(userInfo) {
            userInfo.token = $rootScope.user.token;
            dashboardService.updateProfile(userInfo).then(function (response) {
                if (response.status == 200) {
                    toastr.success(response.data.message, "Update Profile");
                } else {
                    toastr.error(response.data.message, "Update Profile");
                }
            }).catch(function (response) {
                toastr.error(response.data.message, "Update Profile");
            });
        }; //END updateProfile()

        function changePassword(changePasswordInfo) {
            changePasswordInfo.token = $rootScope.user.token;
            dashboardService.changePassword(changePasswordInfo).then(function (response) {
                if (response.status == 200 && response.data.status == 1) {
                    toastr.success(response.data.message, "Change Password");
                    $scope.changePasswordForm.reset();
                } else {
                    toastr.error(response.data.message, "Change Password");
                }
            }).catch(function (response) {
                toastr.error(response.data.message, "Change Password");
            });
        }; //END changePassword()
    };

}());
(function() {
    "use strict";
    angular
        .module('dashboardApp')
        .service('dashboardService', dashboardService);

    dashboardService.$inject = ['$http', 'APPCONFIG', '$q'];

    function dashboardService($http, APPCONFIG, $q) {
        return {
            updateProfile: updateProfile,
            changePassword: changePassword
        }

        /* update profile */
        function updateProfile(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'update-profile';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    formData.append("token", obj.token);
                    formData.append("firstname", obj.firstname);
                    formData.append("lastname", obj.lastname);
                    return formData;
                },
                headers: {
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END updateProfile();

        /* change password */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'change-password';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    formData.append("token", obj.token);
                    formData.append("oldpassword", obj.old_password);
                    formData.append("password", obj.new_password);
                    formData.append("cpassword", obj.confirm_password);
                    return formData;
                },
                headers: {
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END changePassword();



    }; //END dashboardService()
}());
(function () {
    'use strict';
    angular.module('ratingApp', []).config(config);

    function config($stateProvider, $urlRouterProvider) {
        var ratingPath = 'app/components/rating/';
        $stateProvider
            .state('backoffice.rating', {
                url: 'rating',
                views: {
                    'content@backoffice': {
                        templateUrl: ratingPath + 'views/index.html',
                        controller: 'RatingController',
                        controllerAs: 'rating'
                    }
                }                
            })
            .state('backoffice.rating.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: ratingPath + 'views/create.html',
                        controller: 'RatingController',
                        controllerAs: 'rating'
                    }
                }
            })
            .state('backoffice.rating.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: ratingPath + 'views/create.html',
                        controller: 'RatingController',
                        controllerAs: 'rating'
                    }
                }
            })
            .state('backoffice.rating.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: ratingPath + 'views/view.html',
                        controller: 'RatingController',
                        controllerAs: 'rating'
                    }
                }
            });
    }

}());
(function () {
    "use strict";

    angular
        .module('ratingApp')
        .service('RatingService', RatingService);

    RatingService.$inject = ['$http', 'APPCONFIG', '$q'];

    function RatingService($http, APPCONFIG, $q) {
        var self = this;
        self.getRatings = getRatings,
        self.deleteRating = deleteRating,
        self.saveRating = saveRating,
        self.getRatingById = getRatingById,
        self.changeStatus = changeStatus;
                
        // self.saveRoleOrder = saveRoleOrder;
        

        /* to get all Rating */
        function getRatings(pageNum, obj) {
            var deferred = $q.defer();

            var requestData = { pageNum };
            if (obj != undefined) {
                if (obj.id != undefined && obj.id != "") {
                    requestData["id"] = obj.id;
                }

                if (obj.name != undefined && obj.name != "") {
                    requestData["name"] = obj.name;
                }

                if (obj.created_at != undefined && obj.created_at != "") {
                    requestData["created_at"] = moment(obj.created_at).format("YYYY-MM-DD");
                }

                if (obj.status != undefined && obj.status != "") {
                    requestData["status"] = obj.status;
                }
            }

            $http({
                url: APPCONFIG.APIURL + 'rating',
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;

        }//END getRatings();
        
        /* to change active/inactive status of Rating */
        function changeStatus(obj) {
            var deferred = $q.defer();
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.rating_id != undefined && obj.rating_id != "") {
                    requestData["rating_id"] = obj.rating_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status_id"] = obj.status;
                }                
            }            
            $http({
                url: APPCONFIG.APIURL + 'rating/status',
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            
            return deferred.promise;
        }//END changeStatus()

        /* to delete a Rating from database */
        function deleteRating(id) {
            var deferred = $q.defer();
            
            var requestData = {};
            if (id != undefined) {
                requestData["rating_id"] = id;
            }   
            $http({
                url: APPCONFIG.APIURL + 'rating/delete',
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END deleteRating();

        /* to create new Rating */
        function saveRating(obj) {
            var deferred = $q.defer();
            var requestData = {};            
            if(obj.ratingId == 0){
                if (typeof obj.name != undefined && obj.name !== '') {
                    requestData["name"] = obj.name;
                }
                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }   
                $http({
                    url: APPCONFIG.APIURL + 'rating/add',
                    method: "POST",
                    data: requestData,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(error);
                });
            }else{
                if (obj.name != undefined) {
                    requestData["name"] = obj.name;
                }   
                if (obj.ratingId != undefined) {
                    requestData["rating_id"] = obj.ratingId;
                }
                $http({
                    url: APPCONFIG.APIURL + 'rating/edit',
                    method: "POST",
                    data: requestData,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(error);
                });
            }
            return deferred.promise;
        } //END saveRating();

        /* to get Rating by ID */
        function getRatingById(id) {
            var deferred = $q.defer();
            var requestData = {};
            if (id != undefined) {
                requestData["rating_id"] = id;
            }   
            $http({
                url: APPCONFIG.APIURL + 'rating/getRatingById',
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getRatingById();

       
       
       
        // return {
        //     saveEditor: saveEditor,
        //     getEditors: getEditors,
        //     getEditorById: getEditorById,
        //     deleteEditor: deleteEditor,
        //     setAsDefaultEditor: setAsDefaultEditor,
        //     uploadDocs: uploadDocs,
        //     deleteUserFile: deleteUserFile,
        //     changeStatus: changeStatus
        // };
      
        /* to set a editor as default */
        // function setAsDefaultEditor(id) {
        //     var deferred = $q.defer();
        //     var URL = APPCONFIG.APIURL + 'set-default-editor';
        //     $http({
        //         method: 'POST',
        //         url: URL,
        //         processData: false,
        //         transformRequest: function (data) {
        //             var formData = new FormData();
        //             id = (id === undefined) ? '' : id;
        //             formData.append("user_id", id);
        //             return formData;
        //         },
        //         headers: {
        //             'X-Requested-With': 'XMLHttpRequest',
        //             'Content-Type': undefined
        //         }
        //     }).then(function (response) {
        //         deferred.resolve(response);
        //     }, function (error) {
        //         deferred.reject(error);
        //     });
        //     return deferred.promise;
        // }//END setAsDefaultEditor()

        /* to upload multiple docs */
        // function uploadDocs(obj) {
        //     var deferred = $q.defer();
        //     var URL = APPCONFIG.APIURL + 'upload-documents';
        //     $http({
        //         method: 'POST',
        //         url: URL,
        //         processData: false,
        //         transformRequest: function (data) {
        //             var formData = new FormData();
        //             angular.forEach(obj.documents, function (value, key) {
        //                 formData.append("documents[]", obj.documents[key]);
        //             });

        //             if (obj.user_id != undefined && obj.user_id != '') {
        //                 formData.append("user_id", obj.user_id);
        //             }
        //             return formData;
        //         },
        //         headers: {
        //             'X-Requested-With': 'XMLHttpRequest',
        //             'Content-Type': undefined
        //         }
        //     }).then(function (response) {
        //         deferred.resolve(response);
        //     }, function (error) {
        //         deferred.reject(error);
        //     });
        //     return deferred.promise;
        // } //END uploadDocs();

        /* to delete a file from database */
        // function deleteUserFile(id) {
        //     var deferred = $q.defer();
        //     var URL = APPCONFIG.APIURL + 'delete-file';
        //     $http({
        //         method: 'POST',
        //         url: URL,
        //         processData: false,
        //         transformRequest: function (data) {
        //             var formData = new FormData();
        //             id = (id === undefined) ? '' : id;
        //             formData.append("id", id);
        //             return formData;
        //         },
        //         headers: {
        //             'X-Requested-With': 'XMLHttpRequest',
        //             'Content-Type': undefined
        //         }
        //     }).then(function (response) {
        //         deferred.resolve(response);
        //     }, function (error) {
        //         deferred.reject(error);
        //     });
        //     return deferred.promise;
        // } //END deleteUserFile();

                
    };
})();
(function() {
    'use strict';
    angular.module('ratingApp').controller('RatingController', RatingController);

    RatingController.$inject = ['$scope', '$rootScope', '$state', '$location', 'RatingService', 'toastr', 'SweetAlert', '$uibModal'];

    function RatingController($scope, $rootScope, $state, $location, RatingService, toastr, SweetAlert, $uibModal) {
        
        console.log($state.current.name);

        var vm = this;
        $rootScope.headerTitle = 'Ratings'; 
               
        vm.getRatingList = getRatingList;
        vm.getSingleRating = getSingleRating;
        vm.saveRating = saveRating;
        vm.deleteRating = deleteRating; 
        vm.formatDate = formatDate;
        vm.changePage = changePage;       
        vm.changeStatus = changeStatus;
        vm.editRating = editRating;
         
        vm.sort = sort;
        vm.reset = reset;
                
        vm.title = 'Add New';
        vm.addForm = { rating_id: '' };        
        vm.totalRatings = 0;
        vm.ratingsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if(path[3] == ""){
                $state.go('backoffice.rating');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                editRating(path[3]);
                getSingleRating(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting editor list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getRatingList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getRatingList(newPage, searchInfo);
        }//END changePage()

        /* to save editor details */
        function saveRating() {
            vm.addForm.ratingId = (vm.addForm.ratingId == undefined || vm.addForm.ratingId == '') ? 0 : vm.addForm.ratingId;    
            RatingService.saveRating(vm.addForm).then(function(response) {
                if(response.data.status == 1) {
                    toastr.success(response.data.message, 'Rating');
                    $state.go('backoffice.rating');
                } else {
                    toastr.error(response.data.error, 'Rating');
                }
            },function (error) {
                toastr.error('Internal server error', 'Rating');
            }).finally(function () {
                $rootScope.$emit("CloseLoader", {});
            }); 
        }//END saveEditor()

        /* to get Rating list */
        function getRatingList(newPage, obj) {
            vm.totalRatings = 0;
            vm.ratingList = [];
        	RatingService.getRatings(newPage, obj).then(function(response) {
                if (response.status == 200) {
                	if(response.data.data && response.data.data.length > 0) {
                        vm.totalRatings = response.data.data.length;
                        vm.ratingList = response.data.data;                       
                    }                     
                } 
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getRatingList()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getRatingList(1,'');
        }//END reset()

        /* to change active/inactive status of Rating */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            var data = { rating_id: status.rating_id, status: statusId };
            RatingService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /** to delete a Rating **/
        function deleteRating(obj) {   
            var id = obj;
            SweetAlert.swal({
               title: "Are you sure you want to delete this editor?",
               text: "",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
               cancelButtonText: "No",
               closeOnConfirm: false,
               closeOnCancel: true,
               html: true
            }, 
            function(isConfirm){ 
                if (isConfirm) {
                    RatingService.deleteRating(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            $state.reload();
                            return true;  
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END deleteRating()

        /* to show editor details in form */
        function editRating(userId) {
            vm.editFlag = true;
            vm.title = 'Edit';
            vm.user_id = userId;
            getRatingById(userId);
        }//END editRating()
        
        /* to get Rating information */
        function getRatingById(userId) {
            
        	RatingService.getRatingById(userId).then(function(response) {                
            	if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.editFlag = true;
                        vm.addForm.ratingId = response.data.data[0].rating_id;
                        vm.addForm.name = response.data.data[0].rating_name;
                        vm.addForm.status = response.data.data[0].status;                        
                    }
                }
            },function (error) {
                $state.go('backoffice.editors');
                toastr.error(error.data.message, 'Error');
            });
        }//END getRatingById();

        /* to get single role */
        function getSingleRating(rating_id) {
            RatingService.getRatingById(rating_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleRating = response.data.data[0];
                        vm.ratingForm = response.data.data[0];
                    } else {
                        toastr.error(response.data.message, 'Rating');
                        $state.go('backoffice.rating');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Role');
            });
        }//END getSingleRole();




        // vm.validatePhone = validatePhone;
        // vm.uploadFiles = uploadFiles;
        // vm.getIndex = getIndex;

        // vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        // vm.setAsDefaultEditor = setAsDefaultEditor;
        // vm.uploadDocs = uploadDocs;
        // if(!vm.editFlag) {
        //     vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        // }
        // vm.deleteUserFile = deleteUserFile;
      

        /* to get Rating information Old */
        // function getRatingByIdOld(userId) {
            
        // 	RatingService.getRatingById(userId).then(function(response) {
                
        //     	if (response.status == 200) {
        //             if (response.data.status == 1) {
        //                 vm.editFlag = true;
        //                 vm.addForm.ratingId = response.data.data[0].rating_id;
        //                 vm.addForm.name = response.data.data[0].rating_name;
        //                 return false;
        //                 vm.editFlag = true;
        //                 vm.addForm.name = response.data.user_details.firstname;
        //                 vm.addForm.company_name = response.data.user_details.company_name;
        //                 vm.addForm.address = (response.data.user_details.address == 'undefined') ? '' : response.data.user_details.address;
        //                 vm.addForm.phone = (response.data.user_details.phone == 'undefined') ? '' : response.data.user_details.phone;
        //                 vm.addForm.email = response.data.user_details.email;
        //                 vm.addForm.social_security = (response.data.user_details.social_security == 'undefined') ? '' : response.data.user_details.social_security;
        //                 vm.addForm.ein = (response.data.user_details.ein == 'undefined') ? '' : response.data.user_details.ein;
        //                 vm.addForm.profile_image = (response.data.user_details.profile_image == 'undefined') ? '' : response.data.user_details.profile_image;
        //                 vm.addForm.createdAt = moment(response.data.user_details.created_at).format('MMMM Do YYYY, h:mm:ss a');
        //                 vm.addForm.userId = response.data.user_details.user_id;
        //                 vm.addForm.roleId = response.data.user_details.role_id;
        //                 vm.addForm.isDefaultEditor = response.data.user_details.is_default_editor;
        //                 vm.addForm.password = response.data.user_details.password;
        //                 vm.profile_image = response.data.user_details.profile_image;
        //                 // if(vm.addForm.profile_image != '' && vm.addForm.profile_image != undefined) {
        //                 // 	vm.profile_image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/avatar/thumbs/' + vm.addForm.profile_image;
        //             	// } else {
        //             	// 	vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        //             	// }
        //                 if(response.data.user_files.length > 0) {
        //                     vm.userFiles = [];
        //                     angular.forEach(response.data.user_files, function(value, key) {
        //                         var image_path = '';
        //                         if(value.type == 2) {
        //                             image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/files/' + value.file_name;
        //                         }
        //                         vm.userFiles.push({
        //                             'id':value.id,
        //                             'original_name':value.original_name+'.'+value.extension,
        //                             'size':formatBytes(value.size),
        //                             'type':value.type,
        //                             'image_path':image_path,
        //                             'extension':value.extension
        //                         });

        //                     });
        //                 }

        //                 if(response.data.user_details.status == 0) {
        //                     vm.addForm.status = '2';
        //                 } else {
        //                     vm.addForm.status = '1';
        //                 }
        //             }
        //         }
        //     },function (error) {
        //         $state.go('backoffice.editors');
        //         toastr.error(error.data.message, 'Error');
        //     });
        // }//END getRatingById();



        /* to convert file size to KB,MB,GB */
        // function formatBytes(bytes) {
        //     if(bytes < 1024) return bytes + " Bytes";
        //     else if(bytes < 1048576) return(bytes / 1024).toFixed(2) + " KB";
        //     else if(bytes < 1073741824) return(bytes / 1048576).toFixed(2) + " MB";
        //     else return(bytes / 1073741824).toFixed(2) + " GB";
        // }//END formatBytes()

        /* to accept only number */
        // function validatePhone(number) {
        // 	return sharedService.validatePhoneNumber(number);
        // }//END validatePhone()

        // function uploadFiles(files, errFiles) {
	    // 	vm.addForm.profile_image = files[0];
	    // 	var reader = new FileReader();
        //     reader.readAsDataURL(files[0]);
        //     reader.onloadend = function (event) {
        //         $scope.$apply(function ($scope) {
        //         	vm.profile_image_path = event.target.result;
        //         });
        //     }
	    // }

	    // $scope.uploadDocument = function(files, errFiles) {
	    // 	vm.addForm.document_file = files[0];
	    // }
                        
        //retrive indexing
        // function getIndex(newPage, length){
        //     var index = sharedService.indexPerPage(newPage, length, vm.editorsPerPage);
        //                 vm.fromIndex = index.fromIndex;
        //                 vm.toIndex = index.toIndex;
        // }//END getIndex()
       
        /* to set a editor as default */
        // function setAsDefaultEditor(event) {
        //     var id = vm.addForm.userId;
        //     SweetAlert.swal({
        //        title: "Are you sure you want to set this editor as default?",
        //        text: "",
        //        type: "warning",
        //        showCancelButton: true,
        //        confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",
        //        cancelButtonText: "No",
        //        closeOnConfirm: false,
        //        closeOnCancel: true,
        //        html: true
        //     }, 
        //     function(isConfirm){ 
        //         if (isConfirm) {
        //             RatingService.setAsDefaultEditor(id).then(function(response) {
        //                 if (response.data.status == 1) {
        //                     SweetAlert.swal("Success!", response.data.message, "success");
        //                     vm.addForm.isDefaultEditor = 1;
        //                 }
        //             },function (error) {
        //                 toastr.error(error.data.error, 'Error');
        //             });
        //         }
        //     });
        // }//END setAsDefaultEditor()

        // $scope.uploadMultipleDocument = function(files, errFiles) {
        //     vm.documents = files;
        //     vm.addForm.documents = files;
        // }

        // function uploadDocs() {
        //     $rootScope.$emit("StartLoader", {});
        //     var data = {documents:vm.documents,user_id:vm.user_id}
        //     RatingService.uploadDocs(data).then(function(response) {
        //         if(response.data.status == 1) {
        //             vm.userFiles = [];
        //             angular.forEach(response.data.files, function(value, key) {
        //                 var image_path = '';
        //                 if(value.type == 2) {
        //                     image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/files/' + value.file_name;
        //                 }
        //                 vm.userFiles.push({
        //                     'id':value.id,
        //                     'original_name':value.original_name+'.'+value.extension,
        //                     'size':formatBytes(value.size),
        //                     'type':value.type,
        //                     'image_path':image_path,
        //                     'extension':value.extension
        //                 });

        //             });
        //             toastr.success(response.data.message, 'Success');
        //         } else {
        //             toastr.error(response.data.error, 'Error');
        //         }
        //     },function (error) {
        //         toastr.error(error.data.message, 'Error');
        //     }).finally(function () {
        //         $rootScope.$emit("CloseLoader", {});
        //     }); 
        // }

        /** to delete a user file **/
        // function deleteUserFile(obj,index) {
        //     var id = obj.id;
        //     SweetAlert.swal({
        //        title: "Are you sure you want to delete this file?",
        //        text: "",
        //        type: "warning",
        //        showCancelButton: true,
        //        confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
        //        cancelButtonText: "No",
        //        closeOnConfirm: false,
        //        closeOnCancel: true,
        //        html: true
        //     }, 
        //     function(isConfirm){ 
        //         if (isConfirm) {
        //             RatingService.deleteUserFile(id).then(function(response) {
        //                 if (response.data.status == 1) {
        //                     SweetAlert.swal("Deleted!", response.data.message, "success");
        //                     vm.userFiles.splice(index, 1);
        //                 }
        //             },function (error) {
        //                 toastr.error(error.data.error, 'Error');
        //             });
        //         }
        //     });
        // }//END deleteUserFile()        

    }

}());
(function () {

    angular.module('roleApp', []).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var rolePath = 'app/components/role/';
        $stateProvider
            .state('backoffice.role', {
                url: 'role',
                views: {
                    'content@backoffice': {
                        templateUrl: rolePath + 'views/index.html',
                        controller: 'RoleController',
                        controllerAs: 'role'
                    }
                }
            })
            .state('backoffice.role.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: rolePath + 'views/form.html',
                        controller: 'RoleController',
                        controllerAs: 'role'
                    }
                }
            })
            .state('backoffice.role.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: rolePath + 'views/form.html',
                        controller: 'RoleController',
                        controllerAs: 'role'
                    }
                }
            })
            .state('backoffice.role.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: rolePath + 'views/view.html',
                        controller: 'RoleController',
                        controllerAs: 'role'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('roleApp')
        .service('RoleService', RoleService);

    RoleService.$inject = ['$http', 'APPCONFIG', '$q'];

    function RoleService($http, APPCONFIG, $q) {

        /* save role */
        function saveRole(obj) {
            var URL = APPCONFIG.APIURL + 'role/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = obj.role_id;
                    URL = APPCONFIG.APIURL + 'role/edit';
                }

                if (typeof obj.role_name !== 'undefined' && obj.role_name !== '') {
                    requestData['role_name'] = obj.role_name;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveRole();

        /* to get all roles */
        function getRoles(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'role';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = parseInt(obj.role_id);
                }

                if (typeof obj.role_name !== 'undefined' && obj.role_name !== '') {
                    requestData['role_name'] = obj.role_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getRoles();

        /* to get single role */
        function getSingleRole(role_id) {
            var URL = APPCONFIG.APIURL + 'role/view';
            var requestData = {};

            if (typeof role_id !== undefined && role_id !== '') {
                requestData['role_id'] = role_id;
            }

            return this.runHttp(URL, requestData);
        } //END getRoleById();

        /* to delete a role from database */
        function deleteRole(role_id) {
            var URL = APPCONFIG.APIURL + 'role/delete';
            var requestData = {};

            if (typeof role_id !== undefined && role_id !== '') {
                requestData['role_id'] = role_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteRole();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getRoles: getRoles,
            getSingleRole: getSingleRole,
            saveRole: saveRole,
            deleteRole: deleteRole
        }

    };//END RoleService()
}());

(function () {
    "use strict";
    angular.module('roleApp')
        .controller('RoleController', RoleController);

    RoleController.$inject = ['$scope', '$rootScope', '$state', '$location', 'RoleService', 'toastr', 'SweetAlert', '$uibModal'];

    function RoleController($scope, $rootScope, $state, $location, RoleService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getRolesList = getRolesList;
        vm.getSingleRole = getSingleRole;
        vm.saveRole = saveRole;
        vm.deleteRole = deleteRole;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Roles';
        vm.totalRoles = 0;
        vm.rolesPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.roleForm = { role_id: '' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.role');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleRole(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getRolesList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getRolesList(newPage, searchInfo);
        }//END changePage();

        /* to save role after add and edit  */
        function saveRole() {
            RoleService.saveRole(vm.roleForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Role');
                        $state.go('backoffice.role');
                    } else {
                        toastr.error(response.data.message, 'Role');
                    }
                } else {
                    toastr.error(response.data.message, 'Role');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Role');
            });
        }//END saveRole();

        /* to get role list */
        function getRolesList(newPage, obj) {
            vm.totalRoles = 0;
            vm.roleList = [];
            RoleService.getRoles(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.roles && response.data.roles.length > 0) {
                        vm.totalRoles = response.data.total;
                        vm.roleList = response.data.roles;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getRolesList();

        /* to get single role */
        function getSingleRole(role_id) {
            RoleService.getSingleRole(role_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleRole = response.data.role;
                        vm.roleForm = response.data.role;
                    } else {
                        toastr.error(response.data.message, 'Role');
                        $state.go('backoffice.role');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Role');
            });
        }//END getSingleRole();

        /** to delete a role **/
        function deleteRole(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Role?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    RoleService.deleteRole(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.roleList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Role');
                    });
                }
            });
        }//END deleteRole();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getRolesList(1, '');
        }//END reset();
    }

}());

(function () {

    angular.module('faqApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var faqPath = 'app/components/faq/';
        $stateProvider
            .state('backoffice.faq', {
                url: 'faq',
                views: {
                    'content@backoffice': {
                        templateUrl: faqPath + 'views/index.html',
                        controller: 'FaqController',
                        controllerAs: 'faq'
                    }
                }
            })
            .state('backoffice.faq.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: faqPath + 'views/form.html',
                        controller: 'FaqController',
                        controllerAs: 'faq'
                    }
                }
            })
            .state('backoffice.faq.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: faqPath + 'views/form.html',
                        controller: 'FaqController',
                        controllerAs: 'faq'
                    }
                }
            })
            .state('backoffice.faq.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: faqPath + 'views/view.html',
                        controller: 'FaqController',
                        controllerAs: 'faq'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('faqApp')
        .service('FaqService', FaqService);

    FaqService.$inject = ['$http', 'APPCONFIG', '$q'];

    function FaqService($http, APPCONFIG, $q) {

        /* save faq */
        function saveFaq(obj) {
            var URL = APPCONFIG.APIURL + 'faq/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.faq_id !== 'undefined' && obj.faq_id !== '') {
                    requestData['faq_id'] = obj.faq_id;
                    URL = APPCONFIG.APIURL + 'faq/edit';
                }

                if (typeof obj.faq_question !== 'undefined' && obj.faq_question !== '') {
                    requestData['faq_question'] = obj.faq_question;
                }

                if (typeof obj.faq_answer !== 'undefined' && obj.faq_answer !== '') {
                    requestData['faq_answer'] = obj.faq_answer;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveFaq();

        /* to get all faqs */
        function getFaqs(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'faq';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.faq_id !== 'undefined' && obj.faq_id !== '') {
                    requestData['faq_id'] = parseInt(obj.faq_id);
                }

                if (typeof obj.faq_question !== 'undefined' && obj.faq_question !== '') {
                    requestData['faq_question'] = obj.faq_question;
                }

                if (typeof obj.faq_answer !== 'undefined' && obj.faq_answer !== '') {
                    requestData['faq_answer'] = obj.faq_answer;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getFaqs();

        /* to get single faq */
        function getSingleFaq(faq_id) {
            var URL = APPCONFIG.APIURL + 'faq/view';
            var requestData = {};

            if (typeof faq_id !== undefined && faq_id !== '') {
                requestData['faq_id'] = faq_id;
            }

            return this.runHttp(URL, requestData);
        } //END getFaqById();

        /* to delete a faq from database */
        function deleteFaq(faq_id) {
            var URL = APPCONFIG.APIURL + 'faq/delete';
            var requestData = {};

            if (typeof faq_id !== undefined && faq_id !== '') {
                requestData['faq_id'] = faq_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteFaq();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getFaqs: getFaqs,
            getSingleFaq: getSingleFaq,
            saveFaq: saveFaq,
            deleteFaq: deleteFaq
        }

    };//END FaqService()
}());

(function () {
    "use strict";
    angular.module('faqApp')
        .controller('FaqController', FaqController);

    FaqController.$inject = ['$scope', '$rootScope', '$state', '$location', 'FaqService', 'toastr', 'SweetAlert', '$uibModal'];

    function FaqController($scope, $rootScope, $state, $location, FaqService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getFaqsList = getFaqsList;
        vm.getSingleFaq = getSingleFaq;
        vm.saveFaq = saveFaq;
        vm.deleteFaq = deleteFaq;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'FAQs';
        vm.totalFaqs = 0;
        vm.faqsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.faqForm = { faq_id: '' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.faq');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleFaq(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getFaqsList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getFaqsList(newPage, searchInfo);
        }//END changePage();

        /* to save faq after add and edit  */
        function saveFaq() {
            FaqService.saveFaq(vm.faqForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'FAQ');
                        $state.go('backoffice.faq');
                    } else {
                        toastr.error(response.data.message, 'FAQ');
                    }
                } else {
                    toastr.error(response.data.message, 'FAQ');
                }
            }, function (error) {
                toastr.error('Internal server error', 'FAQ');
            });
        }//END saveFaq();

        /* to get faq list */
        function getFaqsList(newPage, obj) {
            vm.totalFaqs = 0;
            vm.faqList = [];
            FaqService.getFaqs(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.faqs && response.data.faqs.length > 0) {
                        vm.totalFaqs = response.data.total;
                        vm.faqList = response.data.faqs;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getFaqsList();

        /* to get single faq */
        function getSingleFaq(faq_id) {
            FaqService.getSingleFaq(faq_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleFaq = response.data.faq;
                        vm.faqForm = response.data.faq;
                    } else {
                        toastr.error(response.data.message, 'FAQ');
                        $state.go('backoffice.faq');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'FAQ');
            });
        }//END getSingleFaq();

        /** to delete a faq **/
        function deleteFaq(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this FAQ?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    FaqService.deleteFaq(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.faqList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'FAQ');
                    });
                }
            });
        }//END deleteFaq();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getFaqsList(1, '');
        }//END reset();
    }

}());

(function () {
    'use strict';
    angular.module('editorApp', []).config(config);

    function config($stateProvider, $urlRouterProvider) {
        var projectPath = 'app/components/editors/';
        $stateProvider
            .state('backoffice.editors', {
                url: 'editors',
                views: {
                    'content@backoffice': {
                        templateUrl: projectPath + 'views/index.html',
                        controller: 'EditorController',
                        controllerAs: 'editor'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'backoffice.dashboard',
                    label: 'All Editors'
                },
                data: {
                    pageTitle: 'Samples Testing',
                    grantAccessTo: 'authenticated'
                }
            })
            .state('backoffice.editors.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: projectPath + 'views/create.html',
                        controller: 'EditorController',
                        controllerAs: 'editor'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'backoffice.editors',
                    label: 'Add Editor'
                }
            })
            .state('backoffice.editors.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: projectPath + 'views/create.html',
                        controller: 'EditorController',
                        controllerAs: 'editor'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'backoffice.editors',
                    label: 'Edit Editor'
                }
            })
            .state('backoffice.editors.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: projectPath + 'views/view.html',
                        controller: 'EditorController',
                        controllerAs: 'editor'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'backoffice.editors',
                    label: 'Editor Details'
                }
            })
        //$locationProvider.html5Mode(true);
    }

}());
(function() {
    "use strict";

    angular
        .module('editorApp')
        .service('editorServices', editorServices);

    editorServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    function editorServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

    
        self.saveEditor = saveEditor;
        self.getEditors = getEditors;
        self.getEditorById = getEditorById;
        self.deleteEditor = deleteEditor;
        self.setAsDefaultEditor = setAsDefaultEditor;
        self.uploadDocs = uploadDocs;
        self.deleteUserFile = deleteUserFile;
        self.changeStatus = changeStatus;
        
        /* to create new editor */
        function saveEditor(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'save-editor';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    obj.name = (obj.name === undefined) ? '' : obj.name;
                    obj.company_name = (obj.company_name === undefined) ? '' : obj.company_name;
                    obj.email = (obj.email === undefined) ? '' : obj.email;
                    obj.password = (obj.password === undefined) ? '' : obj.password;
                    formData.append("name", obj.name);
                    formData.append("company_name", obj.company_name);
                    formData.append("address", obj.address);
                    formData.append("phone", obj.phone);
                    formData.append("email", obj.email);
                    formData.append("password", obj.password);
                    formData.append("social_security", obj.social_security);
                    formData.append("ein", obj.ein);
                    formData.append("profile_image", obj.profile_image);
                    angular.forEach(obj.documents, function(value, key) {
                        formData.append("documents[]", obj.documents[key]);    
                    });
                    formData.append("email_from", obj.email_from);
                    if(obj.userId != undefined && obj.userId != '') {
                        formData.append("user_id", obj.userId);
                    }    
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END saveEditor();

        /* to get all editors */
        function getEditors(pageNum, obj) {
            var deferred = $q.defer();
            var where = 'pageNum=' + pageNum;
            if (obj != undefined) {
                if (obj.name != undefined && obj.name != "")
                    where += '&name=' + obj.name;
                
                if (obj.status != undefined && obj.status != "")
                    where += '&status=' + obj.status;
            }
            
            $http({
                url: APPCONFIG.APIURL + 'get-editor-list?' + where,
                method: "GET",
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            })
            .then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;

        }//END getEditors();

        /* to get editor */
        function getEditorById(id) {
            var deferred = $q.defer();
            $http({
                url: APPCONFIG.APIURL + 'get-editor',
                method: "POST",
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    id = (id === undefined) ? '' : id;
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getEditorById();

        /* to delete a editor from database */
        function deleteEditor(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'delete-editor';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    id = (id === undefined) ? '' : id;
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END deleteEditor();

        /* to set a editor as default */
        function setAsDefaultEditor(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'set-default-editor';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    id = (id === undefined) ? '' : id;
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END setAsDefaultEditor()

        /* to upload multiple docs */
        function uploadDocs(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'upload-documents';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    angular.forEach(obj.documents, function(value, key) {
                        formData.append("documents[]", obj.documents[key]);    
                    });
                    
                    if(obj.user_id != undefined && obj.user_id != '') {
                        formData.append("user_id", obj.user_id);
                    }    
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END uploadDocs();

        /* to delete a file from database */
        function deleteUserFile(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'delete-file';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    id = (id === undefined) ? '' : id;
                    formData.append("id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END deleteUserFile();
        
        /* to change active/inactive status of user */
        function changeStatus(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'change-user-status';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();
                    formData.append("user_id", obj.userId);
                    formData.append("status", obj.status);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END changeStatus()

        return self;
    };
})();
(function() {
    'use strict';
    angular.module('editorApp').controller('EditorController', EditorController);

    EditorController.$inject = ['$scope', 'editorServices', 'sharedService', 'toastr', '$location', 'SweetAlert', '$state', '$rootScope'];

    function EditorController($scope, editorServices, sharedService, toastr, $location, SweetAlert, $state, $rootScope) {
        var vm = this;
        $rootScope.headerTitle = 'Editors';
        vm.addForm = {};
        vm.validatePhone = validatePhone;
        vm.uploadFiles = uploadFiles;
        vm.saveEditor = saveEditor;
        vm.getEditorList = getEditorList;
        vm.changePage = changePage;
        vm.getIndex = getIndex;
        
        vm.sort = sort;
        vm.totalEditors = 0;
        vm.editorsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = {
            current: 1
        };
        vm.editFlag = false;
        vm.deleteEditor = deleteEditor;
        vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        vm.setAsDefaultEditor = setAsDefaultEditor;
        vm.title = 'Add';
        vm.uploadDocs = uploadDocs;
        if(!vm.editFlag) {
            vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
        }
        vm.deleteUserFile = deleteUserFile;
        
        vm.changeStatus = changeStatus;

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if(path[3] == ""){
                $state.go('backoffice.editors');
                return false;
            } else {
            	editEditor(path[3]);
            }
        }

        /* to accept only number */
        function validatePhone(number) {
        	return sharedService.validatePhoneNumber(number);
        }//END validatePhone()

        function uploadFiles(files, errFiles) {
	    	vm.addForm.profile_image = files[0];
	    	var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            reader.onloadend = function (event) {
                $scope.$apply(function ($scope) {
                	vm.profile_image_path = event.target.result;
                });
            }
	    }

	    $scope.uploadDocument = function(files, errFiles) {
	    	vm.addForm.document_file = files[0];
	    }

        /* to save editor details */
        function saveEditor() {
            $rootScope.$emit("StartLoader", {});
            vm.addForm.email_from = $rootScope.user.email;
        	editorServices.saveEditor(vm.addForm).then(function(response) {
                if(response.data.status == 1) {
                    toastr.success(response.data.message, 'Success');
                    $state.go('backoffice.editors');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            },function (error) {
                toastr.error('Something went wrong!', 'Error');
            }).finally(function () {
                $rootScope.$emit("CloseLoader", {});
            }); 
        }//END saveEditor()

        /* for sorting editor list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getEditorList(newPage, searchInfo);
        }//END changePage()

        //retrive indexing
        function getIndex(newPage, length){
            var index = sharedService.indexPerPage(newPage, length, vm.editorsPerPage);
                        vm.fromIndex = index.fromIndex;
                        vm.toIndex = index.toIndex;
        }//END getIndex()

        /* to get editor list */
        function getEditorList(newPage, obj) {
        	editorServices.getEditors(newPage, obj).then(function(response) {
            	if (response.status == 200) {
                	if(response.data.editors && response.data.editors.length > 0) {
                        vm.totalEditors = response.headers('total_editors');
                        vm.editorList = response.data.editors;
                        getIndex(newPage, vm.editorList.length);
                    } else {
                        vm.totalEditors = 0;
                        vm.editorList = "";
                        getIndex(newPage, vm.editorList.length);
                    }
                } 
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getEditorList()

        /* to show editor details in form */
        function editEditor(userId) {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.user_id = userId;
                getEditorById(userId);
        }//END editEditor()

        /* to get editor information */
        function getEditorById(userId) {
        	editorServices.getEditorById(userId).then(function(response) {
            	if (response.status == 200) {
                    if (response.data.status == 1) {
                        console.log(response.data);
                        vm.editFlag = true;
                        vm.addForm.name = response.data.user_details.firstname;
                        vm.addForm.company_name = response.data.user_details.company_name;
                        vm.addForm.address = (response.data.user_details.address == 'undefined') ? '' : response.data.user_details.address;
                        vm.addForm.phone = (response.data.user_details.phone == 'undefined') ? '' : response.data.user_details.phone;
                        vm.addForm.email = response.data.user_details.email;
                        vm.addForm.social_security = (response.data.user_details.social_security == 'undefined') ? '' : response.data.user_details.social_security;
                        vm.addForm.ein = (response.data.user_details.ein == 'undefined') ? '' : response.data.user_details.ein;
                        vm.addForm.profile_image = (response.data.user_details.profile_image == 'undefined') ? '' : response.data.user_details.profile_image;
                        vm.addForm.createdAt = moment(response.data.user_details.created_at).format('MMMM Do YYYY, h:mm:ss a');
                        vm.addForm.userId = response.data.user_details.user_id;
                        vm.addForm.roleId = response.data.user_details.role_id;
                        vm.addForm.isDefaultEditor = response.data.user_details.is_default_editor;
                        vm.addForm.password = response.data.user_details.password;
                        vm.profile_image = response.data.user_details.profile_image;
                        // if(vm.addForm.profile_image != '' && vm.addForm.profile_image != undefined) {
                        // 	vm.profile_image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/avatar/thumbs/' + vm.addForm.profile_image;
                    	// } else {
                    	// 	vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
                    	// }
                        if(response.data.user_files.length > 0) {
                            vm.userFiles = [];
                            angular.forEach(response.data.user_files, function(value, key) {
                                var image_path = '';
                                if(value.type == 2) {
                                    image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/files/' + value.file_name;
                                }
                                vm.userFiles.push({
                                    'id':value.id,
                                    'original_name':value.original_name+'.'+value.extension,
                                    'size':formatBytes(value.size),
                                    'type':value.type,
                                    'image_path':image_path,
                                    'extension':value.extension
                                });

                            });
                        }

                        if(response.data.user_details.status == 0) {
                            vm.addForm.status = '2';
                        } else {
                            vm.addForm.status = '1';
                        }
                    }
                }
            },function (error) {
                $state.go('backoffice.editors');
                toastr.error(error.data.message, 'Error');
            });
        }//END getEditorById();

        /* to convert file size to KB,MB,GB */
        function formatBytes(bytes) {
            if(bytes < 1024) return bytes + " Bytes";
            else if(bytes < 1048576) return(bytes / 1024).toFixed(2) + " KB";
            else if(bytes < 1073741824) return(bytes / 1048576).toFixed(2) + " MB";
            else return(bytes / 1073741824).toFixed(2) + " GB";
        }//END formatBytes()

        /** to delete a editor **/
        function deleteEditor() {   
        	var id = vm.addForm.userId;
        	SweetAlert.swal({
               title: "Are you sure you want to delete this editor?",
               text: "",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
               cancelButtonText: "No",
               closeOnConfirm: false,
               closeOnCancel: true,
               html: true
            }, 
            function(isConfirm){ 
                if (isConfirm) {
                    editorServices.deleteEditor(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            $state.go('backoffice.editors');
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END deleteEditor()

        /* to set a editor as default */
        function setAsDefaultEditor(event) {
            var id = vm.addForm.userId;
            SweetAlert.swal({
               title: "Are you sure you want to set this editor as default?",
               text: "",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",confirmButtonText: "Yes",
               cancelButtonText: "No",
               closeOnConfirm: false,
               closeOnCancel: true,
               html: true
            }, 
            function(isConfirm){ 
                if (isConfirm) {
                    editorServices.setAsDefaultEditor(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Success!", response.data.message, "success");
                            vm.addForm.isDefaultEditor = 1;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END setAsDefaultEditor()

        $scope.uploadMultipleDocument = function(files, errFiles) {
            vm.documents = files;
            vm.addForm.documents = files;
        }

        function uploadDocs() {
            $rootScope.$emit("StartLoader", {});
            var data = {documents:vm.documents,user_id:vm.user_id}
            editorServices.uploadDocs(data).then(function(response) {
                if(response.data.status == 1) {
                    vm.userFiles = [];
                    angular.forEach(response.data.files, function(value, key) {
                        var image_path = '';
                        if(value.type == 2) {
                            image_path = 'https://s3.amazonaws.com/'+$rootScope.bucketName+'/uploads/users/'+vm.addForm.userId+'/files/' + value.file_name;
                        }
                        vm.userFiles.push({
                            'id':value.id,
                            'original_name':value.original_name+'.'+value.extension,
                            'size':formatBytes(value.size),
                            'type':value.type,
                            'image_path':image_path,
                            'extension':value.extension
                        });

                    });
                    toastr.success(response.data.message, 'Success');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            },function (error) {
                toastr.error(error.data.message, 'Error');
            }).finally(function () {
                $rootScope.$emit("CloseLoader", {});
            }); 
        }

        /** to delete a user file **/
        function deleteUserFile(obj,index) {
            var id = obj.id;
            SweetAlert.swal({
               title: "Are you sure you want to delete this file?",
               text: "",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
               cancelButtonText: "No",
               closeOnConfirm: false,
               closeOnCancel: true,
               html: true
            }, 
            function(isConfirm){ 
                if (isConfirm) {
                    editorServices.deleteUserFile(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.userFiles.splice(index, 1);
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END deleteUserFile()

        /* to change active/inactive status of editor */ 
        function changeStatus(status) {
            var data = { userId: vm.user_id, status: status };
            editorServices.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

    }

}());
(function () {

    angular.module('cafeUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var cafeUserPath = 'app/components/cafeUser/';
        $stateProvider
            .state('backoffice.cafeUser', {
                url: 'cafeUser',
                views: {
                    'content@backoffice': {
                        templateUrl: cafeUserPath + 'views/index.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafeUser'
                    }
                }
            })
            .state('backoffice.cafeUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: cafeUserPath + 'views/form.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafeUser'
                    }
                }
            })
            .state('backoffice.cafeUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: cafeUserPath + 'views/form.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafeUser'
                    }
                }
            })
            .state('backoffice.cafeUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: cafeUserPath + 'views/view.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafeUser'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('cafeUserApp')
        .service('CafeUserService', CafeUserService);

    CafeUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function CafeUserService($http, APPCONFIG, $q) {

        /* save cafe user */
        function saveCafeUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.user_address !== undefined && obj.user_address !== '') {
                    requestData['address'] = obj.user_address;
                }

                if (typeof obj.user_email !== undefined && obj.user_email !== '') {
                    requestData['email'] = obj.user_email;
                }

                if (typeof obj.user_name !== undefined && obj.user_name !== '') {
                    requestData['name'] = obj.user_name;
                }

                if (typeof obj.user_phone !== undefined && obj.user_phone !== '') {
                    requestData['phone'] = obj.user_phone;
                }

                if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                    requestData['username'] = obj.user_username;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveCafeUser();

        /* to get all cafe user  */
        function getCafeUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getCafeUser();

        /* to get single role */
        function getSingleCafeUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getCafeUserById();

        /* to delete a cafe from database */
        function deleteCafeUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteCafeUser();

        /* to change active/inactive status of cafe user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'user/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change active/inactive suspend of cafe user */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.is_suspend != undefined || obj.is_suspend != "") {
                    requestData["is_suspend"] = obj.is_suspend;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getCafeUser: getCafeUser,
            getSingleCafeUser: getSingleCafeUser,
            saveCafeUser: saveCafeUser,
            deleteCafeUser: deleteCafeUser,
            changeStatus: changeStatus,
            changeSuspend: changeSuspend
        }

    };//END CafeUserService()
}());

(function () {
    "use strict";
    angular.module('cafeUserApp')
        .controller('CafeUserController', CafeUserController);

        CafeUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'CafeUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function CafeUserController($scope, $rootScope, $state, $location, CafeUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getCafeUserList = getCafeUserList;
        vm.getSingleCafeUser = getSingleCafeUser;
        vm.saveCafeUser = saveCafeUser;
        vm.deleteCafeUser = deleteCafeUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Cafe User';
        vm.totalCafeUser = 0;
        vm.cafeUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.cafeUserForm = { user_id: '', usertypeFlag: '2', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.cafeUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleCafeUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting cafe user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getCafeUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getCafeUserList(newPage, searchInfo);
        }//END changePage();

        /* to save cafe user after add and edit  */
        function saveCafeUser() {
            CafeUserService.saveCafeUser(vm.cafeUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Cafe User');
                        $state.go('backoffice.cafeUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Cafe User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Cafe User');
                            }else{
                                toastr.error(response.data.message, 'Cafe User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Cafe User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Cafe User');
            });
        }//END saveCafeUser();

        /* to get cafe user list */
        function getCafeUserList(newPage, obj) {
            vm.totalCafeUser = 0;
            vm.cafeUserList = [];
            CafeUserService.getCafeUser(newPage, obj, vm.orderInfo, '2').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalCafeUser = response.data.total;
                        vm.cafeUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getCafeUserList();

        /* to get single cafe user */
        function getSingleCafeUser(user_id) { 
            var UserType = 2;           
            CafeUserService.getSingleCafeUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleCafeUser = response.data.data[0];
                        // vm.cafeUserForm = response.data.data[0];
                        vm.cafeUserForm.user_id = response.data.data[0].user_id;
                        vm.cafeUserForm.usertype = response.data.data[0].user_role_id;
                        vm.cafeUserForm.registertype = response.data.data[0].user_registertype;
                        vm.cafeUserForm.user_name = response.data.data[0].cafeusers_name;
                        vm.cafeUserForm.user_email = response.data.data[0].cafeusers_email;
                        vm.cafeUserForm.user_phone = response.data.data[0].cafeusers_phone;
                        vm.cafeUserForm.user_address = response.data.data[0].cafeusers_address;
                        vm.cafeUserForm.user_username = response.data.data[0].user_username;
                        vm.cafeUserForm.status = response.data.data[0].status; 
                        vm.cafeUserForm.userusername = true;
                    } else {
                        toastr.error(response.data.message, 'Cafe User');
                        $state.go('backoffice.cafeUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Cafe User');
            });
        }//END getSingleCafeUser();

        /** to delete a cafe user **/
        function deleteCafeUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Cafe User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    CafeUserService.deleteCafeUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.cafeUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Cafe User');
                    });
                }
            });
        }//END deleteCafeUser();

        /* to change active/inactive status of cafe user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            CafeUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this Cafe User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    CafeUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getCafeUserList(1, '');
        }//END reset();
    }

}());

(function () {

    angular.module('workerUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var workerUserPath = 'app/components/workerUser/';
        $stateProvider
            .state('backoffice.workerUser', {
                url: 'workerUser',
                views: {
                    'content@backoffice': {
                        templateUrl: workerUserPath + 'views/index.html',
                        controller: 'WorkerUserController',
                        controllerAs: 'workerUser'
                    }
                }
            })
            .state('backoffice.workerUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: workerUserPath + 'views/form.html',
                        controller: 'WorkerUserController',
                        controllerAs: 'workerUser'
                    }
                }
            })
            .state('backoffice.workerUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: workerUserPath + 'views/form.html',
                        controller: 'WorkerUserController',
                        controllerAs: 'workerUser'
                    }
                }
            })
            .state('backoffice.workerUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: workerUserPath + 'views/view.html',
                        controller: 'WorkerUserController',
                        controllerAs: 'workerUser'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('workerUserApp')
        .service('WorkerUserService', WorkerUserService);

    WorkerUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function WorkerUserService($http, APPCONFIG, $q) {

        /* save worker user */
        function saveWorkerUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.user_address !== undefined && obj.user_address !== '') {
                    requestData['address'] = obj.user_address;
                }

                if (typeof obj.user_email !== undefined && obj.user_email !== '') {
                    requestData['email'] = obj.user_email;
                }

                if (typeof obj.user_name !== undefined && obj.user_name !== '') {
                    requestData['name'] = obj.user_name;
                }

                if (typeof obj.user_phone !== undefined && obj.user_phone !== '') {
                    requestData['phone'] = obj.user_phone;
                }

                if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                    requestData['username'] = obj.user_username;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.cafeID !== undefined && obj.cafeID !== '') {
                    requestData['cafeID'] = obj.cafeID;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveWorkerUser();

        /* to get all worker user  */
        function getWorkerUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getWorkerUser();

        /* to get single woker */
        function getSingleWorkerUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleWorkerUser();

        /* to delete a worker from database */
        function deleteWorkerUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteWorkerUser();

        /* to change active/inactive status of worker user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'user/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change active/inactive suspend of worker user */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.is_suspend != undefined || obj.is_suspend != "") {
                    requestData["is_suspend"] = obj.is_suspend;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        /* to get all cafe user  */
        function getAllCafeUser(usertype) {
            var URL = APPCONFIG.APIURL + 'user/AllList';

            var requestData = { usertype };
            // if (typeof obj !== 'undefined' && obj !== '') {
            //     if (typeof obj.usertype !== 'undefined' && obj.usertype !== '') {
            //         requestData['user_role_id'] = parseInt(obj.usertype);
            //     }
            // }     
                   
            return this.runHttp(URL, requestData);
        }//END getAllCafeUser();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getWorkerUser: getWorkerUser,
            getSingleWorkerUser: getSingleWorkerUser,
            saveWorkerUser: saveWorkerUser,
            deleteWorkerUser: deleteWorkerUser,
            changeStatus: changeStatus,
            changeSuspend: changeSuspend,
            getAllCafeUser: getAllCafeUser
        }

    };//END WorkerUserService()
}());

(function () {
    "use strict";
    angular.module('workerUserApp')
        .controller('WorkerUserController', WorkerUserController);

    WorkerUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'WorkerUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function WorkerUserController($scope, $rootScope, $state, $location, WorkerUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getWorkerUserList = getWorkerUserList;
        vm.getSingleWorkerUser = getSingleWorkerUser;
        vm.saveWorkerUser = saveWorkerUser;
        vm.deleteWorkerUser = deleteWorkerUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.getAllCafeUser = getAllCafeUser;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Worker User';
        vm.totalWorkerUser = 0;
        vm.workerUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.workerUserForm = { user_id: '', usertypeFlag: '3', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.workerUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleWorkerUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting worker user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getWorkerUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getWorkerUserList(newPage, searchInfo);
        }//END changePage();

        /* to save worker user after add and edit  */
        function saveWorkerUser() {
            WorkerUserService.saveWorkerUser(vm.workerUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Worker User');
                        $state.go('backoffice.workerUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Worker User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Worker User');
                            }else{
                                toastr.error(response.data.message, 'Worker User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Worker User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker User');
            });
        }//END saveWorkerUser();

        /* to get worker user list */
        function getWorkerUserList(newPage, obj) {
            vm.totalWorkerUser = 0;
            vm.workerUserList = [];
            WorkerUserService.getWorkerUser(newPage, obj, vm.orderInfo, '3').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalWorkerUser = response.data.total;
                        vm.workerUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getWorkerUserList();

        /* to get single worker user */
        function getSingleWorkerUser(user_id) { 
            var UserType = 3;           
            WorkerUserService.getSingleWorkerUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleWorkerUser = response.data.data[0];
                        // console.log(response.data.data[0]);
                        // vm.cafeUserForm = response.data.data[0];
                        vm.workerUserForm.user_id = response.data.data[0].user_id;
                        vm.workerUserForm.usertype = response.data.data[0].user_role_id;
                        vm.workerUserForm.registertype = response.data.data[0].user_registertype;
                        vm.workerUserForm.user_name = response.data.data[0].workers_name;
                        vm.workerUserForm.user_email = response.data.data[0].workers_email;
                        vm.workerUserForm.user_phone = response.data.data[0].workers_phone;
                        vm.workerUserForm.user_address = response.data.data[0].workers_address;
                        vm.workerUserForm.user_username = response.data.data[0].user_username;
                        vm.workerUserForm.status = response.data.data[0].status;
                        // vm.workerUserForm.cafeID = response.data.data[0].cafeuser_id;
                        vm.workerUserForm.userusername = true; 
                    } else {
                        toastr.error(response.data.message, 'Worker User');
                        $state.go('backoffice.workerUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Worker User');
            });
        }//END getSingleWorkerUser();

        /** to delete a worker user **/
        function deleteWorkerUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this worker User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    WorkerUserService.deleteWorkerUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.workerUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Worker User');
                    });
                }
            });
        }//END deleteWorkerUser();

        /* to change active/inactive status of worker user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            WorkerUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this worker User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    WorkerUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getWorkerUserList(1, '');
        }//END reset();

        function getAllCafeUser(){
            vm.cafeUserList = [];
            WorkerUserService.getAllCafeUser('2').then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.cafeUserList = response.data.data;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }        
        

    }

}());

(function () {

    angular.module('assessorsUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var assessorsUserPath = 'app/components/assessorsUser/';
        $stateProvider
            .state('backoffice.assessorsUser', {
                url: 'assessorsUser',
                views: {
                    'content@backoffice': {
                        templateUrl: assessorsUserPath + 'views/index.html',
                        controller: 'AssessorsUserController',
                        controllerAs: 'assessorsUser'
                    }
                }
            })
            .state('backoffice.assessorsUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: assessorsUserPath + 'views/form.html',
                        controller: 'AssessorsUserController',
                        controllerAs: 'assessorsUser'
                    }
                }
            })
            .state('backoffice.assessorsUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: assessorsUserPath + 'views/form.html',
                        controller: 'AssessorsUserController',
                        controllerAs: 'assessorsUser'
                    }
                }
            })
            .state('backoffice.assessorsUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: assessorsUserPath + 'views/view.html',
                        controller: 'AssessorsUserController',
                        controllerAs: 'assessorsUser'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('assessorsUserApp')
        .service('AssessorsUserService', AssessorsUserService);

    AssessorsUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AssessorsUserService($http, APPCONFIG, $q) {

        /* save assessors user */
        function saveAssessorsUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.assessors_address !== undefined && obj.assessors_address !== '') {
                    requestData['address'] = obj.assessors_address;
                }

                if (typeof obj.assessors_email !== undefined && obj.assessors_email !== '') {
                    requestData['email'] = obj.assessors_email;
                }

                if (typeof obj.assessors_name !== undefined && obj.assessors_name !== '') {
                    requestData['name'] = obj.assessors_name;
                }

                if (typeof obj.assessors_phone !== undefined && obj.assessors_phone !== '') {
                    requestData['phone'] = obj.assessors_phone;
                }

                if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                    requestData['username'] = obj.user_username;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveAssessorsUser();

        /* to get all assessors user  */
        function getAssessorsUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getAssessorsUser();

        /* to get single assessors */
        function getSingleAssessorsUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleAssessorsUser();

        /* to delete a assessors from database */
        function deleteAssessorsUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteAssessorsUser();

        /* to change active/inactive status of assessors user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'user/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change active/inactive suspend of assessors user */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.is_suspend != undefined || obj.is_suspend != "") {
                    requestData["is_suspend"] = obj.is_suspend;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAssessorsUser: getAssessorsUser,
            getSingleAssessorsUser: getSingleAssessorsUser,
            saveAssessorsUser: saveAssessorsUser,
            deleteAssessorsUser: deleteAssessorsUser,
            changeStatus: changeStatus,
            changeSuspend: changeSuspend
        }

    };//END AssessorsUserService()
}());

(function () {
    "use strict";
    angular.module('assessorsUserApp')
        .controller('AssessorsUserController', AssessorsUserController);

    AssessorsUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AssessorsUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function AssessorsUserController($scope, $rootScope, $state, $location, AssessorsUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getAssessorsUserList = getAssessorsUserList;
        vm.getSingleAssessorsUser = getSingleAssessorsUser;
        vm.saveAssessorsUser = saveAssessorsUser;
        vm.deleteAssessorsUser = deleteAssessorsUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Assessors User';
        vm.totalAssessorsUser = 0;
        vm.assessorsUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.assessorsUserForm = { user_id: '', usertypeFlag: '4', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.assessorsUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleAssessorsUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting assessors user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAssessorsUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAssessorsUserList(newPage, searchInfo);
        }//END changePage();

        /* to save assessors user after add and edit  */
        function saveAssessorsUser() {
            AssessorsUserService.saveAssessorsUser(vm.assessorsUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Assessors User');
                        $state.go('backoffice.assessorsUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Assessors User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Assessors User');
                            }else{
                                toastr.error(response.data.message, 'Assessors User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Assessors User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessors User');
            });
        }//END saveAssessorsUser();

        /* to get assessors user list */
        function getAssessorsUserList(newPage, obj) {
            vm.totalAssessorsUser = 0;
            vm.assessorsUserList = [];
            AssessorsUserService.getAssessorsUser(newPage, obj, vm.orderInfo, '4').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalAssessorsUser = response.data.total;
                        vm.assessorsUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAssessorsUserList();

        /* to get single assessors user */
        function getSingleAssessorsUser(user_id) { 
            var UserType = 4;           
            AssessorsUserService.getSingleAssessorsUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleAssessorsUser = response.data.data[0];
                        // vm.cafeUserForm = response.data.data[0];
                        vm.assessorsUserForm.user_id = response.data.data[0].user_id;
                        vm.assessorsUserForm.usertype = response.data.data[0].user_role_id;
                        vm.assessorsUserForm.registertype = response.data.data[0].user_registertype;
                        vm.assessorsUserForm.assessors_name = response.data.data[0].assessors_name;
                        vm.assessorsUserForm.assessors_email = response.data.data[0].assessors_email;
                        vm.assessorsUserForm.assessors_phone = response.data.data[0].assessors_phone;
                        vm.assessorsUserForm.assessors_address = response.data.data[0].assessors_address;
                        vm.assessorsUserForm.user_username = response.data.data[0].user_username;
                        vm.assessorsUserForm.status = response.data.data[0].status; 
                        vm.assessorsUserForm.userusername = true;
                    } else {
                        toastr.error(response.data.message, 'Assessors User');
                        $state.go('backoffice.assessorsUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessors User');
            });
        }//END getSingleAssessorsUser();

        /** to delete a assessors user **/
        function deleteAssessorsUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Assessors User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AssessorsUserService.deleteAssessorsUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.assessorsUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Assessors User');
                    });
                }
            });
        }//END deleteAssessorsUser();

        /* to change active/inactive status of assessors user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            AssessorsUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this Assessors User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    AssessorsUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAssessorsUserList(1, '');
        }//END reset();
    }

}());

(function () {

    angular.module('clientUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var clientUserPath = 'app/components/clientUser/';
        $stateProvider
            .state('backoffice.clientUser', {
                url: 'clientUser',
                views: {
                    'content@backoffice': {
                        templateUrl: clientUserPath + 'views/index.html',
                        controller: 'ClientUserController',
                        controllerAs: 'clientUser'
                    }
                }
            })
            .state('backoffice.clientUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: clientUserPath + 'views/form.html',
                        controller: 'ClientUserController',
                        controllerAs: 'clientUser'
                    }
                }
            })
            .state('backoffice.clientUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: clientUserPath + 'views/form.html',
                        controller: 'ClientUserController',
                        controllerAs: 'clientUser'
                    }
                }
            })
            .state('backoffice.clientUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: clientUserPath + 'views/view.html',
                        controller: 'ClientUserController',
                        controllerAs: 'clientUser'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('clientUserApp')
        .service('ClientUserService', ClientUserService);

    ClientUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function ClientUserService($http, APPCONFIG, $q) {

        /* save client user */
        function saveClientUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.user_address !== undefined && obj.user_address !== '') {
                    requestData['address'] = obj.user_address;
                }

                if (typeof obj.user_email !== undefined && obj.user_email !== '') {
                    requestData['email'] = obj.user_email;
                }

                if (typeof obj.user_name !== undefined && obj.user_name !== '') {
                    requestData['name'] = obj.user_name;
                }

                if (typeof obj.user_phone !== undefined && obj.user_phone !== '') {
                    requestData['phone'] = obj.user_phone;
                }

                if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                    requestData['username'] = obj.user_username;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveClientUser();

        /* to get all client user  */
        function getClientUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getClientUser();

        /* to get single client */
        function getSingleClientUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleClientUser();

        /* to delete a client from database */
        function deleteClientUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteClientUser();

        /* to change active/inactive status of client user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'user/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change active/inactive suspend of client user */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.is_suspend != undefined || obj.is_suspend != "") {
                    requestData["is_suspend"] = obj.is_suspend;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getClientUser: getClientUser,
            getSingleClientUser: getSingleClientUser,
            saveClientUser: saveClientUser,
            deleteClientUser: deleteClientUser,
            changeStatus: changeStatus,
            changeSuspend: changeSuspend
        }

    };//END ClientUserService()
}());

(function () {
    "use strict";
    angular.module('clientUserApp')
        .controller('ClientUserController', ClientUserController);

    ClientUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ClientUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function ClientUserController($scope, $rootScope, $state, $location, ClientUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getClientUserList = getClientUserList;
        vm.getSingleClientUser = getSingleClientUser;
        vm.saveClientUser = saveClientUser;
        vm.deleteClientUser = deleteClientUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Client User';
        vm.totalClientUser = 0;
        vm.clientUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.clientUserForm = { user_id: '', usertypeFlag: '5', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.clientUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleClientUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting client user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getClientUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getClientUserList(newPage, searchInfo);
        }//END changePage();

        /* to save client user after add and edit  */
        function saveClientUser() {
            ClientUserService.saveClientUser(vm.clientUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Client User');
                        $state.go('backoffice.clientUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Client User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Client User');
                            }else{
                                toastr.error(response.data.message, 'Client User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Client User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Client User');
            });
        }//END saveClientUser();

        /* to get client user list */
        function getClientUserList(newPage, obj) {
            vm.totalClientUser = 0;
            vm.clientUserList = [];
            ClientUserService.getClientUser(newPage, obj, vm.orderInfo, '5').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalClientUser = response.data.total;
                        vm.clientUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getClientUserList();

        /* to get single client user */
        function getSingleClientUser(user_id) { 
            var UserType = 5;           
            ClientUserService.getSingleClientUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleClientUser = response.data.data[0];
                        // vm.cafeUserForm = response.data.data[0];
                        vm.clientUserForm.user_id = response.data.data[0].user_id;
                        vm.clientUserForm.usertype = response.data.data[0].user_role_id;
                        vm.clientUserForm.registertype = response.data.data[0].user_registertype;
                        vm.clientUserForm.user_name = response.data.data[0].clients_name;
                        vm.clientUserForm.user_email = response.data.data[0].clients_email;
                        vm.clientUserForm.user_phone = response.data.data[0].clients_phone;
                        vm.clientUserForm.user_address = response.data.data[0].clients_address;
                        vm.clientUserForm.user_username = response.data.data[0].user_username;
                        vm.clientUserForm.status = response.data.data[0].status; 
                        vm.clientUserForm.userusername = true;
                    } else {
                        toastr.error(response.data.message, 'client User');
                        $state.go('backoffice.clientUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'client User');
            });
        }//END getSingleClientUser();

        /** to delete a client user **/
        function deleteClientUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Client User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    ClientUserService.deleteClientUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.clientUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Client User');
                    });
                }
            });
        }//END deleteClientUser();

        /* to change active/inactive status of client user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            ClientUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this Client User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    ClientUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getClientUserList(1, '');
        }//END reset();
    }

}());

(function () {

    angular.module('adminUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var adminUserPath = 'app/components/adminUser/';
        $stateProvider
            .state('backoffice.adminUser', {
                url: 'adminUser',
                views: {
                    'content@backoffice': {
                        templateUrl: adminUserPath + 'views/index.html',
                        controller: 'AdminUserController',
                        controllerAs: 'adminUser'
                    }
                }
            })
            .state('backoffice.adminUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: adminUserPath + 'views/form.html',
                        controller: 'AdminUserController',
                        controllerAs: 'adminUser'
                    }
                }
            })
            .state('backoffice.adminUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: adminUserPath + 'views/form.html',
                        controller: 'AdminUserController',
                        controllerAs: 'adminUser'
                    }
                }
            })
            .state('backoffice.adminUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: adminUserPath + 'views/view.html',
                        controller: 'AdminUserController',
                        controllerAs: 'adminUser'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('adminUserApp')
        .service('AdminUserService', AdminUserService);

    AdminUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AdminUserService($http, APPCONFIG, $q) {

        /* save admin user */
        function saveAdminUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.user_address !== undefined && obj.user_address !== '') {
                    requestData['address'] = obj.user_address;
                }

                if (typeof obj.user_email !== undefined && obj.user_email !== '') {
                    requestData['email'] = obj.user_email;
                }

                if (typeof obj.user_name !== undefined && obj.user_name !== '') {
                    requestData['name'] = obj.user_name;
                }

                if (typeof obj.user_phone !== undefined && obj.user_phone !== '') {
                    requestData['phone'] = obj.user_phone;
                }

                if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                    requestData['username'] = obj.user_username;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveAdminUser();

        /* to get all admin user  */
        function getAdminUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getAdminUser();

        /* to get single admin */
        function getSingleAdminUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleAdminUser();

        /* to delete a admin from database */
        function deleteAdminUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteAdminUser();

        /* to change active/inactive status of admin user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'user/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change active/inactive suspend of admin user */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.is_suspend != undefined || obj.is_suspend != "") {
                    requestData["is_suspend"] = obj.is_suspend;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAdminUser: getAdminUser,
            getSingleAdminUser: getSingleAdminUser,
            saveAdminUser: saveAdminUser,
            deleteAdminUser: deleteAdminUser,
            changeStatus: changeStatus,
            changeSuspend: changeSuspend
        }

    };//END AdminUserService()
}());

(function () {
    "use strict";
    angular.module('adminUserApp')
        .controller('AdminUserController', AdminUserController);

    AdminUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AdminUserService', 'toastr', 'SweetAlert', '$uibModal'];

    function AdminUserController($scope, $rootScope, $state, $location, AdminUserService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getAdminUserList = getAdminUserList;
        vm.getSingleAdminUser = getSingleAdminUser;
        vm.saveAdminUser = saveAdminUser;
        vm.deleteAdminUser = deleteAdminUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.changeSuspend = changeSuspend;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Admin User';
        vm.totalAdminUser = 0;
        vm.adminUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.adminUserForm = { user_id: '', usertypeFlag: '1', registertypeFlag: '0' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.adminUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleAdminUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting admin user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAdminUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAdminUserList(newPage, searchInfo);
        }//END changePage();

        /* to save admin user after add and edit  */
        function saveAdminUser() {
            AdminUserService.saveAdminUser(vm.adminUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Admin User');
                        $state.go('backoffice.adminUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Admin User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Admin User');
                            }else{
                                toastr.error(response.data.message, 'Admin User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Admin User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Admin User');
            });
        }//END saveAdminUser();

        /* to get admin user list */
        function getAdminUserList(newPage, obj) {
            vm.totalAdminUser = 0;
            vm.adminUserList = [];
            AdminUserService.getAdminUser(newPage, obj, vm.orderInfo, '1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalAdminUser = response.data.total;
                        vm.adminUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAdminUserList();

        /* to get single admin user */
        function getSingleAdminUser(user_id) { 
            var UserType = 1;           
            AdminUserService.getSingleAdminUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleAdminUser = response.data.data[0];
                        // vm.cafeUserForm = response.data.data[0];
                        vm.adminUserForm.user_id = response.data.data[0].user_id;
                        vm.adminUserForm.usertype = response.data.data[0].user_role_id;
                        vm.adminUserForm.registertype = response.data.data[0].user_registertype;
                        vm.adminUserForm.user_name = response.data.data[0].admin_name;
                        vm.adminUserForm.user_email = response.data.data[0].admin_email;
                        vm.adminUserForm.user_phone = response.data.data[0].admin_phone;
                        vm.adminUserForm.user_address = response.data.data[0].admin_address;
                        vm.adminUserForm.user_username = response.data.data[0].user_username;
                        vm.adminUserForm.status = response.data.data[0].status; 
                        vm.adminUserForm.userusername = true;
                    } else {
                        toastr.error(response.data.message, 'Admin User');
                        $state.go('backoffice.adminUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Admin User');
            });
        }//END getSingleAdminUser();

        /** to delete a admin user **/
        function deleteAdminUser(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Admin User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AdminUserService.deleteAdminUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.adminUserList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Admin User');
                    });
                }
            });
        }//END deleteAdminUser();

        /* to change active/inactive status of admin user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            AdminUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        function changeSuspend(suspend) {
            if(suspend.is_suspend == 1){
                var is_suspend = 0;
                var texts = "unsuspend";  
            }else{
                var is_suspend = 1;  
                var texts = "suspend";  
            }
            SweetAlert.swal({
                title: "Sure you want to "+ texts +" this Admin User?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, "+ texts +" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var data = { user_id: suspend.user_id, is_suspend: is_suspend };
                    AdminUserService.changeSuspend(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAdminUserList(1, '');
        }//END reset();
    }

}());

(function () {

    angular.module('demoApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var demoPath = 'app/components/demo/';
        $stateProvider
            .state('backoffice.demo', {
                url: 'demo',
                views: {
                    'content@backoffice': {
                        templateUrl: demoPath + 'views/index.html',
                        controller: 'DemoController',
                        controllerAs: 'demo'
                    }
                }
            })
            .state('backoffice.demo.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: demoPath + 'views/form.html',
                        controller: 'DemoController',
                        controllerAs: 'demo'
                    }
                }
            })
            .state('backoffice.demo.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: demoPath + 'views/form.html',
                        controller: 'DemoController',
                        controllerAs: 'demo'
                    }
                }
            })
            .state('backoffice.demo.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: demoPath + 'views/view.html',
                        controller: 'DemoController',
                        controllerAs: 'demo'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('demoApp')
        .service('DemoService', DemoService);

    DemoService.$inject = ['$http', 'APPCONFIG', '$q'];

    function DemoService($http, APPCONFIG, $q) {

        /* save demo */
        function saveDemo(obj) {
            // var URL = APPCONFIG.APIURL + 'demo/add';

            // var requestData = {};
            // if (typeof obj !== 'undefined' && obj !== '') {
            //     if (typeof obj.demo_id !== 'undefined' && obj.demo_id !== '') {
            //         requestData['demo_id'] = obj.demo_id;
            //         URL = APPCONFIG.APIURL + 'demo/edit';
            //     }

            //     if (typeof obj.demo_data !== 'undefined' && obj.demo_data !== '') {
            //         requestData['demo_data'] = obj.demo_data;
            //     }
            // }

            // return this.runHttp(URL, requestData);
        } //END saveDemo();

        /* to get all demo */
        function getDemos(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'demo';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.demo_id !== 'undefined' && obj.demo_id !== '') {
                    requestData['demo_id'] = parseInt(obj.demo_id);
                }

                if (typeof obj.demo_data !== 'undefined' && obj.demo_data !== '') {
                    requestData['demo_data'] = obj.demo_data;
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getDemos();

        /* to get single demo */
        function getSingleDemo(demo_id) {
            var URL = APPCONFIG.APIURL + 'demo/view';
            var requestData = {};

            if (typeof demo_id !== undefined && demo_id !== '') {
                requestData['demo_id'] = demo_id;
            }

            return this.runHttp(URL, requestData);
        } //END getdemoById();

        /* to delete a demo from database */
        function deleteDemo(demo_id) {
            var URL = APPCONFIG.APIURL + 'demo/delete';
            var requestData = {};

            if (typeof demo_id !== undefined && demo_id !== '') {
                requestData['demo_id'] = demo_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteDemo();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getDemos: getDemos,
            getSingleDemo: getSingleDemo,
            saveDemo: saveDemo,
            deleteDemo: deleteDemo
        }

    };//END FaqService()
}());

(function () {
    "use strict";
    angular.module('demoApp')
        .controller('DemoController', DemoController);

    DemoController.$inject = ['$scope', '$rootScope', '$state', '$location', 'DemoService', 'toastr', 'SweetAlert', '$uibModal','Upload'];

    function DemoController($scope, $rootScope, $state, $location, DemoService, toastr, SweetAlert, $uibModal,Upload) {
        var vm = this;

        vm.getDemosList = getDemosList;
        vm.getSingleDemo = getSingleDemo;
        vm.saveDemo = saveDemo;
        vm.deleteDemo = deleteDemo;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Demos';
        vm.totalDemos = 0;
        vm.demosPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.demoForm = { demo_id: '' };
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.demo');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleDemo(path[3]);
            }
        }

      
        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getDemosList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getDemosList(newPage, searchInfo);
        }//END changePage();

        /* to save demo after add and edit  */
        function saveDemo() {
            console.log(vm.demoForm.file.$ngfName);
            var tempPath = vm.demoForm.file.$ngfName,
             targetPath = path.resolve('./assets/uploads/worker/tempPath');
                    if (path.extname(vm.demoForm.file.$ngfName).toLowerCase() === '.jpg') {
                        fs.rename(tempPath, targetPath, function(err) {
                            if (err) throw err;
                            console.log("Upload completed!");
                        });
                    } else {
                        fs.unlink(tempPath, function () {
                            if (err) throw err;
                            console.error("Only .png files are allowed!");
                        });
                    }
            
            // DemoService.saveDemo(vm.demoForm).then(function (response) {
            //     if (response.status == 200) {
            //         if (response.data.status == 1) {
            //             toastr.success(response.data.message, 'DEMO');
            //             $state.go('backoffice.demo');
            //         } else {
            //             toastr.error(response.data.message, 'DEMO');
            //         }
            //     } else {
            //         toastr.error(response.data.message, 'DEMO');
            //     }
            // }, function (error) {
            //     toastr.error('Internal server error', 'DEMO');
            // });
        }//END saveDemo();

        /* to get demo list */
        function getDemosList(newPage, obj) {
            vm.totalDemos = 0;
            vm.demoList = [];
            DemoService.getDemos(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.demos && response.data.demos.length > 0) {
                        vm.totalDemos = response.data.total;
                        vm.demoList = response.data.demos;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getDemosList();

        /* to get single demo */
        function getSingleDemo(demo_id) {
            DemoService.getSingleDemo(demo_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleDemo = response.data.demo;
                        vm.demoForm = response.data.demo;
                    } else {
                        toastr.error(response.data.message, 'FAQ');
                        $state.go('backoffice.demo');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'DEMO');
            });
        }//END getSingleDemo();

        /** to delete a demo **/
        function deleteDemo(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Demo Data?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    DemoService.deleteDemo(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.demoList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'FAQ');
                    });
                }
            });
        }//END deleteDemo();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getDemosList(1, '');
        }//END reset();
    }

}());

(function () {

    angular.module('checklistApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var checklistPath = 'app/components/checklist/';
        $stateProvider
            .state('backoffice.checklist', {
                url: 'checklist',
                views: {
                    'content@backoffice': {
                        templateUrl: checklistPath + 'views/index.html',
                        controller: 'ChecklistController',
                        controllerAs: 'checklist'
                    }
                }
            })
            .state('backoffice.checklist.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: checklistPath + 'views/form.html',
                        controller: 'ChecklistController',
                        controllerAs: 'checklist'
                    }
                }
            })
            .state('backoffice.checklist.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: checklistPath + 'views/form.html',
                        controller: 'ChecklistController',
                        controllerAs: 'checklist'
                    }
                }
            })
            .state('backoffice.checklist.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: checklistPath + 'views/view.html',
                        controller: 'ChecklistController',
                        controllerAs: 'checklist'
                    }
                }
            })
    }

}());
(function () {
    "use strict";
    angular
        .module('checklistApp')
        .service('ChecklistService', ChecklistService);

    ChecklistService.$inject = ['$http', 'APPCONFIG', '$q'];

    function ChecklistService($http, APPCONFIG, $q) {

        /* save checklist */
        function saveChecklist(obj) {
            var URL = APPCONFIG.APIURL + 'checklist/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.checklist_id !== 'undefined' && obj.checklist_id !== '') {
                    requestData['checklist_id'] = obj.checklist_id;
                    URL = APPCONFIG.APIURL + 'checklist/edit';
                }

                if (typeof obj.checklist_name !== 'undefined' && obj.checklist_name !== '') {
                    requestData['checklist_name'] = obj.checklist_name;
                }
                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }                
            }

            return this.runHttp(URL, requestData);
        } //END saveChecklist();

        /* to get all checklist */
        function getChecklist(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'checklist';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.checklist_id !== 'undefined' && obj.checklist_id !== '') {
                    requestData['checklist_id'] = parseInt(obj.checklist_id);
                }

                if (typeof obj.checklist_name !== 'undefined' && obj.checklist_name !== '') {
                    requestData['checklist_name'] = obj.checklist_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }                
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getChecklist();

        /* to get single checklist */
        function getSingleChecklist(checklist_id) {
            var URL = APPCONFIG.APIURL + 'checklist/view';
            var requestData = {};

            if (typeof checklist_id !== undefined && checklist_id !== '') {
                requestData['checklist_id'] = checklist_id;
            }

            return this.runHttp(URL, requestData);
        } //END getchecklistById();

        /* to delete a checklist from database */
        function deleteChecklist(checklist_id) {
            var URL = APPCONFIG.APIURL + 'checklist/delete';
            var requestData = {};

            if (typeof checklist_id !== undefined && checklist_id !== '') {
                requestData['checklist_id'] = checklist_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteChecklist();

        /* to change active/inactive status of worker user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'checklist/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.checklist_id != undefined && obj.checklist_id != "") {
                    requestData["checklist_id"] = obj.checklist_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()        

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getChecklist: getChecklist,
            getSingleChecklist: getSingleChecklist,
            saveChecklist: saveChecklist,
            deleteChecklist: deleteChecklist,
            changeStatus   : changeStatus
        }

    };//END ChecklistService()
}());

(function () {
    "use strict";
    angular.module('checklistApp')
        .controller('ChecklistController', ChecklistController);

    ChecklistController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ChecklistService', 'toastr', 'SweetAlert', '$uibModal'];

    function ChecklistController($scope, $rootScope, $state, $location, ChecklistService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getChecklistList = getChecklistList;
        vm.getSingleChecklist = getSingleChecklist;
        vm.saveChecklist = saveChecklist;
        vm.deleteChecklist = deleteChecklist;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Checklist';
        vm.totalChecklist = 0;
        vm.checklistPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.checklistForm = { checklist_id: '' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.checklist');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleChecklist(path[3]);
            }
        }

      
        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getChecklistList(1, '');
        }//END sort()

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()


        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getChecklistList(newPage, searchInfo);
        }//END changePage();

        /* to save saveChecklist after add and edit  */
        function saveChecklist() {
            ChecklistService.saveChecklist(vm.checklistForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'CHECKLIST');
                        $state.go('backoffice.checklist');
                    } else {
                        toastr.error(response.data.message, 'CHECKLIST');
                    }
                } else {
                    toastr.error(response.data.message, 'CHECKLIST');
                }
            }, function (error) {
                toastr.error('Internal server error', 'CHECKLIST');
            });
        }//END savechecklist();

        /* to get getChecklistList list */
        function getChecklistList(newPage, obj) {
            vm.totalChecklist = 0;
            vm.checklistList = [];
            ChecklistService.getChecklist(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.checklist && response.data.checklist.length > 0) {
                        vm.totalChecklist = response.data.total;
                        vm.checklistList = response.data.checklist;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getChecklistList();

        /* to get getSingleChecklist checklist */
        function getSingleChecklist(checklist_id) {
            ChecklistService.getSingleChecklist(checklist_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleChecklist = response.data.checklist;
                        vm.checklistForm = response.data.checklist;
                    } else {
                        toastr.error(response.data.message, 'CHECKLIST');
                        $state.go('backoffice.checklist');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'CHECKLIST');
            });
        }//END getSingleChecklist();

        /** to delete a checklist **/
        function deleteChecklist(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Checklist?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    ChecklistService.deleteChecklist(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.demoList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'CHECKLIST');
                    });
                }
            });
        }//END deleteChecklist();

        /* to change active/inactive status of checklist */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { checklist_id: status.checklist_id, status: statusId };
            ChecklistService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()        

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getChecklistList(1, '');
        }//END reset();
    }

}());

(function () {

    angular.module('skillsApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var skillsPath = 'app/components/skills/';
        $stateProvider
            .state('backoffice.skills', {
                url: 'skills',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/index.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/form.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/form.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/view.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.subview', {
                url: '/subview/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/view.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })            
            .state('backoffice.skills.subcreate', {
                url: '/subcreate',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/form.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.subedit', {
                url: '/subedit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/form.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })         
    }

}());
(function () {
    "use strict";
    angular
        .module('skillsApp')
        .service('SkillsService', SkillsService);

        SkillsService.$inject = ['$http', 'APPCONFIG', '$q'];

    function SkillsService($http, APPCONFIG, $q) {

        /* save skills */
        function saveSkills(obj) {
            var URL = APPCONFIG.APIURL + 'skills/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.skills_id !== 'undefined' && obj.skills_id !== '') {
                    requestData['skills_id'] = obj.skills_id;
                    URL = APPCONFIG.APIURL + 'skills/edit';
                }

                if (typeof obj.skills_name !== 'undefined' && obj.skills_name !== '') {
                    requestData['skills_name'] = obj.skills_name;
                }
                
                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                } 
                
                if (typeof obj.cat_id !== 'undefined' && obj.cat_id !== '') {
                    requestData['parent'] = obj.cat_id;
                } 
            }

            return this.runHttp(URL, requestData);
        } //END saveSkills();
       

        /* to get all skills */
        function getSkills(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'skills';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.skills_id !== 'undefined' && obj.skills_id !== '') {
                    requestData['skills_id'] = parseInt(obj.skills_id);
                }

                if (typeof obj.skills_name !== 'undefined' && obj.skills_name !== '') {
                    requestData['skills_name'] = obj.skills_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }  
                
                if (typeof obj.parent !== 'undefined' && obj.parent !== '') {
                    requestData['parent'] = parseInt(obj.parent);
                } 
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSkills();

        /* to get single skill */
        function getSingleSkill(skills_id) {
            var URL = APPCONFIG.APIURL + 'skills/view';
            var requestData = {};

            if (typeof skills_id !== undefined && skills_id !== '') {
                requestData['skills_id'] = skills_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleSkill();

        /* to get all parent skills  */
        function getAllMainCatSkills(parent) {
            var URL = APPCONFIG.APIURL + 'skills/AllList';

            var requestData = { parent };             
                   
            return this.runHttp(URL, requestData);
        }//END getAllMainCatSkills();        

        /* to delete a skills from database */
        function deleteSkills(skills_id) {
            var URL = APPCONFIG.APIURL + 'skills/delete';
            var requestData = {};

            if (typeof skills_id !== undefined && skills_id !== '') {
                requestData['skills_id'] = skills_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteSkills();

        /* to change active/inactive status of skills */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'skills/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.skills_id != undefined && obj.skills_id != "") {
                    requestData["skills_id"] = obj.skills_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()  

        function getCategoryParent(obj){
            var URL = APPCONFIG.APIURL + 'skills/parent';
            
            var requestData = {obj};
            
            return this.runHttp(URL, requestData);
        }
        
        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getSkills: getSkills,
            getSingleSkill: getSingleSkill,
            saveSkills: saveSkills,
            deleteSkills: deleteSkills,
            changeStatus: changeStatus,
            getAllMainCatSkills: getAllMainCatSkills,
            getCategoryParent: getCategoryParent
        }

    };//END SkillsService()
}());

(function () {
    "use strict";
    angular.module('skillsApp')
        .controller('SkillsController', SkillsController);

        SkillsController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SkillsService', 'toastr', 'SweetAlert', '$uibModal'];

    function SkillsController($scope, $rootScope, $state, $location, SkillsService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.getSkillsList = getSkillsList;
        vm.getSingleSkill = getSingleSkill;
        vm.saveSkills = saveSkills;
        vm.deleteSkills = deleteSkills;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.getAllMainCatSkills = getAllMainCatSkills;
               
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Skills';
        vm.totalSkills = 0;
        vm.skillsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.catFlag  = false;
        vm.catEditFlag = false;
        vm.title = 'Add New';
        vm.skillsForm = { skills_id: '' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        vm.getAllMainCatSkills();
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.skills');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleSkill(path[3]);
            }
        } else {
           if(path[2] == "subedit" || path[2] == "subview") {
                vm.catEditFlag = true;
                vm.catFlag = true;
                vm.title = 'Edit Sub Category';
                vm.getSingleSkill(path[3]);
           } else if(path[2] == "subcreate") {
                vm.catFlag = true;
           }
        }

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getSkillsList(1, '');
        }//END sort()

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()


        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getSkillsList(newPage, searchInfo);
        }//END changePage();

        /* to save saveSkills after add and edit  */
        function saveSkills() {
            SkillsService.saveSkills(vm.skillsForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'SKILLS');
                        $state.go('backoffice.skills');
                    } else {
                        toastr.error(response.data.message, 'SKILLS');
                    }
                } else {
                    toastr.error(response.data.message, 'SKILLS');
                }
            }, function (error) {
                toastr.error('Internal server error', 'SKILLS');
            });
        }//END saveSkills();
  

        /* to get getSkillsList list */
        function getSkillsList(newPage, obj) {
            vm.totalSkills = 0;
            vm.skillsList = [];
            SkillsService.getSkills(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.skills && response.data.skills.length > 0) {
                        vm.totalSkills = response.data.total;
                        vm.skillsList = response.data.skills;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSkillsList();

        /* to get getSingleSkill  */
        function getSingleSkill(skills_id) {
            SkillsService.getSingleSkill(skills_id).then(function (response) {
                // console.log(response);
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleSkill = response.data.skills;
                        vm.skillsForm = response.data.skills;
                        if(response.data.skills.parent != 0){
                            var parent = response.data.skills.parent;
                            SkillsService.getCategoryParent(parent).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.data && responses.data.data.length > 0) {
                                        vm.singleSkill.parent = responses.data.data[0].skills_name;
                                        vm.singleSkill.cat_id = parent;                        
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }
                        
                    } else {
                        toastr.error(response.data.message, 'SKILLS');
                        $state.go('backoffice.skills');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'SKILLS');
            });
        }//END getSingleSkill();

        /** to delete a skills **/
        function deleteSkills(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this Skill?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    SkillsService.deleteSkills(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.demoList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'SKILLS');
                    });
                }
            });
        }//END deleteSkills();

        /* to change active/inactive status of skills */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { skills_id: status.skills_id, status: statusId };
            SkillsService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()        

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getSkillsList(1, '');
        }//END reset();

        function getAllMainCatSkills(){
            vm.skillsList = [];
            SkillsService.getAllMainCatSkills('0').then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.skillsList = response.data.data;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }
    }
}());

(function () {
    'use strict';
    var adminApp = angular.module('backofficeApp', [
        'authApp',
        'dashboardApp',
        'ratingApp',
        'roleApp',
        'cafeUserApp',
        'faqApp',
        'demoApp',
        'workerUserApp',
        'assessorsUserApp',
        'clientUserApp',
        'adminUserApp',
        'checklistApp',
        'skillsApp'
    ]);

    authenticateUser.$inject = ['authServices', '$state']

    function authenticateUser(authServices, $state) {
        return authServices.checkValidUser(true);
    } //END authenticateUser()

    adminApp.config(funConfig);
    adminApp.run(funRun);

    adminApp.component("leftSidebarComponent", {
        templateUrl: 'app/layouts/left-sidebar.html',
        controller: 'BackofficeController',
        controllerAs: 'siderbar'
    });

    adminApp.component("headerComponent", {
        templateUrl: 'app/layouts/header.html',
        controller: 'BackofficeController',
        controllerAs: 'header'
    });

    adminApp.component("footerComponent", {
        templateUrl: 'app/layouts/footer.html',
        controller: 'BackofficeController',
        controllerAs: 'footer'
    });

    // App Config
    function funConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('backoffice', {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'app/layouts/layout.html'
                    },
                    'content@backoffice': {
                        templateUrl: 'app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
            .state('backoffice.dashboard', {
                url: 'dashboard',
                views: {
                    'content@backoffice': {
                        templateUrl: 'app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
            .state('backoffice.profile', {
                url: 'profile',
                views: {
                    'content@backoffice': {
                        templateUrl: 'app/components/dashboard/profile.html',
                        controller: 'BackofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
            .state('backoffice.changePassword', {
                url: 'change-password',
                views: {
                    'content@backoffice': {
                        templateUrl: 'app/components/dashboard/change-password.html',
                        controller: 'BackofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            });
    }


    // App Run
    funRun.$inject = ['$http', '$rootScope', '$state', '$location', '$log', '$transitions', 'authServices'];

    function funRun($http, $rootScope, $state, $location, $log, $transitions, authServices) {
        // $rootScope.isLogin = false;

        // $rootScope.$on('auth:login:success', function (event, data) {
        //     $state.go('backoffice.dashboard');
        // }); // Event fire after login successfully

        // $rootScope.$on('auth:access:denied', function (event, data) {
        //     $state.go('auth.login');
        // }); //Event fire after check access denied for user

        // $rootScope.$on('auth:login:required', function (event, data) {
        //     $state.go('auth.login');
        // }); //Event fire after logout

        // $transitions.onStart({ to: '**' }, function ($transition$) {
        //     $rootScope.showBreadcrumb = true;
        //     authServices.checkValidUser(true);
        //     //console.log($rootScope.user);
        //     //console.log($location.$$path);
        //     if ($transition$.to().name == 'backoffice' || $transition$.to().name == 'backoffice.dashboard') {
        //         $rootScope.showBreadcrumb = false;
        //     }
        // });
    }

}());
(function () {
    "use strict";
    angular.module('backofficeApp')
        .controller('BackofficeController', BackofficeController);

    BackofficeController.$inject = ['$scope', '$location', '$state', 'authServices', '$rootScope', 'toastr', 'SweetAlert'];

    function BackofficeController($scope, $location, $state, authServices, $rootScope, toastr, SweetAlert) {
        var vm = this;

        vm.isActive = isActive;
        vm.logout = logout;
        vm.getCurrentState = getCurrentState;
        vm.getProfileInfo = getProfileInfo;
        vm.updateProfile = updateProfile;
        vm.changePassword = changePassword;
        vm.profileForm = {}; vm.passwordForm = {};

        if($state.current.name == 'backoffice') {
            $rootScope.headerTitle = 'Dashboard';
        } else if($state.current.name == 'backoffice.changePassword') {
            $rootScope.headerTitle = 'Change Password';
        }

        console.log($state.current.name, $rootScope.headerTitle);

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu

        /* to get profile details */
        function getProfileInfo() {
            console.log($rootScope.user);
            var user = $rootScope.user;
            authServices.getUserProfile(user.user_id).then(function (response) {
                if (response.data.status == 1) {
                    vm.profileForm.user_id = response.data.user_detail.user_id;
                    vm.profileForm.name = response.data.user_detail.firstname + " " + response.data.user_detail.lastname;
                    vm.profileForm.company_name = response.data.user_detail.company_name;
                    vm.profileForm.address = response.data.user_detail.address;
                    vm.profileForm.phone = response.data.user_detail.phone;
                    vm.profileForm.email = response.data.user_detail.email;
                    vm.profileForm.profile_image = response.data.user_detail.profile_image;
                    if (vm.profileForm.profile_image != '' && vm.profileForm.profile_image != undefined) {
                        vm.profile_image_path = 'https://s3.amazonaws.com/' + $rootScope.bucketName + '/uploads/users/' + vm.profileForm.user_id + '/avatar/thumbs/' + vm.profileForm.profile_image;
                    } else {
                        vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error('Something went wrong', 'Error');
            });
        }//END getProfileInfo()

        /* to upload profile image */
        $scope.uploadProfileImage = function (files, errFiles) {
            vm.profileForm.profile_image = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            reader.onloadend = function (event) {
                $scope.$apply(function ($scope) {
                    vm.profile_image_path = event.target.result;
                });
            }
        }//END uploadProfileImage()

        /* to update profile data */
        function updateProfile() {
            authServices.updateProfile(vm.profileForm).then(function (response) {
                if (response.data.status == 1) {
                    $rootScope.user.profile_image = response.data.profile.profile_image;
                    $rootScope.user.full_name = response.data.profile.full_name;
                    toastr.success(response.data.message, 'Success');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error('Something went wrong', 'Error');
            });
        }//END updateProfile()

        /* to change passowrd for admin */
        function changePassword() {
            vm.passwordForm.user_id = $rootScope.user.id;
            authServices.changePassword(vm.passwordForm).then(function (response) {
                if (response.data.status == 1) {
                    toastr.success(response.data.message, 'Success');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error(error.data.message, 'Error');
            });
        }//END changePassword()        

        function logout() {
            SweetAlert.swal({
                title: "Sure you want to Logout?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f62d51", confirmButtonText: "Yes, Logout!",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $state.go('auth.login');
                    // authServices.logout().then(function (response) {
                    //     if (response.status == 200) {
                    //         $state.go('auth.login');
                    //     }
                    // }).catch(function (response) {
                    //     toastr.error("Unable to Logout!<br/>Try again later", "Error");
                    // });
                }
            });
        }

        function getCurrentState() {
            return $state.current.name;
        }

        $rootScope.$on("StartLoader", function () {
            $rootScope.loader = true;
        });

        $rootScope.$on("CloseLoader", function () {
            $rootScope.loader = false;
        });

    }

}());
"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngFileUpload',
    'backofficeApp'
]);

// app.constant('APPCONFIG', {
//     'APIURL': 'http://heaven.dev/api/'
// });

app.constant('APPCONFIG', {
    // 'APIURL': 'http://localhost:3000/'
    'APIURL': 'http://'+window.location.hostname+':3000/'
    // 'APIURL': 'http://192.168.0.122:3000/'
});

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');

});