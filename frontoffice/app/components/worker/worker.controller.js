(function () {
    "use strict";
    angular.module('workerApp')
        .controller('WorkerController', WorkerController);

    WorkerController.$inject = ['$scope', '$rootScope', '$state', '$location', 'WorkerService', 'toastr', 'SweetAlert', '$uibModal'];

    function WorkerController($scope, $rootScope, $state, $location, WorkerService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.saveWorker = saveWorker;
        
        vm.workerForm = { user_id: '', usertypeFlag: '3', registertypeFlag: '0' };
        vm.WorkTypeList = { '0': 'Part Time', '1': 'Long Time' };

        /* to save worker user after add and edit  */
        function saveWorker() {

            console.log(vm.workerForm);
            WorkerService.saveWorker(vm.workerForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Worker User');
                        $state.go('frontoffice.worker');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Worker User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Worker User');
                            }else{
                                toastr.error(response.data.message, 'Worker User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Worker User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker User');
            });
        }//END saveWorkerUser();

    }

}());
