(function () {

    angular.module('workerApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var workerPath = 'frontoffice/app/components/worker/';
        $stateProvider
            .state('frontoffice.worker', {
                url: 'worker',
                views: {
                    'content@frontoffice': {
                        templateUrl: workerPath + 'views/index.html',
                        // controller: 'WorkerController',
                        // controllerAs: 'worker'
                    }
                }
            })
            .state('frontoffice.worker.create', {
                url: '/create',
                views: {
                    'content@frontoffice': {
                        templateUrl: workerPath + 'views/form.html',
                        controller: 'WorkerController',
                        controllerAs: 'worker'
                    }
                }
            })
            // .state('frontoffice.worker.edit', {
            //     url: '/edit/:id',
            //     views: {
            //         'content@frontoffice': {
            //             templateUrl: workerPath + 'views/form.html',
            //             controller: 'WorkerController',
            //             controllerAs: 'worker'
            //         }
            //     }
            // })
            // .state('frontoffice.worker.view', {
            //     url: '/view/:id',
            //     views: {
            //         'content@frontoffice': {
            //             templateUrl: workerPath + 'views/view.html',
            //             controller: 'WorkerController',
            //             controllerAs: 'worker'
            //         }
            //     }
            // })
    }

}());