(function () {
    'use strict';
    angular.module('dashboardApp', []).controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', '$rootScope', '$state', '$location', 'authServices', 'dashboardService', 'toastr'];
    function DashboardController($scope, $rootScope, $state, $location, authServices, dashboardService, toastr) {
        var vm = this;
        vm.updateProfile = updateProfile;
        vm.changePassword = changePassword;
        vm.passwordValidator = passwordValidator;

        $rootScope.bodyClass = 'fix-header fix-sidebar card-no-border';
        $scope.user = $rootScope.user;
        
        function passwordValidator(password) {
            if (!password) return;
            if (password.length < 6) return "Password must be at least " + 6 + " characters long";
            if (!password.match(/[A-Z]/)) return "Password must have at least one capital letter";
            if (!password.match(/[0-9]/)) return "Password must have at least one number";

            return true;
        };

        function updateProfile(userInfo) {
            userInfo.token = $rootScope.user.token;
            dashboardService.updateProfile(userInfo).then(function (response) {
                if (response.status == 200) {
                    toastr.success(response.data.message, "Update Profile");
                } else {
                    toastr.error(response.data.message, "Update Profile");
                }
            }).catch(function (response) {
                toastr.error(response.data.message, "Update Profile");
            });
        }; //END updateProfile()

        function changePassword(changePasswordInfo) {
            changePasswordInfo.token = $rootScope.user.token;
            dashboardService.changePassword(changePasswordInfo).then(function (response) {
                if (response.status == 200 && response.data.status == 1) {
                    toastr.success(response.data.message, "Change Password");
                    $scope.changePasswordForm.reset();
                } else {
                    toastr.error(response.data.message, "Change Password");
                }
            }).catch(function (response) {
                toastr.error(response.data.message, "Change Password");
            });
        }; //END changePassword()
    };

}());