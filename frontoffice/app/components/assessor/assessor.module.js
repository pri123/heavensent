(function () {

    angular.module('assessorApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var assessorPath = 'frontoffice/app/components/assessor/';
        $stateProvider
            .state('frontoffice.assessor', {
                url: 'assessor',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/index.html',
                        // controller: 'WorkerController',
                        // controllerAs: 'worker'
                    }
                }
            })
            .state('frontoffice.assessor.book', {
                url: '/book',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/form.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })

    }

}());