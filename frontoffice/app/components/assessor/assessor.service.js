(function () {
    "use strict";
    angular
        .module('assessorApp')
        .service('AssessorService', AssessorService);

    AssessorService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AssessorService($http, APPCONFIG, $q) {

        /* to get all assessors   */
        function getAssessor(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'assessor';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
  
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getWorkerUser();

        /* to get single woker */
        function getSingleAssessor(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'assessor/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleWorkerUser();


        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAssessor: getAssessor,
            getSingleAssessor: getSingleAssessor,
        }

    };//END WorkerUserService()
}());
