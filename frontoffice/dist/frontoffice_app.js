(function () {
    'use strict';
    var authApp = angular.module('authApp', []);

    authenticateUser1.$inject = ['authServices', '$state']

    function authenticateUser1(authServices, $state) {
        return authServices.checkValidUser(false);
    } //END authenticateUser()

    authApp.config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('auth', {
                url: '/auth',
                views: {
                    '': {
                        templateUrl: 'frontoffice/app/layouts/auth/layout.html'
                    },
                    'content@auth': {
                        templateUrl: 'frontoffice/app/components/auth/login.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                },
                resolve: {
                //    auth1: authenticateUser1
                }
            })
            .state('auth.login', {
                url: '/login',
                views: {
                    'content@auth': {
                        templateUrl: 'frontoffice/app/components/auth/login.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                },
                resolve: {
                    // auth1: authenticateUser1
                }
            })
            .state('auth.forgotpassword', {
                url: '/forgot-password',
                views: {
                    'content@auth': {
                        templateUrl: 'frontoffice/app/components/auth/forgotpassword.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                },
                resolve: {
                //    auth1: authenticateUser1
                }
            });
    });
})();
(function () {
    "use strict";

    angular
        .module('authApp')
        .service('authServices', authServices);

    authServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    var someValue = '';

    function authServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

        self.checkLogin = checkLogin;
        self.checkForgotPassword = checkForgotPassword;
        self.checkValidUser = checkValidUser;
        self.setAuthToken = setAuthToken;
        self.getAuthToken = getAuthToken;
        self.saveUserInfo = saveUserInfo;
        self.checkValidUrl = checkValidUrl;
        self.getUserProfile = getUserProfile;
        self.updateProfile = updateProfile;
        self.changePassword = changePassword;
        self.logout = logout;

        //to check if user is login and set user details in rootscope
        function checkLogin(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login';
            var requestData = {};

            if (typeof obj.email !== undefined && obj.email !== '') {
                requestData['username'] = obj.email;
            }
            if (typeof obj.password !== undefined && obj.password !== '') {
                requestData['password'] = obj.password;
            }
            if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                requestData['usertype'] = obj.usertypeFlag;
            }
            if (typeof obj.device_token !== undefined && obj.device_token !== '') {
                requestData['device_token'] = obj.device_token;
            }
            if (typeof obj.device_type !== undefined && obj.device_type !== '') {
                requestData['device_type'] = obj.device_type;
            }
            // console.log(requestData);
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                $rootScope.isLogin = true;
                deferred.resolve(response);
            }, function (response) {
                $rootScope.isLogin = false;
                $rootScope.$broadcast('auth:login:required');
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkLogin();


        function checkValidUser(isAuth) {
            var URL = APPCONFIG.APIURL + 'validate-user';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("type", 'admin');
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined,
                    'Access-Token': getAuthToken()
                }
            }).then(function (response) {
                $rootScope.isLogin = true;
                saveUserInfo(response.data);
                if (isAuth === false) {
                    $rootScope.$broadcast('auth:login:success');
                } else {
                    checkValidUrl(response.data.user_detail.role_id, $location.$$path);
                }
                deferred.resolve();
            }).catch(function (response) {
                $rootScope.isLogin = false;
                if (isAuth === false) {
                    deferred.resolve();
                } else {
                    $rootScope.$broadcast('auth:login:required');
                    deferred.resolve();
                }
            });
            return deferred.promise;
        } //END checkValidUser();

        function checkForgotPassword(email) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login/forgetpassword';
            var requestData = {};

            if (typeof email !== undefined && email !== '') {
                requestData['user_username'] = email;
            }
            // console.log(requestData['user_username']);
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                // processData: false,
                // transformRequest: function (data) {
                //     var formData = new FormData();
                //     formData.append("email", email);
                //     return formData;
                // },
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkForgotPassword();

        function setAuthToken(userInfo) {
            // localStorage.setItem('token', userInfo.headers('Access-token'));
            localStorage.setItem('token', userInfo);
        } //END setAuthToken();

        function getAuthToken() {
            if (localStorage.getItem('token') != undefined && localStorage.getItem('token') != null)
                return localStorage.getItem('token');
            else
                return null;
        } //END getAuthToken();

        function saveUserInfo(data) {
            var user = {};
            if (data.status == 1) {
                user = data.user_detail;
                $rootScope.user = user;
                $rootScope.baseUrl = APPCONFIG.APIURL;
                $rootScope.bucketName = APPCONFIG.BUCKETNAME;
                //$cookies.putObject("token_key", localStorage.getItem('token'));
            }
            return user;
        } //END saveUserInfo();

        function checkValidUrl(role, location) {
            var urlData = location.split('/');
            var actualUrl = urlData[1];
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'valid-url';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("role_id", role);
                    formData.append("url", actualUrl);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        deferred.resolve(response);
                    } else {
                        $state.go('backoffice.dashboard');
                    }
                }
            });
            return deferred.promise;
        }

        /* to get user profile data */
        function getUserProfile(id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'view-profile';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("user_id", id);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END getUserProfile()

        /* to update profile data */
        function updateProfile(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'update-profile';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    var name = obj.name.split(" ");
                    obj.lastname = (name[1] === undefined) ? '' : name[1];
                    formData.append("firstname", name[0]);
                    formData.append("lastname", obj.lastname);
                    formData.append("company_name", obj.company_name);
                    formData.append("address", obj.address);
                    formData.append("phone", obj.phone);
                    formData.append("email", obj.email);
                    formData.append("user_id", obj.user_id);
                    formData.append("profile_image", obj.profile_image);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END updateProfile();

        /* to update profile data */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'change-password';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("old_password", obj.password);
                    formData.append("new_password", obj.new_password);
                    formData.append("c_password", obj.confirm_passowrd);
                    formData.append("user_id", obj.user_id);
                    formData.append("token", getAuthToken());
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END changePassword();

        function logout() {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'logout';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) { },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                localStorage.removeItem('token');
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END logout()  

        return self;
        
    };
})();
(function () {
    'use strict';
    angular.module('authApp').controller('AuthController', AuthController);

    AuthController.$inject = ['$rootScope', '$location', 'authServices', '$state', 'toastr'];

    function AuthController($rootScope, $location, authServices, $state, toastr) {
        var vm = this;
        vm.login = login;
        vm.forgotPassword = forgotPassword;
        
        vm.user = { usertypeFlag: '2', device_token: ' ', device_type: '1' };
        // vm.user = { email: 'admin@mailinator.com', password: '123456' };
        $rootScope.bodyClass = '';

        function login(loginInfo) {
             $state.go('frontoffice.dashboard');
            authServices.checkLogin(loginInfo).then(function (response) {
                var token = response.data.data[0].device_token;
                if (response.status == 200) {
                    authServices.setAuthToken(token); //Set auth token
                     $state.go('frontoffice.dashboard');
                } else {
                    toastr.error(response.data.message, "Error");
                }
            }).catch(function (response) {
                toastr.error(response.data.error, "Error");
            });
        }; //END login()   

        function forgotPassword(email) {
            authServices.checkForgotPassword(email).then(function (response) {
                if (response.status == 200) {
                    toastr.success(response.data.message, "Forgot Password");
                } else {
                    toastr.error(response.data.message, "Forgot Password");
                }
            }).catch(function (response) {
                toastr.error(response.data.message, "Forgot Password");
            });
        }; //END forgotPassword() 
    };
}());
(function () {

    angular.module('workerApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var workerPath = 'frontoffice/app/components/worker/';
        $stateProvider
            .state('frontoffice.worker', {
                url: 'worker',
                views: {
                    'content@frontoffice': {
                        templateUrl: workerPath + 'views/index.html',
                        // controller: 'WorkerController',
                        // controllerAs: 'worker'
                    }
                }
            })
            .state('frontoffice.worker.create', {
                url: '/create',
                views: {
                    'content@frontoffice': {
                        templateUrl: workerPath + 'views/form.html',
                        controller: 'WorkerController',
                        controllerAs: 'worker'
                    }
                }
            })
            // .state('frontoffice.worker.edit', {
            //     url: '/edit/:id',
            //     views: {
            //         'content@frontoffice': {
            //             templateUrl: workerPath + 'views/form.html',
            //             controller: 'WorkerController',
            //             controllerAs: 'worker'
            //         }
            //     }
            // })
            // .state('frontoffice.worker.view', {
            //     url: '/view/:id',
            //     views: {
            //         'content@frontoffice': {
            //             templateUrl: workerPath + 'views/view.html',
            //             controller: 'WorkerController',
            //             controllerAs: 'worker'
            //         }
            //     }
            // })
    }

}());
(function () {
    "use strict";
    angular
        .module('workerApp')
        .service('WorkerUserService', WorkerUserService);

    WorkerUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function WorkerUserService($http, APPCONFIG, $q) {

        /* save worker user */
        function saveWorker(obj) {
            var URL = APPCONFIG.APIURL + 'worker/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'worker/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.user_address !== undefined && obj.user_address !== '') {
                    requestData['address'] = obj.user_address;
                }

                if (typeof obj.user_name !== undefined && obj.user_name !== '') {
                    requestData['name'] = obj.user_name;
                }

                if (typeof obj.user_phone !== undefined && obj.user_phone !== '') {
                    requestData['phone'] = obj.user_phone;
                }

                if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                    requestData['username'] = obj.user_username;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

            }

            return this.runHttp(URL, requestData);
        } //END saveWorkerUser();

        /* to get all worker   */
        function getWorkerUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'worker';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
  
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getWorkerUser();

        /* to get single woker */
        function getSingleWorkerUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'worker/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleWorkerUser();


        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getWorkerUser: getWorkerUser,
            getSingleWorkerUser: getSingleWorkerUser,
            saveWorkerUser: saveWorkerUser
        }

    };//END WorkerUserService()
}());

(function () {
    "use strict";
    angular.module('workerApp')
        .controller('WorkerController', WorkerController);

    WorkerController.$inject = ['$scope', '$rootScope', '$state', '$location', 'WorkerService', 'toastr', 'SweetAlert', '$uibModal'];

    function WorkerController($scope, $rootScope, $state, $location, WorkerService, toastr, SweetAlert, $uibModal) {
        var vm = this;

        vm.saveWorker = saveWorker;
        
        vm.workerForm = { user_id: '', usertypeFlag: '3', registertypeFlag: '0' };
        vm.WorkTypeList = { '0': 'Part Time', '1': 'Long Time' };

        /* to save worker user after add and edit  */
        function saveWorker() {

            console.log(vm.workerForm);
            WorkerService.saveWorker(vm.workerForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Worker User');
                        $state.go('frontoffice.worker');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Worker User');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Worker User');
                            }else{
                                toastr.error(response.data.message, 'Worker User');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Worker User');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker User');
            });
        }//END saveWorkerUser();

    }

}());

(function () {

    angular.module('assessorApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var assessorPath = 'frontoffice/app/components/assessor/';
        $stateProvider
            .state('frontoffice.assessor', {
                url: 'assessor',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/index.html',
                        // controller: 'WorkerController',
                        // controllerAs: 'worker'
                    }
                }
            })
            .state('frontoffice.assessor.book', {
                url: '/book',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/form.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })

    }

}());
(function () {
    "use strict";
    angular
        .module('assessorApp')
        .service('AssessorService', AssessorService);

    AssessorService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AssessorService($http, APPCONFIG, $q) {

        /* to get all assessors   */
        function getAssessor(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'assessor';

            var requestData = { pageNum , usertype };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.user_username !== 'undefined' && obj.user_username !== '') {
                    requestData['user_username'] = obj.user_username;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
  
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getWorkerUser();

        /* to get single woker */
        function getSingleAssessor(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'assessor/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleWorkerUser();


        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAssessor: getAssessor,
            getSingleAssessor: getSingleAssessor,
        }

    };//END WorkerUserService()
}());

(function () {
    "use strict";
    angular.module('assessorApp')
        .controller('AssessorController', AssessorController);

    AssessorController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AssessorService', 'toastr', 'SweetAlert', '$uibModal'];

    function AssessorController($scope, $rootScope, $state, $location, AssessorService, toastr, SweetAlert, $uibModal) {
        var vm = this;

    }

}());

(function () {
    'use strict';
    var frontApp = angular.module('frontofficeApp', [
         'authApp',
        // 'dashboardApp' 
          'workerApp',
          'assessorApp'       
    ]);

    authenticateUser.$inject = ['authServices', '$state']

    function authenticateUser(authServices, $state) {
        // return authServices.checkValidUser(true);
    } //END authenticateUser()

    frontApp.config(funConfig);
    frontApp.run(funRun);

    frontApp.component("headerComponent", {
        templateUrl: 'frontoffice/app/layouts/header.html',
        controller: 'FrontofficeController',
        controllerAs: 'header'
    });

    frontApp.component("headerloginComponent", {
        templateUrl: 'frontoffice/app/layouts/auth/header.html',
        controller: 'FrontofficeController',
        controllerAs: 'headerlogin'
    });

    frontApp.component("menuComponent", {
        templateUrl: 'frontoffice/app/layouts/menu.html',
        controller: 'FrontofficeController',
        controllerAs: 'menu'
    });

    // frontApp.component("rightSidebarComponent", {
    //     templateUrl: 'frontoffice/app/layouts/right-sidebar.html',
    //     controller: 'FrontofficeController',
    //     controllerAs: 'right-sidebar'
    // });

    frontApp.component("footerComponent", {
        templateUrl: 'frontoffice/app/layouts/footer.html',
        controller: 'FrontofficeController',
        controllerAs: 'footer'
    });

    // App Config
    function funConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('frontoffice', {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'frontoffice/app/layouts/layout.html'
                    },
                    'content@frontoffice': {
                        templateUrl: 'frontoffice/app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
            .state('frontoffice.dashboard', {
                url: 'dashboard',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'frontoffice/app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
            .state('frontoffice.profile', {
                url: 'profile',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'frontoffice/app/components/dashboard/profile.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
            .state('frontoffice.changePassword', {
                url: 'change-password',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'frontoffice/app/components/dashboard/change_password.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
            .state('frontoffice.terms_condition', {
                url: 'terms_condition',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'frontoffice/app/components/links/terms_condition.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
             .state('frontoffice.privacy_policy', {
                url: 'privacy_policy',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'frontoffice/app/components/links/privacy_policy.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            })
             .state('frontoffice.faq', {
                url: 'faq',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'frontoffice/app/components/links/faq.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    // auth: authenticateUser
                }
            }); 
            
    }


    // App Run
    funRun.$inject = ['$http', '$rootScope', '$state', '$location', '$log', '$transitions'];

    function funRun($http, $rootScope, $state, $location, $log, $transitions) {
        // $rootScope.isLogin = false;

        // $rootScope.$on('auth:login:success', function (event, data) {
        //     $state.go('frontoffice.dashboard');
        // }); // Event fire after login successfully

        // $rootScope.$on('auth:access:denied', function (event, data) {
        //     $state.go('auth.login');
        // }); //Event fire after check access denied for user

        // $rootScope.$on('auth:login:required', function (event, data) {
        //     $state.go('auth.login');
        // }); //Event fire after logout

        // $transitions.onStart({ to: '**' }, function ($transition$) {
        //     $rootScope.showBreadcrumb = true;
        //     authServices.checkValidUser(true);
        //     //console.log($rootScope.user);
        //     //console.log($location.$$path);
        //     if ($transition$.to().name == 'frontoffice' || $transition$.to().name == 'frontoffice.dashboard') {
        //         $rootScope.showBreadcrumb = false;
        //     }
        // });
    }

}());
(function () {
    "use strict";
    angular.module('frontofficeApp')
        .controller('FrontofficeController', FrontofficeController);

    FrontofficeController.$inject = ['$scope', '$location', '$state', 'authServices', '$rootScope', 'toastr', 'SweetAlert'];

    function FrontofficeController($scope, $location, $state, authServices, $rootScope, toastr, SweetAlert) {
        var vm = this;

        vm.isActive = isActive;
        vm.logout = logout;
        vm.getCurrentState = getCurrentState;
        vm.getProfileInfo = getProfileInfo;
        vm.updateProfile = updateProfile;
        vm.changePassword = changePassword;
        vm.profileForm = {}; vm.passwordForm = {};

        if($state.current.name == 'frontoffice') {
           // $rootScope.headerTitle = 'Dashboard';
        } else if($state.current.name == 'frontoffice.changePassword') {
            $rootScope.headerTitle = 'Change Password';
        }

        console.log($state.current.name, $rootScope.headerTitle);

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu

        /* to get profile details */
        function getProfileInfo() {
            console.log($rootScope.user);
            var user = $rootScope.user;
            authServices.getUserProfile(user.user_id).then(function (response) {
                if (response.data.status == 1) {
                    vm.profileForm.user_id = response.data.user_detail.user_id;
                    vm.profileForm.name = response.data.user_detail.firstname + " " + response.data.user_detail.lastname;
                    vm.profileForm.company_name = response.data.user_detail.company_name;
                    vm.profileForm.address = response.data.user_detail.address;
                    vm.profileForm.phone = response.data.user_detail.phone;
                    vm.profileForm.email = response.data.user_detail.email;
                    vm.profileForm.profile_image = response.data.user_detail.profile_image;
                    if (vm.profileForm.profile_image != '' && vm.profileForm.profile_image != undefined) {
                        vm.profile_image_path = 'https://s3.amazonaws.com/' + $rootScope.bucketName + '/uploads/users/' + vm.profileForm.user_id + '/avatar/thumbs/' + vm.profileForm.profile_image;
                    } else {
                        vm.profile_image_path = '../assets/backoffice/images/heading_icon3.png';
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error('Something went wrong', 'Error');
            });
        }//END getProfileInfo()

        /* to upload profile image */
        $scope.uploadProfileImage = function (files, errFiles) {
            vm.profileForm.profile_image = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            reader.onloadend = function (event) {
                $scope.$apply(function ($scope) {
                    vm.profile_image_path = event.target.result;
                });
            }
        }//END uploadProfileImage()

        /* to update profile data */
        function updateProfile() {
            authServices.updateProfile(vm.profileForm).then(function (response) {
                if (response.data.status == 1) {
                    $rootScope.user.profile_image = response.data.profile.profile_image;
                    $rootScope.user.full_name = response.data.profile.full_name;
                    toastr.success(response.data.message, 'Success');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error('Something went wrong', 'Error');
            });
        }//END updateProfile()

        /* to change passowrd for admin */
        function changePassword() {
            vm.passwordForm.user_id = $rootScope.user.id;
            authServices.changePassword(vm.passwordForm).then(function (response) {
                if (response.data.status == 1) {
                    toastr.success(response.data.message, 'Success');
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error(error.data.message, 'Error');
            });
        }//END changePassword()        

        function logout() {
            SweetAlert.swal({
                title: "Sure you want to Logout?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f62d51", confirmButtonText: "Yes, Logout!",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $state.go('auth.login');
                    // authServices.logout().then(function (response) {
                    //     if (response.status == 200) {
                    //         $state.go('auth.login');
                    //     }
                    // }).catch(function (response) {
                    //     toastr.error("Unable to Logout!<br/>Try again later", "Error");
                    // });
                }
            });
        }

        function getCurrentState() {
            return $state.current.name;
        }

        $rootScope.$on("StartLoader", function () {
            $rootScope.loader = true;
        });

        $rootScope.$on("CloseLoader", function () {
            $rootScope.loader = false;
        });

    }

}());
"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngFileUpload',    
    'frontofficeApp'
]);

app.constant('APPCONFIG', {
    'APIURL': 'http://'+window.location.hostname+':3000/'
});

// app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
//     $urlRouterProvider.otherwise('/');    
// });

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    // paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');

});