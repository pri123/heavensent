var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var User = {

    countUser: function (searchParams = {}, usertype) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(user_id) as total FROM tbl_user WHERE user_role_id = "+ usertype +" AND is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;

        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No User found!"));
            }
        });
        return deferred.promise;
    },


    //Get All User
    // getUser: function(Usertype) {
    //     var deferred = Q.defer();

    //     var table = '';
    //     if(Usertype == 1){
    //         table = 'tbl_admin';
    //     }else{
    //         if(Usertype == 2){
    //             table = 'tbl_cafeusers';
    //         }else{
    //             if(Usertype == 3){
    //                 table = 'tbl_workers';
    //             }else{
    //                 if(Usertype == 4){
    //                     table = '   tbl_assessors';
    //                 }else{
    //                     table = 'tbl_clients';
    //                 }
    //             }
    //         }
    //     }

    //     var query = "SELECT * FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id  WHERE  ";
    //     var where = "";

    //     if (typeof Usertype != 'undefined' && Usertype != '') {
    //         where += " user_role_id = '" + striptags(Usertype) + "'";
    //     }

    //     where += " AND status = 1";
    //     where += " AND is_suspend = 0";
    //     where += " AND is_deleted = 0";

    //     query += where;
    //     query = mysql.format(query, where);

    //     db.connection.query(query, function(err, rows) {
    //         // console.log(query);
    //         if (err) {
    //             deferred.reject(new Error(err));
    //         }
    //         deferred.resolve(rows);
    //     });
    //     return deferred.promise;
    // },


    getUser: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'user_id', orderBy: 'DESC' }, usertype) {
        var deferred = Q.defer();
        var query = "SELECT user_id, user_username, status, created_at, is_suspend FROM tbl_user WHERE user_role_id = "+ usertype +" AND is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Delete User
    deleteUser: function(Delete, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';

        db.connection.query(query, [Delete, { user_id: UserId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Status User
    statusUser: function(Status, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';

        db.connection.query(query, [Status, { user_id: UserId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Suspend User
    suspendUser: function(Suspend, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';

        db.connection.query(query, [Suspend, { user_id: UserId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    // CHECK USERNAME EXISTS OR NOT
    checkUserExists: function(username) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += " user_username = '" + striptags(username) + "'";
        }

        // where += " AND status = 1";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // CHECK USER EMAIL EXISTS OR NOT
    checkUserEmailExists: function(email) {
        var deferred = Q.defer();
        var query = "SELECT * FROM `tbl_cafeusers`, `tbl_clients`, `tbl_workers`, `tbl_assessors` WHERE ";
        var where = "";

        if (typeof email != 'undefined' && email != '') {
            where += " ( `tbl_cafeusers`.`cafeusers_email` = '" + email + "' )";
            where += " OR ( `tbl_clients`.`clients_email` = '" + email + "' )";
            where += " OR ( `tbl_workers`.`workers_email` = '" + email + "' )";
            where += " OR ( `tbl_assessors`.`assessors_email` = '" + email + "' )";
        }

        // where += " AND status = 1";
        //where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            // console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ADD CAFE USER INTO MAIN LOGIN TABLE
    addUser: function(data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_user SET ?";

        db.connection.query(query, data, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result.insertId);
        });
        return deferred.promise;
    },

    // ADD CAFE USER IN CSFEUSER TABLE    
    addInfo: function(data, UserType) {
        var deferred = Q.defer();

        var table = '';
        if(UserType == 1){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }

        var query = "INSERT INTO "+ table +" SET ?";
        db.connection.query(query, data, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // UPDATE USER DATA
    updateUser: function(user_id, UserType, Data, userData) {
        var deferred = Q.defer();
        
        var table = '';
        if(UserType == 1){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }

        var query = "UPDATE "+ table +" SET ? WHERE ?";

        db.connection.query(query, [Data, { user_id: user_id }], function(err, rows) {

            if (err) {
                deferred.reject(new Error(err));
            }
            var querys = "UPDATE tbl_user SET ? WHERE ?";
            db.connection.query(querys, [userData, { user_id: user_id }], function(errs, rows1) {
                if (errs) {
                    deferred.reject(new Error(errs));
                }
                deferred.resolve(rows1);
            });
        });
        return deferred.promise;
    },

    //Get User Details
    userDetails: function(UserId, Usertype) {
        var table = '';
        if(Usertype == 1){
            table = 'tbl_admin';
        }else{
            if(Usertype == 2){
                table = 'tbl_cafeusers';
            }else{
                if(Usertype == 3){
                    table = 'tbl_workers';
                }else{
                    if(Usertype == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user JOIN "+ table +" ON tbl_user.user_id = "+ table +".user_id WHERE";
        var where = "";

        if (typeof UserId != 'undefined' && UserId != '') {
            where += " tbl_user.user_id = '" + (UserId) + "';";
        }

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            //console.log(query);
            if (err) {
                //console.log(err);
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.user_id !== 'undefined' && searchParams.user_id !== '') {
            where += " AND user_id = " + searchParams.user_id;
        }

        if (typeof searchParams.user_username !== 'undefined' && searchParams.user_username !== '') {
            where += " AND user_username LIKE '%" + striptags(searchParams.user_username) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        if (typeof searchParams.is_suspend !== 'undefined' && searchParams.is_suspend !== '') {
            where += " AND is_suspend = " + searchParams.is_suspend;
        }

        return where;
    },

    singleUser: function (user_id, UserType) {
        var deferred = Q.defer();

        var table = '';
        if(UserType == 1){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }
        // console.log(UserType +"------"+user_id);
        var query = "SELECT * FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id  WHERE  ";
        var where = "";

        if (typeof user_id != 'undefined' && user_id != '') {
            where += " tbl_user.user_id = '" + striptags(user_id) + "'";
        }

        where += " AND is_deleted = 0";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllUser: function (UserType) {
        var deferred = Q.defer();

        var table = '';
        if(UserType == 1){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }
        var query = "SELECT * FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id  WHERE  ";
        var where = "";

        if (typeof UserType != 'undefined' && UserType != '') {
            where += " tbl_user.user_role_id = '" + striptags(UserType) + "'";
        }

        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0";

        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }


};

module.exports = User;