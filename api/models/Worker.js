var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Worker = {

    // CHECK USERNAME EXISTS OR NOT
    checkUsernameExists: function(username) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += " user_username = '" + striptags(username) + "'";
        }

        // where += " AND status = 1";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ADD CAFE USER INTO MAIN LOGIN TABLE
    addWorkers: function(data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_user SET ?";

        db.connection.query(query, data, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result.insertId);
        });
        return deferred.promise;
    },

    // ADD CAFE USER IN CSFEUSER TABLE    
    addInfo: function(data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_workers SET ?";
        db.connection.query(query, data, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // UPDATE WORKERS DATA
    updateWorkers: function(workers_id, workersData) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_workers SET ? WHERE ?";

        db.connection.query(query, [workersData, { user_id: striptags(workers_id) }], function(err, rows) {
            if (err) {
                console.log(err);
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }


};

module.exports = Worker;