var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Assessor = {

    // CHECK USERNAME EXISTS OR NOT
    checkUsernameExists: function(username) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += " user_username = '" + striptags(username) + "'";
        }

        // where += " AND status = 1";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ADD ASSESSORS INTO MAIN LOGIN TABLE
    addAssessors: function(data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_user SET ?";

        db.connection.query(query, data, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result.insertId);
        });
        return deferred.promise;
    },

    // ADD ASSESSORS IN ASSESSORS TABLE    
    addInfo: function(data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_assessors SET ?";
        db.connection.query(query, data, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // UPDATE WORKERS DATA
    updateAssessors: function(assessors_id, assessorsData) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_assessors SET ? WHERE ?";

        db.connection.query(query, [assessorsData, { user_id: striptags(assessors_id) }], function(err, rows) {
            if (err) {
                console.log(err);
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }


};

module.exports = Assessor;