var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Rating = {

    //Get All Rating List
    getRating: function(limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'rating_id', orderBy: 'DESC' }) {

        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_rating WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;

        // var deferred = Q.defer();
        // var query = "SELECT * FROM tbl_rating WHERE ";
        // var where = "";

        // // where += " status = 1";
        // where += " is_deleted = 0";

        // query += where;
        // query = mysql.format(query, where);

        // db.connection.query(query, function(err, rows) {
        //     //console.log(query);
        //     if (err) {
        //         deferred.reject(new Error(err));
        //     }
        //     deferred.resolve(rows);
        // });
        // return deferred.promise;
    },

    //Delete Rating Name
    deleteRating: function(Delete, RatingId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_rating SET ? WHERE ?';
        db.connection.query(query, [Delete, { rating_id: RatingId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Status Rating Name
    statusRating: function(Status, RatingId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_rating SET ? WHERE ?';

        db.connection.query(query, [Status, { rating_id: RatingId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    // Check Rating Name
    checkNameExists: function(name) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_rating WHERE ";
        var where = "";

        if (typeof name != 'undefined' && name != '') {
            where += " rating_name = '" + striptags(name) + "'";
        }

        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Add Rating Name
    addRating: function(data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_rating SET ?";
        
        db.connection.query(query, data, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },    

    // Edit Rating Name
    updateRating: function(Rating, RatingId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_rating SET ? WHERE ?';

        db.connection.query(query, [Rating, { rating_id: RatingId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Get Rating By Id
    getRatingById: function(RatingId) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_rating WHERE ";
        var where = "";

        where += " rating_id = " + RatingId;
        where += " AND is_deleted = 0";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, result, fields) {
            //console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Count Rating
    countRating: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(rating_id) as total FROM tbl_rating WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No role found!"));
            }
        });
        return deferred.promise;
    },

    //Return Where Condition
    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.id != 'undefined' && searchParams.id != '') {
            where += " AND rating_id = " + searchParams.id;
        }

        if (typeof searchParams.name != 'undefined' && searchParams.name != '') {
            where += " AND rating_name LIKE '%" + striptags(searchParams.name) + "%' ";
        }

        if (typeof searchParams.status != 'undefined' && searchParams.status != '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Rating;