var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Role = {

    addRole: function (data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_role SET ?";
        data.role_name = striptags(data.role_name);

        db.connection.query(query, data, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getRoles: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'role_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT role_id, role_name, status, created_at FROM tbl_role WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleRole: function (role_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_role WHERE ? AND is_deleted = 0";
        db.connection.query(query, { role_id: role_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateRole: function (roleData, role_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_role SET ? WHERE ?';
        roleData.role_name = striptags(roleData.role_name);

        db.connection.query(query, [roleData, { role_id: role_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteRole: function (role_id) {
        var deferred = Q.defer();
        var role_id = role_id;
        var query = "UPDATE tbl_role SET is_deleted = 1 WHERE role_id = " + role_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countRole: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(role_id) as total FROM tbl_role WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No role found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.role_id !== 'undefined' && searchParams.role_id !== '') {
            where += " AND role_id = " + searchParams.role_id;
        }

        if (typeof searchParams.role_name !== 'undefined' && searchParams.role_name !== '') {
            where += " AND role_name LIKE '%" + striptags(searchParams.role_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Role;