var striptags = require('striptags');
var mysql = require('mysql');
var nodemailer = require('nodemailer');
var db = require('../config/database.js');
var commonHelper = require('../helper/common_helper.js');
var Q = require("q");

var Login = {

    //Login Check User
    checkLoginExists: function(username) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += " user_username = '" + striptags(username) + "'";
        }

        //where += " AND user_role_id = " + logintype;
        // where += " AND status = 1";
        // where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            //console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Login User
    getLogin: function(username, password, logintype, device_token, device_type) {
        var deferred = Q.defer();
        var total = 0;
        // var currentDate = commonHelper.getCurrentDateTime();
        var currentDate = '';
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += "user_username = '" + username + "' ";
        }

        if (typeof password != 'undefined' && password != '') {
            where += " AND user_password = '" + password + "' ";
        } 

        where += " AND user_role_id = " + logintype;
        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(error, result, fields) {
            total = result.length;
            

            if(total > 0){
                if (error) {
                    deferred.reject(new Error(error));
                }
                
                var UserId = result[0].user_id;
                // var token = commonHelper.setEncodeString(UserId);
                var token = UserId;
                var querys = 'UPDATE tbl_user SET ? WHERE ?';

                if( (logintype == 3 || logintype == 5) && device_type != 1 ){
                    var data = { device_token: device_token, device_type: device_type, updated_at: currentDate };
                    db.connection.query(querys, [data, { user_id: UserId }], function(err, results, fields) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(result);
                    });
                }else{
                    var data = { device_token: token, device_type: device_type, updated_at: currentDate };
                    db.connection.query(querys, [data, { user_id: UserId }], function(err, results, fields) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(result);
                    });
                }
            }else{
                deferred.resolve(0); 
            }       
            
        });

        return deferred.promise;
    },

    //Register User
    checkUserExists: function(username) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        //console.log(username);
        if (typeof username != 'undefined' && username != '') {
            where += " user_username = '" + striptags(username) + "'";
        }

        // where += " AND status = 1";
        // where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);
        //console.log(query);

        db.connection.query(query, function(err, rows) {
            // console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Add User
    addUser: function(data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_user SET ?";
        //data.name = striptags(data.name);

        db.connection.query(query, data, function(error, result, fields) {
            // console.log(query);
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    //Add User Info
    addUserInfo: function(data) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_clients SET ?";

        db.connection.query(query, data, function(error, result, fields) {

            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    //Check Password
    checkPassword: function(OldPassword, UserId) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof OldPassword != 'undefined' && OldPassword != '') {
            where += " user_password = '" + striptags(OldPassword) + "'";
        }

        where += " AND user_id = " + UserId;
        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            //console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Change Password
    ChangePassword: function(OldPassword, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';
        
        db.connection.query(query, [OldPassword, { user_id: striptags(UserId) }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Forget Password
    forgetPassword: function(Email) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof Email != 'undefined' && Email != '') {
            where += " user_username = '" + striptags(Email) + "'";
        }

        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            //console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            if(rows){
                 
                var transporter = nodemailer.createTransport({
                  service: 'gmail',
                  auth: {
                    user: 'shindepri9@gmail.com',
                    pass: ''
                  }
                });

                var mailOptions = {
                  from: 'shindepri9@gmail.com',
                  to: 'priya@exceptionaire.co',
                  subject: 'User Cafe Credentials',
                  text: 'Username : priya@gmail.com and password : priya'
                };

                transporter.sendMail(mailOptions, function(error, info){
                  if (error) {
                    console.log(error);
                  } else {
                    console.log('Email sent: ' + info.response);
                  }
                });
                 deferred.resolve(rows);   
            }            
        });
        return deferred.promise;
    },

    //User Details
    userDetails: function(userId, usertype) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof userId != 'undefined' && userId != '') {
            where += "user_id = '" + userId + "' ";
        }

        if (typeof usertype != 'undefined' && usertype != '') {
            where += " AND user_role_id = '" + usertype + "' ";
        } 

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(error, result, fields) {
        
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    }

};

module.exports = Login;