var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Demo = {

    addDemo: function (demoData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_demo SET ?";
        // demoqData.demo_data = striptags(demoData.demo_data);
        // console.log(demoData.demo_data);
        
        db.connection.query(query, demoData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getDemos: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'demo_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_demo";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleDemo: function (demo_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_demo WHERE ? ";
        db.connection.query(query, { demo_id: demo_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateDemo: function (demoData, demo_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_demo SET ? WHERE ?';
        demoData.demo_data = striptags(demoData.demo_data);
        
        db.connection.query(query, [demoData, { demo_id: demo_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteDemo: function (demo_id) {
        var deferred = Q.defer();
        var demo_id = demo_id;
       
        var query = "DELETE from tbl_demo WHERE demo_id = " + demo_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countDemo: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(demo_id) as total FROM tbl_demo";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Demo found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.demo_id !== 'undefined' && searchParams.demo_id !== '') {
            where += " AND demo_id = " + searchParams.demo_id;
        }

        if (typeof searchParams.demo_data !== 'undefined' && searchParams.demo_data !== '') {
            where += " AND demo_data LIKE '%" + striptags(searchParams.demo_data) + "%' ";
        }
 
        return where;
    }

};

module.exports = Demo;