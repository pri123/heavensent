var fs = require('fs');
var md5 = require('md5');
var Login = require('../models/Login');
var commonHelper = require('../helper/common_helper.js');

var LoginController = {

    actionIndex: function(req, res, next) {
        var total = 0;

        var Username = req.body.username;
        var Password = md5(req.body.password);
        var LoginType = req.body.usertype;
        var device_token = req.body.device_token;
        var device_type = req.body.device_type;
        
        Login.checkLoginExists(Username).then(function(result) {
        	total = result.length;
            
            if (total > 0) {

            	if (result[0].status == 0) {
	                var response = { "status": 0, "message": "Your Account Deactivated." };
	                res.json(response);
	            } else {

	            	if(result[0].is_suspend == 1){
	            		var response = { "status": 0, "message": "Your Account Suspend." };
	                	res.json(response);
	            	}else{

	            		Login.getLogin(Username, Password, LoginType, device_token, device_type).then(function(results) {

                            // console.log(results);
	            			// var currentDates = commonHelper.setEncodeString("121");
		              //       console.log(currentDates);

		                    // var currentDatess = commonHelper.setDecodeString(results);
		                    // console.log(currentDatess);

                            // var currentDatess = commonHelper.getUserData(3,3);
                            // console.log(currentDatess);
	                     	

		                    if(results == 0){
		                    	var response = { "status": 0, "message": "Username And Password Don't Match." };
		                    	res.json(response);
		                    }else{
		                    	var response = { "status": 1, "message": "Login Successfully.", "data": results };
		                    	res.json(response);
		                    }                    
		                }).catch(function(error) {
		                    var response = { "status": 0, "message": "Try Again." };
		                    res.json(response);
		                });
	            	}	                
	            }      	
                
            } else {
				var response = { "status": 0, "message": "User Does Not Exist." };
        		res.json(response); 	
			}            
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });

    },

    actionCreate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        
        var Username = req.body.username;
        var Password = md5(req.body.password);
        var userRole = req.body.usertype;

        var register = { user_username: req.body.username, created_at: currentDate, updated_at: currentDate, user_password: Password, user_role_id: userRole, device_token: req.body.device_token, device_type: req.body.device_type, social_id: req.body.social_id, user_registertype: req.body.registertype, status: 1 };
        // console.log(req.body);
        
        var total = 0;
        Login.checkUserExists(Username).then(function(resultUser) {
            total = resultUser.length;
            if (total == 0) {
                Login.addUser(register).then(function(result) {

                    var info = { user_id: result, clients_name: req.body.name, clients_email: req.body.email, clients_phone: req.body.mobile };

                    Login.addUserInfo(info).then(function(results) {                        
                        // var userInfo = [];
                        commonHelper.getUserData(result, userRole, function(Data){
                            // userInfo = {};
                            var response = { "status": 1, "message": "Add User", "data": Data };
                            res.json(response);
                        });                        

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Add User failed" };
                        res.json(response);
                    });
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Add User failed" };
                    res.json(response);
                });
            } else {
                var response = { "status": 1, "message": "Email Id Already Exist." };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });
    },

    //Change Password
    actionCreatePass: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var password = { user_password: req.body.newpassword, updated_at: currentDate };

        var UserId = req.body.user_id;
        var NewPassword = req.body.newpassword;
        var OldPassword = req.body.oldpassword;

        var total = 0;

        Login.checkPassword(OldPassword, UserId).then(function(result) {
            total = result.length;
            if (total > 0) {
                //console.log(total);
                Login.ChangePassword(password, UserId).then(function(result) {
                    var response = { "status": 1, "message": "Password Changed." };
                    res.json(response);                    
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Add User failed" };
                    res.json(response);
                });
            } else {
                var response = { "status": 1, "message": "Old Password Wrong." };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });
    },

    //Forget Password
    actionForget: function(req, res, next) {

        var Email = req.body.user_username;

        var total = 0;

        var totals = 0;
        Login.checkLoginExists(Email).then(function(result) {

            totals = result.length;
             
            if (totals > 0) {
            	if (result[0].status == 0) {
	                var response = { "status": 0, "message": "Your Account Deactivated." };
	                res.json(response);
	            } else {

	            	if(result[0].is_suspend == 1){
	            		var response = { "status": 0, "message": "Your Account Suspend." };
	                	res.json(response);
	            	}else{
	            		if (typeof Email != 'undefined' && Email != '') {
				            Login.forgetPassword(Email).then(function(result) {
				                total = result.length;
				                if (total > 0) {
				                    var response = { "status": 1, "message": "Email Send" };
				                    res.json(response);
				                }else{
				                    var response = { "status": 1, "message": "Email Id Not Exist" };
				                    res.json(response);
				                }
				            }).catch(function(error) {
				                var response = { "status": 1, "message": "Try Again." };
				                res.json(response);
				            });
				        } else {
				            var response = { "status": 0, "message": "Try Again." };
				            res.json(response);
				        }
	            	}	                
	            } 
            }else{
            	var response = { "status": 0, "message": "User Does Not Exist." };
        		res.json(response); 
            }
        });        
    }    

}

module.exports = LoginController;