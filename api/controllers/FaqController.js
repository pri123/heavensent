var fs = require('fs');
var Faq = require('../models/Faq');
var commonHelper = require('../helper/common_helper.js');

var FaqController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            faq_id: req.body.faq_id,
            faq_question: req.body.faq_question,
            faq_answer: req.body.faq_answer,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Faq.countFaq(searchParams).then(function (result) {
            total = result[0].total;
            Faq.getFaqs(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "faqs": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var faq_id = req.body.faq_id;

        if (typeof faq_id !== 'undefined' && faq_id !== '') {
            Faq.singleFaq(faq_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "faq": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var faqData = {
            faq_question: req.body.faq_question,
            faq_answer: req.body.faq_answer,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        Faq.addFaq(faqData).then(function (result) {
            var response = { "status": 1, "message": "FAQ created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "FAQ create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var faq_id = req.body.faq_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var faqData = {
            faq_question: req.body.faq_question,
            faq_answer: req.body.faq_answer,
            status: req.body.status,
            updated_at: currentDate
        };

        Faq.updateFaq(faqData, faq_id).then(function (result) {
            var response = { "status": 1, "message": "FAQ updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "FAQ update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this FAQ!" };
        var faq_id = req.body.faq_id;

        Faq.deleteFaq(faq_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your FAQ record has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    }

}

module.exports = FaqController;