var fs = require('fs');
var Worker = require('../models/Worker');
var commonHelper = require('../helper/common_helper.js');

var WorkerController = {


    //ADD USER WORKERS
    actionCreate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var addWorker = { user_username: req.body.username, created_at: currentDate, updated_at: currentDate, user_password: req.body.password, user_role_id: req.body.usertype, user_registertype: req.body.registertype , device_token: req.body.device_token , device_type: req.body.device_type };
        var Username = req.body.username;
        var total = 0;
        //console.log(addWorker);

        Worker.checkUsernameExists(Username).then(function(result) {
            total = result.length;
            console.log(total);
            if (total == 0) {

                Worker.addWorkers(addWorker).then(function(result) {
                    //console.log(result);
                    var Info = { user_id: result, workers_name: req.body.name, workers_email: req.body.username, workers_phone: req.body.phone, workers_address: req.body.address, workers_state: req.body.state, workers_country: req.body.country, workers_zip: req.body.zip };
                    Worker.addInfo(Info).then(function(result) {
                        var response = { "status": 1, "message": "Add Worker" };
                        res.json(response);
                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Add Worker failed" };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Add Worker failed" };
                    res.json(response);
                });
            } else {
                var response = { "status": 1, "message": "Email Id Already Exist." };
                res.json(response);
            }

        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });

    },

    //EDIT WORKERS
    actionUpdate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var workers_id = req.body.id;
        var workersData = { workers_name: req.body.name, workers_phone: req.body.phone, workers_address: req.body.address, workers_state: req.body.state, workers_country: req.body.country, workers_zip: req.body.zip };

        Worker.updateWorkers(workers_id, workersData).then(function(result) {

            var response = { "status": 1, "message": "Workers Data Updated" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Workers Update failed" };
            res.json(response);
        });

    }

}

module.exports = WorkerController;