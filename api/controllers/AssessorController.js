var fs = require('fs');
var Assessor = require('../models/Assessor');
var commonHelper = require('../helper/common_helper.js');

var AssessorController = {


    //ADD USER WORKERS
    actionCreate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var addAssessor = { user_username: req.body.username, created_at: currentDate, updated_at: currentDate, user_password: req.body.password, user_role_id: req.body.usertype, user_registertype: req.body.registertype };
        var Username = req.body.username;
        var total = 0;
        //console.log(addAssessor);

        Assessor.checkUsernameExists(Username).then(function(result) {
            total = result.length;
            //console.log(total);
            if (total == 0) {

                Assessor.addAssessors(addAssessor).then(function(result) {
                    //console.log(result);
                    var Info = { user_id: result, assessors_name: req.body.name, assessors_email: req.body.username, assessors_phone: req.body.phone, assessors_address: req.body.address, assessors_state: req.body.state, assessors_country: req.body.country, assessors_zip: req.body.zip };
                    //console.log(Info);
                    Assessor.addInfo(Info).then(function(result) {
                        var response = { "status": 1, "message": "Add Assessor" };
                        res.json(response);
                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Add Assessor failed" };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Add Assessor failed" };
                    res.json(response);
                });
            } else {
                var response = { "status": 1, "message": "Email Id Already Exist." };
                res.json(response);
            }

        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });

    },

    //EDIT WORKERS
    actionUpdate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var assessors_id = req.body.id;
        var assessorsData = { assessors_name: req.body.name, assessors_phone: req.body.phone, assessors_address: req.body.address, assessors_state: req.body.state, assessors_country: req.body.country, assessors_zip: req.body.zip };

        Assessor.updateAssessors(assessors_id, assessorsData).then(function(result) {
            var response = { "status": 1, "message": "Assessors Data Updated" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Assessors Update failed" };
            res.json(response);
        });

    }

}

module.exports = AssessorController;