var fs = require('fs');
var md5 = require('md5');
var User = require('../models/User');
var commonHelper = require('../helper/common_helper.js');

var UserController = {

    // All User List
    actionIndex: function(req, res, next) {
    	var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;
        var usertype = req.body.usertype;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            user_id: req.body.user_id,
            user_username: req.body.user_username,
            status: req.body.status,
            created_at: req.body.created_at,
            is_suspend: req.body.is_suspend
        };
        // console.log(searchParams);
        User.countUser(searchParams, usertype).then(function (result) {
        	total = result[0].total;
            User.getUser(limit, offset, searchParams, orderParams, usertype).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "users": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });


        // var UserType = req.body.usertype;
        
        // User.getUser(UserType).then(function(results) {
        //     //console.log(results);
        //     total = results.length;
        //     if(total > 0){
        //         var response = { "status": 1, "message": "User Successfully.", "data": results };
        //         res.json(response);
        //     }else{
        //         var response = { "status": 1, "message": "No Record Found."};
        //         res.json(response);
        //     }            
        // }).catch(function(error) {
        //     var response = { "status": 0, "message": "Try Again." };
        //     res.json(response);
        // });
    },

    //Delete User
    actionDelete: function(req, res, next) {
        var UserId = req.body.user_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { is_deleted: 1 };

        User.deleteUser(data, UserId).then(function(result) {
            var response = { "status": 1, "message": "User Deleted" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    //Status User
    actionStatus: function(req, res, next) {
        var UserId = req.body.user_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        User.statusUser(data, UserId).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Account Actived" };                
            }else{
                var response = { "status": 1, "message": "Account Deactived" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    //Suspend User
    actionSuspend: function(req, res, next) {
        var UserId = req.body.user_id;
        var Suspend = req.body.is_suspend;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { is_suspend: req.body.is_suspend };

        User.suspendUser(data, UserId).then(function(result) {
            if(Suspend == 1){
                var response = { "status": 1, "message": "Account Actived For Suspend" };                
            }else{
                var response = { "status": 1, "message": "Account Deactived For Suspend" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    //ADD USER INTERNATE CAFE
    actionCreate: function(req, res, next) {
    	var currentDate = commonHelper.getCurrentDateTime();
        var addUsers = { user_username: req.body.username, created_at: currentDate, updated_at: currentDate, user_password: md5(req.body.username), user_role_id: req.body.usertype, user_registertype: req.body.registertype };
        var Username = req.body.username;
        var Email = req.body.email;   
        var UserType = req.body.usertype;     
        var total = 0;
        var totalEmail = 0;
        //console.log(addUsers);
        User.checkUserExists(Username).then(function(result) {
            total = result.length;
            if (total == 0) {
            	User.checkUserEmailExists(Email).then(function(results) {
            		totalEmail = results.length;
            		if (totalEmail == 0) {

		                User.addUser(addUsers).then(function(result) {
		                    // var Info = { user_id: result, cafeusers_name: req.body.name, cafeusers_email: req.body.email, cafeusers_phone: req.body.phone, cafeusers_address: req.body.address, cafeusers_state: req.body.state, cafeusers_country: req.body.country, cafeusers_zip: req.body.zip, cafeusers_location: req.body.location };
                            if(UserType == 1){
                                var Info = { user_id: result, admin_name: req.body.name, admin_email: req.body.email, admin_phone: req.body.phone, admin_address: req.body.address };
                            }else{
                                if(UserType == 2){
                                    var Info = { user_id: result, cafeusers_name: req.body.name, cafeusers_email: req.body.email, cafeusers_phone: req.body.phone, cafeusers_address: req.body.address };
                                }else{
                                    if(UserType == 3){
                                        var Info = { user_id: result, workers_name: req.body.name, workers_email: req.body.email, workers_phone: req.body.phone, workers_address: req.body.address, cafeuser_id: req.body.cafeID };
                                    }else{
                                        if(UserType == 4){
                                            var Info = { user_id: result, assessors_name: req.body.name, assessors_email: req.body.email, assessors_phone: req.body.phone, assessors_address: req.body.address };
                                        }else{
                                        	var Info = { user_id: result, clients_name: req.body.name, clients_email: req.body.email, clients_phone: req.body.phone, clients_address: req.body.address };
                                        }
                                    }
                                }
                            }
                            
		                    User.addInfo(Info, UserType).then(function(result) {
		                        var response = { "status": 1, "message": "Added Successfully" };
		                        res.json(response);
		                    }).catch(function(error) {
		                        var response = { "status": 0, "message": "Added failed" };
		                        res.json(response);
		                    });

		                }).catch(function(error) {
		                    var response = { "status": 0, "message": "Added failed" };
		                    res.json(response);
		                });
		            }else{
		            	var response = { "status": 3, "message": "Email ID Already Exist." };
                		res.json(response);
		            }
                });
            } else {            	
                var response = { "status": 2, "message": "User Name Already Exist." };
                res.json(response);
            }

        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });

    },

    //EDIT USER INTERNATE CAFE
    actionUpdate: function(req, res, next) {
    	
        var currentDate = commonHelper.getCurrentDateTime();
        var user_id = req.body.user_id;
        var usertype = req.body.usertype;
        if(usertype == 1){
            var Data = { admin_name: req.body.name, admin_phone: req.body.phone, admin_address: req.body.address };
            var userData = { status: req.body.status, updated_at: currentDate };
        }else{
            if(usertype == 2){
            	var Data = { cafeusers_name: req.body.name, cafeusers_phone: req.body.phone, cafeusers_address: req.body.address };
            	var userData = { status: req.body.status, updated_at: currentDate };
            }else{
                if(usertype == 3){
                    var Data = { workers_name: req.body.name, workers_phone: req.body.phone, workers_address: req.body.address };
                    var userData = { status: req.body.status, updated_at: currentDate };
                }else{
                    if(usertype == 4){
                        var Data = { assessors_name: req.body.name, assessors_phone: req.body.phone, assessors_address: req.body.address };
                        var userData = { status: req.body.status, updated_at: currentDate };
                    }else{
                    	var Data = { clients_name: req.body.name, clients_phone: req.body.phone, clients_address: req.body.address };
                        var userData = { status: req.body.status, updated_at: currentDate };
                    }
                }
            } 
        }       

        User.updateUser(user_id, usertype, Data, userData).then(function(result) {
        	var response = { "status": 1, "message": "Updated Successfully" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Updated failed" };
            res.json(response);
        });

    },

    actionView: function (req, res, next) {
    	var user_id = req.body.user_id;
        var UserType = req.body.UserType;
        // console.log(req.body);
        if (typeof user_id !== 'undefined' && user_id !== '') {
            User.singleUser(user_id, UserType).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionAllList: function (req, res, next) {
        var user_id = req.body.usertype;
        if (typeof user_id !== 'undefined' && user_id !== '') {
            User.getAllUser(user_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    }

}

module.exports = UserController;