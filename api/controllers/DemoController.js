var fs = require('fs');
var Demo = require('../models/Demo');
var commonHelper = require('../helper/common_helper.js');

var DemoController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            demo_id: req.body.demo_id,
            demo_data: req.body.demo_data
        };

        Demo.countDemo(searchParams).then(function (result) {
            total = result[0].total;
            Demo.getDemos(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "demos": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var demo_id = req.body.demo_id;

        if (typeof demo_id !== 'undefined' && demo_id !== '') {
            Demo.singleDemo(demo_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "demo": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var demoData = {
            demo_data: req.body.demo_data
         };
             
        Demo.addDemo(demoData).then(function (result) {
            var response = { "status": 1, "message": "Demo created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Demo create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var demo_id = req.body.demo_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var demoData = {
            demo_data: req.body.demo_data
        };

        Demo.updateDemo(demoData, demo_id).then(function (result) {
            var response = { "status": 1, "message": "Demo updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Demo update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        
        var response = { status: 0, message: "Unable to delete this Demo!" };
        var demo_id = req.body.demo_id;

        Demo.deleteDemo(demo_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Demo record has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    }

}

module.exports = DemoController;