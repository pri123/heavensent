var fs = require('fs');
var Rating = require('../models/Rating');
var commonHelper = require('../helper/common_helper.js');

var RatingController = {

    // All Rating List
    actionIndex: function(req, res, next) {

        var limit = 10;
        var total = 0;
        var page = parseInt(req.body.pageNum) || 1;
        var offset = (limit * page) - limit;
        
        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            id: req.body.id,
            name: req.body.name,
            status: req.body.status,
            created_at: req.body.created_at
        };
        // console.log(req.body);
        // return false;

        Rating.countRating(searchParams).then(function (result) {
            total = result[0].total;
            Rating.getRating(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Rating Successfully.", "data": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });

        // Rating.getRating(limit, offset, searchParams, orderParams).then(function(results) {
        //     //console.log(results);
        //     total = results.length;
        //     if(total > 0){
        //         var response = { "status": 1, "message": "Rating Successfully.", "data": results };
        //         res.json(response);
        //     }else{
        //         var response = { "status": 1, "message": "No Record Found."};
        //         res.json(response);
        //     }            
        // }).catch(function(error) {
        //     var response = { "status": 0, "message": "Try Again." };
        //     res.json(response);
        // });
    },

    //Delete Rating Name
    actionDelete: function(req, res, next) {
        var RatingId = req.body.rating_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { is_deleted: 1 };

        Rating.deleteRating(data, RatingId).then(function(result) {
            var response = { "status": 1, "message": "Rating Deleted" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    //Status Rating Name
    actionStatus: function(req, res, next) {
        var RatingId = req.body.rating_id;
        var Status = req.body.status_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status_id };
        Rating.statusRating(data, RatingId).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Account Actived" };                
            }else{
                var response = { "status": 1, "message": "Account Deactived" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    // Add Rating Name
    actionCreate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        if(req.body.status == 1){
            var rating = { rating_name: req.body.name, created_at: currentDate, updated_at: currentDate, status: req.body.status };
        }else{
            var rating = { rating_name: req.body.name, created_at: currentDate, updated_at: currentDate };
        }        
        
        var Ratingname = req.body.name;
        var total = 0;
        Rating.checkNameExists(Ratingname).then(function(result) {
            total = result.length;
            // console.log(total);
            if (total == 0) {
                Rating.addRating(rating).then(function(result) {
                    var response = { "status": 1, "message": "Add Rating" };
                    res.json(response);
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Add Rating failed" };
                    res.json(response);
                });
            } else {
                var response = { "status": 1, "message": "Rating Name Already Exist." };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });
    },

    // Edit Rating Name
    actionUpdate: function(req, res, next) {
        var rating_id = req.body.rating_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var rating = { rating_name: req.body.name, updated_at: currentDate };

        Rating.updateRating(rating, rating_id).then(function(result) {
            var response = { "status": 1, "message": "Rating Updated" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Rating Update failed" };
            res.json(response);
        });

    },

    // Get Rating By Id
    actionGetRatingById: function(req, res, next) {
        var ratingId = req.body.rating_id;
        
        Rating.getRatingById(ratingId).then(function(result) {
            var response = { "status": 1, "message": "Rating Data", "data": result };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Rating Not Found" };
            res.json(response);
        });

    } 

}

module.exports = RatingController;