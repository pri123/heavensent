var express = require('express');
var router = express.Router();

var util = require("util"); 
var fs = require("fs");
var path = require('path');
var url = require('url');
var commonHelper = require('../helper/common_helper.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  //res.send('respond with a resource');
  	var db = req.con;
	var data = "";
	db.query('SELECT * FROM test where is_deleted = 0',function(err,rows){
		//if(err) throw err;
		//console.log(rows);
		var data = {"userlist" : rows };
		//res.render('userIndex', { title: 'User Information', dataGet: data });
		res.send(data);
	});
});

/* POST add user. */
router.post('/addUser', function(req, res, next) {
	var db = req.con;
	//console.log("FormData "+ JSON.stringify(req.body));
	var currentDate = commonHelper.getCurrentDateTime();
	var data = {
			test_name: req.body.name,
			test_date: req.body.date,
			created_at: currentDate,
			updated_at: currentDate
		};
	
	db.query('INSERT INTO test set ? ', data , function(err,rows){
		//if(err) throw err;
		res.setHeader('Content-Type', 'application/json');
		
		var respons = { "message" : "Add User"};
		res.send(respons);
	});
	
});

/* PUT update user. */
router.put('/update', function(req, res, next) {
	//console.log("---"+JSON.stringify(req.body));
	var db = req.con;
	var currentDate = commonHelper.getCurrentDateTime();
	var data = {
			test_name: req.body.name,
			test_date: req.body.date,
			updated_at: currentDate
		}
	db.query('UPDATE test set ? WHERE test_id = ?',[data, req.body.id] ,function(err,rows){
		if(err){
			res.send(JSON.stringify({'status': 0, 'msg': 'Error updating user', 'raw': JSON.stringify(req.body)}));
		}
		res.send(JSON.stringify({'status': 1, 'msg': 'User updated'}));
	});
});

/* DELETE delete user */
router.delete('/delete/:id', function(req, res) {
	//var url_parts = url.parse(req.url, true); // Read parameter from url if any
	//console.log("---"+JSON.stringify(url_parts.query));
	//console.log("---"+JSON.stringify(req.params));
	var db = req.con;
	db.query('DELETE FROM test WHERE test_id = ? ',[req.params.id] ,function(err,rows){
		if(err){
			res.send(JSON.stringify({'status': 0, 'msg': 'Error User deleted', 'raw': JSON.stringify(req.params)}));
		}
		res.send(JSON.stringify({'status': 1, 'msg': 'User deleted'}));
	});
});


module.exports = router;
