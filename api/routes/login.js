var express = require('express');
var router = express.Router();
var LoginController = require('../controllers/LoginController');

router.post('/', LoginController.actionIndex);

router.post('/register', LoginController.actionCreate);

router.post('/changepassword', LoginController.actionCreatePass);

router.post('/forgetpassword', LoginController.actionForget);

// router.post('/validate-user', LoginController.actionValidateUser);

// router.post('/delete', LoginController.actionDelete);

module.exports = router;