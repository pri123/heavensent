var express = require('express');
var router = express.Router();
var RoleController = require('../../controllers/RoleController');

router.post('/', RoleController.actionIndex);
router.post('/add', RoleController.actionCreate);
router.post('/edit', RoleController.actionUpdate);
router.post('/view', RoleController.actionView);
router.post('/delete', RoleController.actionDelete);

module.exports = router;