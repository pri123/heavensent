var express = require('express');
var router = express.Router();
var WorkerController = require('../../controllers/WorkerController');


router.post('/create', WorkerController.actionCreate);

router.post('/update', WorkerController.actionUpdate);

module.exports = router;