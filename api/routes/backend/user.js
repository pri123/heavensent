var express = require('express');
var router = express.Router();
var UserController = require('../../controllers/UserController');

router.post('/', UserController.actionIndex);

router.post('/delete', UserController.actionDelete);

router.post('/status', UserController.actionStatus);

router.post('/suspend', UserController.actionSuspend);

router.post('/create', UserController.actionCreate);

router.post('/update', UserController.actionUpdate);

router.post('/view', UserController.actionView);

router.post('/AllList', UserController.actionAllList);


// router.post('/changepassword', LoginController.actionCreatePass);

// router.post('/edit', LoginController.actionUpdate);

// router.post('/delete', LoginController.actionDelete);

module.exports = router;