var express = require('express');
var router = express.Router();
var path = require('path');
var DemoController = require('../../controllers/DemoController');

router.post('/', DemoController.actionIndex);
router.post('/add', DemoController.actionCreate);
router.post('/edit', DemoController.actionUpdate);
router.post('/view', DemoController.actionView);
router.post('/delete', DemoController.actionDelete);

module.exports = router;