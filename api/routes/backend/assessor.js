var express = require('express');
var router = express.Router();
var AssessorController = require('../../controllers/AssessorController');


router.post('/create', AssessorController.actionCreate);

router.post('/update', AssessorController.actionUpdate);

module.exports = router;