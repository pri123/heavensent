var express = require('express');
var router = express.Router();
var path = require('path');
var ChecklistController = require('../../controllers/ChecklistController');

router.post('/', ChecklistController.actionIndex);
router.post('/add', ChecklistController.actionCreate);
router.post('/edit', ChecklistController.actionUpdate);
router.post('/view', ChecklistController.actionView);
router.post('/status', ChecklistController.actionStatus);
router.post('/delete', ChecklistController.actionDelete);

module.exports = router;