var express = require('express');
var router = express.Router();
var RatingController = require('../../controllers/RatingController');

router.post('/', RatingController.actionIndex);

router.post('/add', RatingController.actionCreate);

router.post('/edit', RatingController.actionUpdate);

router.post('/getRatingById', RatingController.actionGetRatingById);

router.post('/status', RatingController.actionStatus);

router.post('/delete', RatingController.actionDelete);

module.exports = router;