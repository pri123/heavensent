var express = require('express');
var router = express.Router();
var CategoryController = require('../controllers/CategoryController');

router.post('/', CategoryController.actionIndex);

router.post('/show', CategoryController.actionShow);

router.post('/create', CategoryController.actionCreate);

router.post('/edit', CategoryController.actionUpdate);

router.post('/delete', CategoryController.actionDelete);

module.exports = router;