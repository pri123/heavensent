var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');


//Test
var routes = require('./routes/index');
var users = require('./routes/users');
var categories = require('./routes/categories');

//Live
var login = require('./routes/login');
var user = require('./routes/backend/user');
var rating = require('./routes/backend/rating');
var role = require('./routes/backend/role');
var faq = require('./routes/backend/faq');
var demo = require('./routes/backend/demo');
var checklist = require('./routes/backend/checklist');
var skills = require('./routes/backend/skills');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Enable CORS from client-side
 */

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials, total_roles");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

//Test
app.use('/', routes);
app.use('/users', users);
app.use('/categories', categories);

//Live
app.use('/login', login);
app.use('/user', user);
app.use('/rating', rating);
app.use('/role', role);
app.use('/faq', faq);
app.use('/demo', demo);
app.use('/checklist', checklist);
app.use('/skills', skills);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}


app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;