var gulp = require('gulp'),
    gp_concat = require('gulp-concat'),
    gp_rename = require('gulp-rename'),
    gp_uglify = require('gulp-uglify');

gulp.task('backoffice_vendor_css', function () {
    var backoffice_css_files = [
        './assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css',
        './assets/admin/css/colors/red.css',
        './vendor/angular-toastr/dist/angular-toastr.css',
        './vendor/angucomplete-alt/angucomplete-alt.css',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.css',
        './vendor/angular-material/angular-material.css',
        './vendor/oi.select/dist/select.min.css',
        './assets/common/css/sweetalert.css',
        './assets/admin/css/custom.css',
        './assets/common/css/custom.css'
    ]

    return gulp.src(backoffice_css_files)
        .pipe(gp_concat('backoffice_vendor.css'))
        .pipe(gulp.dest('backoffice/dist'));
});

gulp.task('backoffice_vendor_js', function () {
    var backoffice_js_files = [
        './vendor/jquery/dist/jquery.min.js',
        './assets/admin/assets/plugins/bootstrap/js/popper.min.js',
        './assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js',
        './assets/admin/js/jquery.slimscroll.js',
        './assets/admin/js/waves.js',
        './vendor/angular/angular.min.js',
        './vendor/angular-ui-router/release/angular-ui-router.min.js',
        './vendor/angular-sanitize/angular-sanitize.min.js',
        './vendor/angular-toastr/dist/angular-toastr.tpls.js',
        './vendor/tg-angular-validator/dist/angular-validator.min.js',
        './vendor/angular-animate/angular-animate.min.js',
        './vendor/angularUtils-pagination/dirPagination.js',
        './vendor/ngSweetAlert/SweetAlert.min.js',
        './assets/common/js/sweetalert.min.js',
        './vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
        './vendor/ng-file-upload/ng-file-upload.min.js',
        './vendor/moment/min/moment-with-locales.js',
        './vendor/lodash/lodash.js',
        './vendor/angucomplete-alt/dist/angucomplete-alt.min.js',
        './vendor/angular-aria/angular-aria.js',
        './vendor/angular-animate/angular-animate.min.js',
        './vendor/angularUtils-pagination/dirPagination.js',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.js',
        './vendor/angular-material/angular-material.js',
        './vendor/oi.select/dist/select-tpls.min.js',
        './vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',
        './assets/common/js/custom.js'
    ]

    return gulp.src(backoffice_js_files)
        .pipe(gp_concat('backoffice_vendor.js'))
        .pipe(gulp.dest('backoffice/dist'));
});


var backOfficePath = './backoffice/app/components/'
gulp.task('backoffice_app_js', function () {
    var backoffice_app_files = [

        backOfficePath + 'auth/auth.module.js',
        backOfficePath + 'auth/auth.service.js',
        backOfficePath + 'auth/auth.controller.js',

        backOfficePath + 'dashboard/dashboard.controller.js',
        backOfficePath + 'dashboard/dashboard.service.js',

        backOfficePath + 'rating/rating.module.js',
        backOfficePath + 'rating/rating.service.js',
        backOfficePath + 'rating/rating.controller.js',

        backOfficePath + 'role/role.module.js',
        backOfficePath + 'role/role.service.js',
        backOfficePath + 'role/role.controller.js',

        backOfficePath + 'faq/faq.module.js',
        backOfficePath + 'faq/faq.service.js',
        backOfficePath + 'faq/faq.controller.js',


        backOfficePath + 'editors/editors.module.js',
        backOfficePath + 'editors/editors.service.js',
        backOfficePath + 'editors/editors.controller.js',

        backOfficePath + 'cafeUser/cafeUser.module.js',
        backOfficePath + 'cafeUser/cafeUser.service.js',
        backOfficePath + 'cafeUser/cafeUser.controller.js',

        backOfficePath + 'workerUser/workerUser.module.js',
        backOfficePath + 'workerUser/workerUser.service.js',
        backOfficePath + 'workerUser/workerUser.controller.js',

        backOfficePath + 'assessorsUser/assessorsUser.module.js',
        backOfficePath + 'assessorsUser/assessorsUser.service.js',
        backOfficePath + 'assessorsUser/assessorsUser.controller.js',

        backOfficePath + 'clientUser/clientUser.module.js',
        backOfficePath + 'clientUser/clientUser.service.js',
        backOfficePath + 'clientUser/clientUser.controller.js',

        backOfficePath + 'adminUser/adminUser.module.js',
        backOfficePath + 'adminUser/adminUser.service.js',
        backOfficePath + 'adminUser/adminUser.controller.js',

        backOfficePath + 'demo/demo.module.js',
        backOfficePath + 'demo/demo.service.js',
        backOfficePath + 'demo/demo.controller.js',

        backOfficePath + 'checklist/checklist.module.js',
        backOfficePath + 'checklist/checklist.service.js',
        backOfficePath + 'checklist/checklist.controller.js', 
        
        backOfficePath + 'skills/skills.module.js',
        backOfficePath + 'skills/skills.service.js',
        backOfficePath + 'skills/skills.controller.js',                

        backOfficePath + 'backoffice.module.js',
        backOfficePath + 'backoffice.controller.js',
        'backoffice/app.js'
    ]

    return gulp.src(backoffice_app_files)
        .pipe(gp_concat('backoffice_app.js'))
        .pipe(gulp.dest('backoffice/dist'));
});


//frontoffice gulp files start here

gulp.task('frontoffice_vendor_css', function () {
    var frontoffice_css_files = [
        './assets/front/css/bootstrap.min.css',
        './assets/front/css/media.css',
        './assets/front/css/style.css',
        './assets/front/css/font-awesome/css/font-awesome.css',
        './assets/front/fonts/stylesheet.css',
        './vendor/angular-toastr/dist/angular-toastr.css',
        './vendor/angucomplete-alt/angucomplete-alt.css',
        // './vendor/angularjs-datetime-picker/angularjs-datetime-picker.css',
        './vendor/angular-material/angular-material.css',
        './vendor/oi.select/dist/select.min.css',
        './assets/common/css/sweetalert.css'       
    ]

    return gulp.src(frontoffice_css_files)
        .pipe(gp_concat('frontoffice_vendor_css.css'))
        .pipe(gulp.dest('frontoffice/dist'));
});

gulp.task('frontoffice_vendor_js', function () {
    var frontoffice_js_files = [
        './vendor/jquery/dist/jquery.min.js',
        './assets/admin/assets/plugins/bootstrap/js/popper.min.js',
        './assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js',
        './assets/admin/js/jquery.slimscroll.js',
        // './assets/admin/js/waves.js',
        './vendor/angular/angular.min.js',
        './vendor/angular-ui-router/release/angular-ui-router.min.js',
        './vendor/angular-sanitize/angular-sanitize.min.js',
        './vendor/angular-toastr/dist/angular-toastr.tpls.js',
        './vendor/tg-angular-validator/dist/angular-validator.min.js',
        './vendor/angular-animate/angular-animate.min.js',
        './vendor/angularUtils-pagination/dirPagination.js',
        './vendor/ngSweetAlert/SweetAlert.min.js',
        './assets/common/js/sweetalert.min.js',
        './vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
        './vendor/ng-file-upload/ng-file-upload.min.js',
        './vendor/moment/min/moment-with-locales.js',
        './vendor/lodash/lodash.js',
        './vendor/angucomplete-alt/dist/angucomplete-alt.min.js',
        './vendor/angular-aria/angular-aria.js',
        // './vendor/angularjs-datetime-picker/angularjs-datetime-picker.js',
        './vendor/angular-material/angular-material.js',
        './vendor/oi.select/dist/select-tpls.min.js',
        './vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',
        './assets/front/js/jquery-clock-timepicker.min.js',
        './assets/front/js/jquery.min.js',
        './assets/front/js/bootstrap.min.js'
        // './assets/front/js/custom.js'
    ]

    return gulp.src(frontoffice_js_files)
        .pipe(gp_concat('frontoffice_vendor_js.js'))
        .pipe(gulp.dest('frontoffice/dist'));
});


var frontPath = './frontoffice/app/components/'
gulp.task('frontoffice_app_js', function () {
    var frontoffice_app_files = [

        frontPath + 'auth/auth.module.js',
        frontPath + 'auth/auth.service.js',
        frontPath + 'auth/auth.controller.js',

        frontPath + 'worker/worker.module.js',
        frontPath + 'worker/worker.service.js',
        frontPath + 'worker/worker.controller.js',  

        frontPath + 'assessor/assessor.module.js',
        frontPath + 'assessor/assessor.service.js',
        frontPath + 'assessor/assessor.controller.js',        

        // frontPath + 'shared/shared.module.js',
        // frontPath + 'shared/shared.service.js',

        // frontPath + 'staticpages/staticpages.module.js',
        // frontPath + 'staticpages/staticpages.service.js',
        // frontPath + 'staticpages/staticpages.controller.js',

        // frontPath + 'home/home.module.js',
        // frontPath + 'home/home.controller.js',
        // frontPath + 'home/home.service.js',

        // frontPath + 'dashboard/dashboard.module.js',
        // frontPath + 'dashboard/dashboard.service.js',
        // frontPath + 'dashboard/dashboard.controller.js',

        // frontPath + 'orders/order.module.js',
        // frontPath + 'orders/order.controller.js',
        // frontPath + 'orders/order.service.js',

        // frontPath + 'placeOrder/placeOrder.module.js',
        // frontPath + 'placeOrder/placeOrder.controller.js',
        // frontPath + 'placeOrder/placeOrder.service.js',

        // frontPath + 'calendar/calendar.module.js',
        // frontPath + 'calendar/calendar.controller.js',
        // frontPath + 'calendar/calendar.service.js',

        frontPath + 'frontoffice.module.js',
        frontPath + 'frontoffice.controller.js',
        'app.js'
    ]

    return gulp.src(frontoffice_app_files)
        .pipe(gp_concat('frontoffice_app.js'))
        .pipe(gulp.dest('frontoffice/dist'));
});



gulp.task('default', ['backoffice_vendor_css', 'backoffice_vendor_js', 'backoffice_app_js', 'frontoffice_vendor_css', 'frontoffice_vendor_js', 'frontoffice_app_js'], function () {
    gulp.watch('backoffice/**/*.*', function () {
        gulp.run('backoffice_app_js');
    });

    gulp.watch('frontoffice/**/*.*', function () {
        gulp.run('frontoffice_app_js');
    });

    gulp.watch('assets/**/*.*', function () {
        gulp.run('backoffice_vendor_css');
        gulp.run('backoffice_vendor_js');
        gulp.run('frontoffice_vendor_css');
        gulp.run('frontoffice_vendor_js');
    });    
});


// gulp.task('default', ['backoffice_vendor_css', 'backoffice_vendor_js', 'backoffice_app_js'], function () {
//     gulp.watch('backoffice/**/*.*', function () {
//         gulp.run('backoffice_app_js');
//     });
//     gulp.watch('assets/**/*.*', function () {
//         gulp.run('backoffice_vendor_css');
//         gulp.run('backoffice_vendor_js');
//     });
// });
