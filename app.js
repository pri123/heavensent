"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngFileUpload',    
    'frontofficeApp'
]);

app.constant('APPCONFIG', {
    'APIURL': 'http://'+window.location.hostname+':3000/'
});

// app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
//     $urlRouterProvider.otherwise('/');    
// });

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    // paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');

});