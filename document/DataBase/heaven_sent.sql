-- 22/01/2018 
-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2018 at 06:54 PM
-- Server version: 5.5.58-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `heaven_sent`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assessors`
--

CREATE TABLE IF NOT EXISTS `tbl_assessors` (
  `assessors_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `assessors_name` varchar(255) NOT NULL,
  `assessors_email` varchar(255) NOT NULL,
  `assessors_phone` varchar(255) NOT NULL,
  `assessors_address` varchar(255) NOT NULL,
  `assessors_state` int(11) NOT NULL,
  `assessors_country` int(11) NOT NULL,
  `assessors_zip` varchar(255) NOT NULL,
  `assessors_lag` varchar(255) NOT NULL,
  `assessors_long` varchar(255) NOT NULL,
  `assessors_suspend_start` varchar(255) NOT NULL,
  `assessors_suspend_end` varchar(255) NOT NULL,
  PRIMARY KEY (`assessors_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_booking`
--

CREATE TABLE IF NOT EXISTS `tbl_booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_client_id` int(11) NOT NULL,
  `booking_worker_id` int(11) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` int(11) NOT NULL,
  `booking_servicetypeestimate_id` int(11) NOT NULL,
  `booking_task_id` int(11) NOT NULL,
  `booking_substitute` int(11) NOT NULL,
  `booking_start_time` varchar(255) NOT NULL,
  `booking_end_time` varchar(255) NOT NULL,
  `booking_payment_status` int(11) NOT NULL,
  `booking_status` tinyint(4) NOT NULL DEFAULT '0',
  `booking_client_status` tinyint(4) NOT NULL DEFAULT '0',
  `booking_worker_status` tinyint(4) NOT NULL DEFAULT '0',
  `booking_created_at` varchar(255) NOT NULL,
  `booking_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cafeusers`
--

CREATE TABLE IF NOT EXISTS `tbl_cafeusers` (
  `cafeusers_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cafeusers_name` varchar(255) NOT NULL,
  `cafeusers_email` varchar(255) NOT NULL,
  `cafeusers_phone` varchar(255) NOT NULL,
  `cafeusers_address` varchar(255) NOT NULL,
  `cafeusers_state` int(11) NOT NULL,
  `cafeusers_country` int(11) NOT NULL,
  `cafeusers_zip` varchar(255) NOT NULL,
  PRIMARY KEY (`cafeusers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_cafeusers`
--

INSERT INTO `tbl_cafeusers` (`cafeusers_id`, `user_id`, `cafeusers_name`, `cafeusers_email`, `cafeusers_phone`, `cafeusers_address`, `cafeusers_state`, `cafeusers_country`, `cafeusers_zip`) VALUES
(1, 5, 'Ankush', 'ankush@exceptionaire.co', '1234567890', 'Hno 1564', 8, 1, '45632'),
(2, 6, 'Priya', 'priya@exceptionaire.co', '1234567890', 'Hno 1564', 8, 1, '45632'),
(3, 22, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(4, 23, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(5, 24, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(6, 25, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(7, 26, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(8, 27, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(9, 28, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(10, 29, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(11, 30, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(12, 31, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(13, 32, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_claim`
--

CREATE TABLE IF NOT EXISTS `tbl_claim` (
  `claim_id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_claimcategory_id` int(11) NOT NULL,
  `claim_client_id` int(11) NOT NULL,
  `claim_worker_id` int(11) NOT NULL,
  `claim_status` tinyint(4) NOT NULL DEFAULT '0',
  `claim_created_at` varchar(255) NOT NULL,
  `claim_updated_id` varchar(255) NOT NULL,
  PRIMARY KEY (`claim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_claimcategory`
--

CREATE TABLE IF NOT EXISTS `tbl_claimcategory` (
  `claimcategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `claimcategory_name` varchar(255) NOT NULL,
  `claimcategory_des` text NOT NULL,
  `claimcategory_status` tinyint(4) NOT NULL DEFAULT '0',
  `claimcategory_created_at` varchar(255) NOT NULL,
  `claimcategory_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`claimcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_clients`
--

CREATE TABLE IF NOT EXISTS `tbl_clients` (
  `clients_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `clients_name` varchar(255) NOT NULL,
  `clients_email` varchar(255) NOT NULL,
  `clients_phone` varchar(255) NOT NULL,
  `clients_address` varchar(255) NOT NULL,
  `clients_state` int(11) NOT NULL,
  `clients_country` int(11) NOT NULL,
  `clients_zip` varchar(255) NOT NULL,
  `clients_image` text NOT NULL,
  `clients_lat` varchar(255) NOT NULL,
  `clients_long` varchar(255) NOT NULL,
  `clients_suspend_start` varchar(255) NOT NULL,
  `clients_suspend_end` varchar(255) NOT NULL,
  PRIMARY KEY (`clients_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_clients`
--

INSERT INTO `tbl_clients` (`clients_id`, `user_id`, `clients_name`, `clients_email`, `clients_phone`, `clients_address`, `clients_state`, `clients_country`, `clients_zip`, `clients_image`, `clients_lat`, `clients_long`, `clients_suspend_start`, `clients_suspend_end`) VALUES
(1, 0, 'Ankush', 'ankush@exceptionaire.co', '1234567890', '', 0, 0, '', '', 'ankush', 'ankush', '', ''),
(2, 0, 'Priya', 'priya@exceptionaire.co', '1234567890', '', 0, 0, '', '', 'Priya', 'priya', '', ''),
(3, 12, 'Ankush', 'admin1dd2d31@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(4, 14, 'Ankush', 'admin1dd2d311@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

CREATE TABLE IF NOT EXISTS `tbl_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_name` varchar(255) NOT NULL,
  `faq_des` text NOT NULL,
  `faq_role` int(11) NOT NULL,
  `faq_status` tinyint(4) NOT NULL DEFAULT '0',
  `faq_created_at` varchar(255) NOT NULL,
  `faq_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_favorite`
--

CREATE TABLE IF NOT EXISTS `tbl_favorite` (
  `favorite_id` int(11) NOT NULL AUTO_INCREMENT,
  `favorite_client_id` int(11) NOT NULL,
  `favorite_worker_id` int(11) NOT NULL,
  `favorite_created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`favorite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leaves`
--

CREATE TABLE IF NOT EXISTS `tbl_leaves` (
  `leaves_id` int(11) NOT NULL AUTO_INCREMENT,
  `leaves_name` varchar(255) NOT NULL,
  `leaves_des` text NOT NULL,
  `leaves_status` tinyint(4) NOT NULL DEFAULT '0',
  `leaves_created_at` varchar(255) NOT NULL,
  `leaves_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`leaves_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating`
--

CREATE TABLE IF NOT EXISTS `tbl_rating` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approved, 1 = Approved',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = Deleted',
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_rating`
--

INSERT INTO `tbl_rating` (`rating_id`, `rating_name`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Cleaning', 1, '2018-01-16 20:36:35', '2018-01-16 20:36:35', 0),
(2, 'Ironing', 1, '2018-01-16 20:47:53', '2018-01-16 20:36:35', 0),
(3, 'Cleaning8', 1, '2018-01-16 21:08:58', '2018-01-16 21:08:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE IF NOT EXISTS `tbl_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approved, 1 = Approved',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = Deleted',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role_id`, `role_name`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Administrator', 1, '2018-01-14 09:44:44', '2018-01-14 09:44:44', 0),
(2, 'User Cafe ', 1, '2018-01-14 09:44:44', '2018-01-14 09:44:44', 0),
(3, 'Workers', 1, '2018-01-14 09:44:44', '2018-01-14 09:44:44', 0),
(4, 'Assessors', 1, '2018-01-14 09:44:44', '2018-01-14 09:44:44', 0),
(5, 'Clients', 1, '2018-01-14 09:44:44', '2018-01-14 09:44:44', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_servicetype`
--

CREATE TABLE IF NOT EXISTS `tbl_servicetype` (
  `servicetype_id` int(11) NOT NULL AUTO_INCREMENT,
  `servicetype_name` varchar(255) NOT NULL,
  `servicetype_status` tinyint(4) NOT NULL DEFAULT '0',
  `servicetype_created_at` varchar(255) NOT NULL,
  `servicetype_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`servicetype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_servicetypecategory`
--

CREATE TABLE IF NOT EXISTS `tbl_servicetypecategory` (
  `servicetypecategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `servicetypecategory_servicetype_id` int(11) NOT NULL,
  `servicetypecategory_name` varchar(255) NOT NULL,
  `servicetypecategory_status` tinyint(4) NOT NULL DEFAULT '0',
  `servicetypecategory_created_at` varchar(255) NOT NULL,
  `servicetypecategory_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`servicetypecategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_servicetypeestimate`
--

CREATE TABLE IF NOT EXISTS `tbl_servicetypeestimate` (
  `servicetypeestimate_id` int(11) NOT NULL AUTO_INCREMENT,
  `servicetypeestimate_servicetype_id` int(11) NOT NULL,
  `servicetypeestimate_servicetypecategory_id` int(11) NOT NULL,
  `servicetypeestimate_name` varchar(255) NOT NULL,
  `servicetypeestimate_hour` varchar(255) NOT NULL,
  `servicetypeestimate_status` tinyint(4) NOT NULL DEFAULT '0',
  `servicetypeestimate_created_at` varchar(255) NOT NULL,
  `servicetypeestimate_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`servicetypeestimate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_skillcategory`
--

CREATE TABLE IF NOT EXISTS `tbl_skillcategory` (
  `skillcategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `skillcategory_skill_id` int(11) DEFAULT NULL,
  `skillcategory_name` varchar(255) NOT NULL,
  `skillcategory_des` text NOT NULL,
  `skillcategory_status` tinyint(4) NOT NULL DEFAULT '0',
  `skillcategory_created_at` varchar(255) NOT NULL,
  `skillcategory_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`skillcategory_id`),
  KEY `skillcategory_skill_ID` (`skillcategory_skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_skills`
--

CREATE TABLE IF NOT EXISTS `tbl_skills` (
  `skills_id` int(11) NOT NULL AUTO_INCREMENT,
  `skills_name` varchar(255) NOT NULL,
  `skills_des` text NOT NULL,
  `skills_status` tinyint(4) NOT NULL DEFAULT '0',
  `skills_created_at` varchar(255) NOT NULL,
  `skills_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`skills_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_substitute`
--

CREATE TABLE IF NOT EXISTS `tbl_substitute` (
  `substitute_id` int(11) NOT NULL AUTO_INCREMENT,
  `substitute_send_worker_id` int(11) NOT NULL,
  `substitute_receive_worker_id` int(11) NOT NULL,
  `substitute_status` tinyint(4) NOT NULL DEFAULT '0',
  `substitute_created_at` varchar(255) NOT NULL,
  `substitute_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`substitute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tutorial`
--

CREATE TABLE IF NOT EXISTS `tbl_tutorial` (
  `tutorial_id` int(11) NOT NULL AUTO_INCREMENT,
  `tutorial_type` int(11) NOT NULL,
  `tutorial_text` text NOT NULL,
  `tutorial_status` tinyint(4) NOT NULL DEFAULT '0',
  `tutorial_created_at` varchar(255) NOT NULL,
  `tutorial_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`tutorial_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `user_registertype` int(11) NOT NULL,
  `device_token` text NOT NULL,
  `device_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1= Web, 2 = Android, 3 = iPhone',
  `social_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approve, 1 = Approve',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_suspend` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Suspend, 1 = Suspend',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Delete, 1 = Delete',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_username`, `user_password`, `user_role_id`, `user_registertype`, `device_token`, `device_type`, `social_id`, `status`, `created_at`, `updated_at`, `is_suspend`, `is_deleted`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '', 0, 0, 1, '2018-01-19 10:13:59', '2018-01-14 09:53:42', 0, 0),
(2, 'aman', 'ccda1683d8c97f8f2dff2ea7d649b42c', 4, 0, '', 0, 0, 1, '2018-01-20 10:47:06', '2018-01-14 09:53:42', 0, 0),
(3, 'priya', '5e8b0322bedd4d4029bfc3bd9544b017', 3, 0, 'Uz5DUkAAAAsEHqc2', 1, 0, 1, '2018-01-19 19:07:54', '2018-01-19 19:07:54', 0, 0),
(4, 'ankush', '3a0135f9157447e16da5c17863f1531c', 5, 0, 'asdgasgdjgsahd', 2, 0, 1, '2018-01-19 19:17:36', '2018-01-19 19:17:36', 0, 0),
(5, 'amit', '0cb1eb413b8f7cee17701a37a1d74dc3', 2, 0, '', 0, 0, 1, '2018-01-19 10:13:59', '2018-01-14 09:53:42', 0, 0),
(6, 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 2, 0, '213213sdfdnanjkngkj', 127, 21323132, 1, '2018-01-19 10:13:59', '2018-01-15 19:24:08', 0, 0),
(7, 'adminq@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 3, 0, '213213sdfdnanjkngkj', 127, 21323132, 1, '2018-01-19 10:13:59', '2018-01-15 19:29:44', 0, 0),
(8, 'adminq@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 3, 0, '213213sdfdnanjkngkj', 127, 21323132, 1, '2018-01-19 10:13:59', '2018-01-15 19:30:03', 0, 0),
(15, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:03:50', 0, 0),
(16, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:05:36', 0, 0),
(17, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:10:34', 0, 0),
(18, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:11:25', 0, 0),
(19, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:12:23', 0, 0),
(20, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:13:10', 0, 0),
(21, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:14:37', 0, 0),
(22, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:15:05', 0, 0),
(23, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:17:49', 0, 0),
(24, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:17:54', 0, 0),
(25, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:17:57', 0, 0),
(26, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:18:00', 0, 0),
(27, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:18:02', 0, 0),
(28, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:18:03', 0, 0),
(29, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:18:04', 0, 0),
(30, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:20:54', 0, 0),
(31, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:22:04', 0, 0),
(32, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:22:47', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_workers`
--

CREATE TABLE IF NOT EXISTS `tbl_workers` (
  `workers_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `workers_name` varchar(255) NOT NULL,
  `workers_email` varchar(255) NOT NULL,
  `workers_phone` varchar(255) NOT NULL,
  `workers_address` varchar(255) NOT NULL,
  `workers_state` int(11) NOT NULL,
  `workers_country` int(11) NOT NULL,
  `workers_zip` varchar(255) NOT NULL,
  `workers_image` text NOT NULL,
  `workers_lat` varchar(255) NOT NULL,
  `workers_long` varchar(255) NOT NULL,
  `workers_suspend_start` varchar(255) NOT NULL,
  `workers_suspend_end` varchar(255) NOT NULL,
  PRIMARY KEY (`workers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_workers`
--

INSERT INTO `tbl_workers` (`workers_id`, `user_id`, `workers_name`, `workers_email`, `workers_phone`, `workers_address`, `workers_state`, `workers_country`, `workers_zip`, `workers_image`, `workers_lat`, `workers_long`, `workers_suspend_start`, `workers_suspend_end`) VALUES
(1, 8, 'Ankush', 'adminq@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(2, 7, 'Ankush', 'adminq@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(3, 3, 'Ankush', 'adminq@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_name` varchar(255) NOT NULL,
  `test_date` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approved, 1= Approved ',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`test_id`, `test_name`, `test_date`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Ankush', '12-12-2018', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Priya', '12-12-2018', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'test', '12-12-2018', 0, 1, '2018-01-12 04:29:59', '0000-00-00 00:00:00'),
(6, 'test', '12-12-2018', 0, 1, '2018-01-12 04:29:49', '0000-00-00 00:00:00'),
(7, 'test', '12-12-2018', 0, 1, '2018-01-12 04:29:53', '0000-00-00 00:00:00'),
(8, 'test', '12-12-2018', 0, 1, '2018-01-12 04:29:56', '0000-00-00 00:00:00'),
(9, 'Aman', '12-12-2018', 0, 0, '2018-01-12 04:28:29', '2018-01-12 04:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `test_categores`
--

CREATE TABLE IF NOT EXISTS `test_categores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0 = Not Approved, 1 = Approved',
  `is_deleted` tinyint(4) NOT NULL COMMENT '0 = Not Deleted, 1 = Deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `test_categores`
--

INSERT INTO `test_categores` (`id`, `name`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Abc', 0, 0, '2018-01-12 05:23:38', '2018-01-12 05:23:38'),
(2, 'Abc', 0, 0, '2018-01-12 05:24:34', '2018-01-12 05:24:34'),
(3, 'Abc', 0, 0, '2018-01-12 05:26:23', '2018-01-12 05:26:23'),
(4, 'Abcd', 0, 1, '2018-01-12 05:30:01', '2018-01-12 05:27:26'),
(5, 'priya', 0, 0, '2018-01-12 05:15:36', '2018-01-12 05:15:36'),
(6, 'dfhdh', 0, 0, '2018-01-12 05:15:40', '2018-01-12 05:15:40'),
(7, 'dfhdhdfhdfh', 0, 0, '2018-01-12 05:15:44', '2018-01-12 05:15:44'),
(8, 'sdfsdfhdhdfhdfh', 0, 0, '2018-01-12 05:15:48', '2018-01-12 05:15:48'),
(9, 'sdfhdhdfhdfh', 0, 0, '2018-01-12 05:15:51', '2018-01-12 05:15:51'),
(10, 'shdfh', 0, 0, '2018-01-12 05:15:53', '2018-01-12 05:15:53'),
(11, 'shdfhsdfsdf', 0, 0, '2018-01-12 05:15:53', '2018-01-12 05:15:53'),
(12, 'shsdfsdf', 0, 0, '2018-01-12 05:15:53', '2018-01-12 05:15:53'),
(13, 'test', 0, 0, '2018-01-12 05:16:45', '0000-00-00 00:00:00'),
(14, 'test12', 0, 0, '2018-01-12 05:16:45', '0000-00-00 00:00:00'),
(15, 'TestTestTest', 0, 0, '2018-01-12 05:59:21', '2018-01-12 05:59:21'),
(16, 'TestTestTest', 0, 0, '2018-01-12 06:41:16', '2018-01-12 06:41:16');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
